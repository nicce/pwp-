# Meetings notes

## Meeting 1.
* **DATE:** 15.2.2019
* **ASSISTANTS:** Marta Cortés Orduña 
* **GRADE:** *To be filled by course staff*

### Minutes
Project and its main concepts were explained verbally. The diagram was not really describing about main concepts: it's more about information relations.
More than enough resources for API calls; minimum requirements for REST are met.
User authentication is processed as extra work: not mandatory, so potential extra points from there.

### Action points
New better diagram is required about main concepts. 

The chapter describing relations should be constructed and written better than earlier.


### Comments from staff
*ONLY USED BY COURSE STAFF: Additional comments from the course staff*

## Meeting 2.
* **DATE:*1.3.2019*
* **ASSISTANTS:*Iván Sánchez Milara*
* **GRADE:** *To be filled by course staff*

### Minutes
We discussed about the project code. Assistant found our project and group of 4 people challenging. Our project contains many things.
We should comment the tables and use id as a primary key. The structure of the project looks good.

### Action points
We should focus on design and implementation. We should test every relation. 

### Comments from staff
*ONLY USED BY COURSE STAFF: Additional comments from the course staff*

## Meeting 3.
* **DATE:** 28.3.2019
* **ASSISTANTS:** Ivan Sanchez Milara
* **GRADE:** *To be filled by course staff*

### Minutes
* Auth does not need to be a resource, it can be isolated. 
* We should try to avoid 500 code, it shouldn't ever happen. 
* For clarity, each resource in the picture should be named similarly as in the resource table (e.g. "device" --> "target-device-item" and "device" --> "device-item") descriptive way.
* Some "up" relations could be changed to "collection" (e.g. from Target --> Targets)


### Action points
* We should complete profiles in blueprint. 
* We should justify why we are using separate "new-scan" resource and not POSTing the new scan to the "Scans" resource. (Justification is discussed in https://gitlab.com/Nicce/pwp-/wikis/RESTful-API-design#addressability-and-uniform-interface)

### Comments from staff
*ONLY USED BY COURSE STAFF: Additional comments from the course staff*

## Meeting 4.
* **DATE:** 26.4.2019
* **ASSISTANTS:** Mika Oja
* **GRADE:** *To be filled by course staff*

### Minutes
*Summary of what was discussed during the meeting*
* Lot of time spent for implementing DL4
* Project size is lot bigger than required by the course
* Overall test coverage is ~80% of all code lines. However, the coverage is 90-100% for resources that have been implemented so far. 

### Action points
*List here the actions points discussed with assistants*
* No actions defined

### Comments from staff
*ONLY USED BY COURSE STAFF: Additional comments from the course staff*

## Midterm meeting
* **DATE:**
* **ASSISTANTS:**
* **GRADE:** *To be filled by course staff*

### Minutes
*Summary of what was discussed during the meeting*

### Action points
*List here the actions points discussed with assistants*


### Comments from staff
*ONLY USED BY COURSE STAFF: Additional comments from the course staff*


## Final meeting
* **DATE:**
* **ASSISTANTS:**
* **GRADE:** *To be filled by course staff*

### Minutes
*Summary of what was discussed during the meeting*

### Action points
*List here the actions points discussed with assistants*


### Comments from staff
*ONLY USED BY COURSE STAFF: Additional comments from the course staff*

