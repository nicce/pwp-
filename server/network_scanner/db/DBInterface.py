from flask import json, request, g
from network_scanner.db.models import User, Device, ScanResult, TargetResult, \
    OpenPort, SinglePort, Traceroute, HOP, db, UserSession
# from network_scanner.config import db
from sqlalchemy.exc import IntegrityError, OperationalError, DataError
import logging
from datetime import datetime
from datetime import timedelta
import uuid
import bcrypt
from werkzeug.exceptions import BadRequest, Conflict,\
    InternalServerError, NotFound, Forbidden, Unauthorized
from datetime import datetime
from network_scanner.config import ADMIN_SECRET, DEFAULT_TOKEN_EXPIRING_TIME


class DBInterface:
    """
    Interface for using the SQLAlchemy with defined database models.
    It is designed mostly in such a way, that it uses static methods and
    possible errors are excepted on higher level.
    There are some cases, when expection is thrown to generate
    some responses from Web server.

    Interface contains validation against database model schemas and
    throws speficied errors in required situations.
    """

    def __init__(self):
        self.name = "Network Scanner Database"

    @staticmethod
    def db_validateToken(token):
        """
        This method checks if there is any user with provided token.

        Returns User sqlalchemy model object if there is one,
        otherwise None

        :param token: Bearer token, passed from security_controller
        :rtype: Sqlalchemy model User
        """
        # PERMANENT tokens:
        # Valid token: 'qwerty' for John Doe, not admin
        # Valid token: '12312AB3info3i' for Very first user, is admin

        try:
            # Check from permanent keys
            user = User.query.filter_by(API_Key=token).first()
            if not user:
                # Check from temporal session tokens
                user_session = UserSession.query.filter_by(
                    SessionToken=token).first()
                if not user_session:
                    return None
                if user_session.TokenExpiresIn < datetime.now():
                    db.session.delete(user_session)
                    db.session.commit()
                    raise Unauthorized("Session token expired. Please, "
                                       "re-log with username and password,"
                                       " or use refresh token.")
                user = User.query.filter_by(
                    ID=user_session.User_ID).first()
                # user = User.query.join(UserSession).filter(
                #     User.ID == UserSession.User_ID).filter(
                #         UserSession.SessionToken == token).first()
        except (OperationalError, KeyError) as e:
            logging.error('Query of user tokens failed: {}'.format(e))
            raise OperationalError("Something went wrong when querying all \
            tokens. User table probably does not exist.",
                                   e, 'User token query')
        else:
            if user:
                user.LastLogin = datetime.now()
                db.session.commit()
                return user
            else:
                return None

    @staticmethod
    def db_validateBasicAuth(username, password):
        """
        Method for querying Basic Auth credentials from database.
        Checking if username and password are valid.

        If username and password are valid, temporary session token
        will be generated. This token should be used as OAUTH 2
        Bearer token, as long as it is valid. Once it expires, user is
        required to log in with username and password again.oauth

        Default expiring time is one (1) hour.

        :param username: Username of the user
        :param password: Password of the user
        :rtype dict: Return dictionary containing userID and token
        """
        try:
            user = User.query.filter_by(Username=username).first()
        except (OperationalError, KeyError) as e:
            logging.error('Somewthing went wrong with database when'
                          ' validating basic auth.'
                          .format(username))
            raise OperationalError("Looks like no-user will log in this time.",
                                   None, 'User login')
        else:
            if not user:
                raise Unauthorized("Invalid username or password.")
            try:
                if bcrypt.checkpw(password.encode('utf8'),
                                  user.HashedPassword):
                    """
                    Passwords match, let's generate new session,
                    if there isn't one already.
                    Delete old if there is.
                    """
                    user_session = UserSession.query.filter_by(
                        User_ID=user.ID).first()
                    if user_session:
                        db.session.delete(user_session)
                        db.session.commit()
                    now = datetime.now()
                    token = uuid.uuid4().hex
                    user_session = UserSession(
                        SessionToken=token,
                        TokenCreated=now,
                        # Expires in one hour
                        TokenExpiresIn=now + \
                        timedelta(seconds=DEFAULT_TOKEN_EXPIRING_TIME)
                    )
                    user.UsersSession = user_session
                    user.LastLogin = datetime.now()
                    try:
                        db.session.commit()
                    except IntegrityError as e:
                        db.session.rollback()
                        logging.debug(
                            "Session token creation failed: {}".format(e.args))
                        raise InternalServerError("Something went wrong."
                                                  " Please,"
                                                  " try again later.")
                    return {
                        'user_id': user.ID,
                        'session_token': token
                    }
                else:
                    logging.error('Login of user {} with Basic Auth failed.'
                                  .format(username))
                    raise Unauthorized("Invalid username or password.")
            except TypeError as e:
                # Either request password or database password was wrong type
                # Not match. By default test data hash is not encoded, and code
                # ends here.
                raise Unauthorized("Invalid username or password.")

    @staticmethod
    def db_destroyUserSession(user_id):
        """
        Method for deleting the user session from database.
        Just in case deleting all sessions related to user.
        Used in logout controller.
        """
        user_sessions = UserSession.query.filter_by(
            User_ID=user_id).all()
        for user_session in user_sessions:
            db.session.delete(user_session)
        db.session.commit()

    # Method for querying all users from the database
    @staticmethod
    def db_getAllUsers():
        """
        Querying database for all users. Note that we are catching
        exceptions already here, but throwing them anyway
        again for higher level.
        We want to catch them on higher level as well, but limit
        the information disclosure at the same time.
        """
        try:
            logging.debug('Querying for all users from the database')
            users = User.query.order_by(User.Username).all()
        except (KeyError, IntegrityError) as e:
            logging.error('This should not happen: {}'.format(e))
            raise IntegrityError('HMM', e, 'User table query')
        except OperationalError as e:
            logging.error('Query of all users failed: {}'.format(e))
            raise OperationalError("Something went wrong when querying all \
            users. User table probably does not exist.", e, 'User table query')
        else:
            users_list = []
            for user in users:
                user_d = {
                    'ID': user.ID,
                    'Email': user.Email,
                    'Username': user.Username,
                    'FirstName': user.FirstName,
                    'Surname': user.Surname,
                    'isAdmin': user.isAdmin,
                    'CreatedAt': user.CreatedAt,
                    'LastLogin': user.LastLogin
                }
                users_list.append(user_d)

            return users_list

    # Method for querying single user by user_id from the database
    @staticmethod
    def db_getSingleUser(user_id):
        """
        Querying database for single user. Note that we are not catching
        any exceptions here.
        We want to catch them on higher level.
        """
        user = User.query.filter_by(ID=user_id).first()
        if user:
            user_d = {
                'ID': user.ID,
                'Email': user.Email,
                'Username': user.Username,
                'FirstName': user.FirstName,
                'Surname': user.Surname,
                'isAdmin': user.isAdmin,
                'CreatedAt': user.CreatedAt,
                'LastLogin': user.LastLogin
            }
            logging.debug("User with id {} found.".format(user_id))
            return user_d
        logging.debug("User with id {} not found.".format(user_id))
        return None

    @staticmethod
    def db_addNewUser(user):
        """
        Adds a new user for database.
        Database checks that username, auth token and email are always
        unique.

        Returns oauth credentials and userID of new user.
        """

        if (user.new_password == user.repeat_new_password):
            # Generate (not secure) random token
            key = uuid.uuid4()
            user_d = User(
                Username=user.username,
                Email=user.email,
                FirstName=user.first_name,
                Surname=user.surname,
                # We are using random salt for bcrypt
                HashedPassword=bcrypt.hashpw(
                    user.new_password.encode('utf8'), bcrypt.gensalt()),
                API_Key=key.hex,
                isAdmin=user.is_admin if user.secret == ADMIN_SECRET\
                else False,
                CreatedAt=datetime.now(),
            )

            """
            Very simplified oauth credentials
            Refresh_token actually not in use, it is permanent access token.
            Access token lasts one hour by default.
            Refersh token added just for demoing purposes
            """
            now = datetime.now()
            token = uuid.uuid4().hex
            user_session = UserSession(
                SessionToken=token,
                TokenCreated=now,
                # Expires in one hour
                TokenExpiresIn=now + timedelta(
                    seconds=DEFAULT_TOKEN_EXPIRING_TIME)
            )
            user_d.UsersSession = user_session
            db.session.add(user_d)
            try:
                db.session.commit()
            except IntegrityError as e:
                db.session.rollback()
                raise Conflict(e.args)
            except DataError as e:
                db.session.rollback()
                raise BadRequest("Invalid data type or content too large.: {}"
                                 .format(e.args))
            logging.debug("New user {} added.".format(user.username))

            credentials = {
                'access_token': token,
                'token_type': 'Bearer',
                'expires_in': DEFAULT_TOKEN_EXPIRING_TIME,
                'refresh_token': key.hex,
                'scope': 'admin' if user.is_admin and
                user.secret == ADMIN_SECRET
                else 'specific-user'
            }
            logging.debug("New user ID is {}".format(user_d.ID))
            return credentials, user_d.ID

        else:
            logging.debug("Passwords not equal in new User Post.")
            raise BadRequest("Passwords are not equal.")

    @staticmethod
    def db_editSingleUser(user, user_id):
        """
        Method for editing existing single user.
        It is used by PUT method, all values of existing
        user will be overridden.
        """
        if (user.new_password == user.repeat_new_password):
            UserExist = User.query.filter_by(ID=user_id).first()
            if not UserExist:
                raise NotFound(
                    "User with user id {} not found for editing.".format(
                        user_id))
            # Update whole existing user
            # since PUT was used.
            UserExist.Username = user.username
            UserExist.Email = user.email
            UserExist.FirstName = user.first_name
            UserExist.Surname = user.surname
            UserExist.HashedPassword = bcrypt.hashpw(
                user.new_password.encode('utf8'), bcrypt.gensalt())
            UserExist.isAdmin = user.is_admin if user.secret == ADMIN_SECRET\
                else False
            try:
                db.session.commit()
            except IntegrityError as e:
                db.session.rollback()
                # Raise conflict if some of the values must be unique
                # and existed already
                raise Conflict(e.args)
            except DataError as e:
                db.session.rollback()
                raise BadRequest("Invalid data type or content too big: {}"
                                 .format(e.args))
            logging.debug("User {} edited successfully.".format(user_id))
        else:
            logging.debug("Passwords not equal in User Edit.")
            raise BadRequest("Passwords are not equal.")

    @staticmethod
    def db_deleteSingleUser(user_id):
        """
        Method for deleting single user from the database,
        and everything related to it.
        """
        user = User.query.filter_by(ID=user_id).first()
        if not user:
            raise NotFound(
                "User with user id {} not found for deleting.".format(user_id))
        db.session.delete(user)
        db.session.commit()
        logging.debug("User {} deleted.".format(user_id))

    @staticmethod
    def db_getAllScansOfUser(user_id):
        """
        Method for querying all scans of single user from the database.
        """
        scans = ScanResult.query.filter_by(User_ID=user_id)
        if not scans:
            return None
        """
        Convert Sqlalchemy model object
        into dictionary, which can be converted further into
        scan_item_get_response object
        """
        scans_list = []
        for scan in scans:
            scan_d = {
                'scan_id': scan.ID,
                'user_id': scan.User_ID,
                'Comment': scan.Comment,
                'IsTagged': scan.IsTagged,
                'ScanParams': scan.ScanParams,
                'ModifiedAt': scan.ModifiedAt,
                'CreatedAt': scan.CreatedAt,
                'ScanInProgress': scan.ScanInProgress
            }
            scans_list.append(scan_d)
        return scans_list

    @staticmethod
    def db_getSingleScanOfUser(user_id, scan_id):
        """
        Method for querying single scan result of user
        form the database. Returns dictionary, which is
        convertable for scan_item_get_response object

        :param user_id: Indentification number of the user
        :param scan_id: Indentification number of the scan
        """
        scan = ScanResult.query.join(User).filter(
            User.ID == user_id,
            ScanResult.ID == scan_id).first()
        if not scan:
            raise NotFound("Scan result with ID {} not found for user ID {}."
                           .format(scan_id, user_id))

        scanresult = {
            'scan_id': scan.ID,
            'user_id': scan.User_ID,
            'Comment': scan.Comment,
            'IsTagged': scan.IsTagged,
            'ScanParams': scan.ScanParams,
            'ModifiedAt': scan.ModifiedAt,
            'CreatedAt': scan.CreatedAt,
            'ScanInProgress': scan.ScanInProgress
        }
        return scanresult

    @staticmethod
    def db_addNewScan(body, user_id):
        """
        Insert new scan results into the database.

        :param user_id: Indentication number of the user
        """
        logging.debug("New scan arguments of user {} :\n{}\n{}\n{}\n{}"
                      .format(user_id, body.is_single_target,
                              body.options_for_targets,
                              body.arbitary_command, body.target,
                              body.arbitrary_command_in_txtfile))
        return 1

    @staticmethod
    def db_editSingleScan(body, user_id, scan_id):
        """
        Edit single scan of user.
        """
        scan = ScanResult.query.join(User).filter(
            User.ID == user_id,
            ScanResult.ID == scan_id).first()
        if not scan:
            raise NotFound("Scan with id {} for user {} not found"
                           " for editing."
                           .format(scan_id, user_id))
        if body.comment:
            scan.Comment = body.comment
        scan.IsTagged = body.is_tagged
        try:
            db.session.commit()
        except (IntegrityError, DataError) as e:
            db.session.rollback()
            raise BadRequest("Invalid data or content too long: {}"
                             .format(e.args))

    @staticmethod
    def db_deleteSingleScan(user_id, scan_id):
        """
        Delete single scan of the user.
        """
        scan = ScanResult.query.join(User).filter(
            User.ID == user_id,
            ScanResult.ID == scan_id).first()
        if not scan:
            raise NotFound(
                "Scan with id {} for user {} not found for deleting.".format(user_id, scan_id))
        db.session.delete(scan)
        db.session.commit()
        logging.debug("Scan with scan id {} for user {} deleted."
                      .format(scan_id, user_id))

    @staticmethod
    def db_getAllTargetsOfSingleScanOfUser(user_id, scan_id):
        """
        Method for querying all targets of single scan by user from
        the database.
        """
        targets = TargetResult.query.join(ScanResult).filter(
            ScanResult.ID == scan_id, User.ID == user_id)
        if not targets:
            return None
        """
        Convert Sqlalchemy model object
        into dictionary, which can be converted further into
        target_item_get_response object
        """
        targets_list = []
        for target in targets:
            target_d = {
                'Target_ID': target.ID,
                'Scan_ID': target.Scan_ID,
                'Device_ID': target.Device_ID,
                'Comment': target.Comment,
                'IsTagged': target.IsTagged,
                'IPv4_address': target.IPv4_address,
                'IPv6_address': target.IPv6_address,
                'Reverse_DNS': target.Reverse_DNS,
                'ModifiedAt': target.ModifiedAt,
                'TargetDomainName': target.TargetDomainName
            }
            targets_list.append(target_d)
        return targets_list

    @staticmethod
    def db_getSingleTargetOfScanOfUser(user_id, scan_id, target_id):
        """
        Get single target result of scan of single user.
        Scan might have multiple target results.
        User might have multiple scan results.

        :param user_id: Indentification number of the user
        :param scan_id: Identification number of the scan
        :param target_id: Identification number of the target
        """
        target = TargetResult.query.join(ScanResult).join(User).filter(
            ScanResult.ID == scan_id, TargetResult.ID == target_id,
            User.ID == user_id).first()
        if not target:
            logging.debug("Target {} not found when querying device in"
                          " scan {} of user {}".format(
                              target_id, scan_id, user_id))
            return None
        return target

    @staticmethod
    def db_editDevice(body,  user_id, device_id=None, scan_id=None, target_id=None):
        if device_id:  # calling from method  'api_users_user_id_devices_device_id_delete()'
            device = Device.query.join(TargetResult).join(ScanResult).join(
                User).filter(User.ID == user_id, Device.ID == device_id).first()
            if not device:
                raise NotFound(
                    "Device with user ID {} and device ID {} not found for editing.".format(user_id, device_id))
            if body.comment == None:
                raise BadRequest(
                    "Invalid data or content too long: {}".format(body))
            device.Comment = body.comment
            device.ModifiedAt = datetime.utcnow()
            db.session.commit()
        if target_id:  # calling from 'api_users_user_id_scans_scan_id_targets_target_id_device_patch' method
            device = Device.query.join(TargetResult).join(ScanResult).join(User).filter(
                User.ID == user_id, ScanResult.ID == scan_id, TargetResult.ID == target_id).first()
            if not device:
                raise NotFound(
                    "Device with user ID = {}, scan ID = {} and target ID = {} not found for editing.".format(user_id, scan_id, target_id))
            if body.comment == None:
                raise BadRequest(
                    "Invalid data or content too long: {}".format(body))
            device.Comment = body.comment
            device.ModifiedAt = datetime.utcnow()
            db.session.commit()

    @staticmethod
    def db_deleteDevice(user_id, device_id=None, scan_id=None, target_id=None):
        if device_id:  # calling from method  'api_users_user_id_devices_device_id_delete()'
            device = Device.query.join(TargetResult).join(
                ScanResult).join(User).filter(User.ID == user_id,
                                              Device.ID == device_id).first()
            if not device:
                raise NotFound(
                    "Device with user ID {} and device ID {} not found for editing.".format(user_id, device_id))
            db.session.delete(device)
            db.session.commit()
            logging.debug("Device {} deleted.".format(device_id))
        if target_id:  # calling from 'api_users_user_id_scans_scan_id_targets_target_id_device_delete' method
            device = Device.query.join(TargetResult).join(
                ScanResult).join(User).filter(User.ID == user_id,
                                              ScanResult.ID == scan_id, TargetResult.ID == target_id).first()
            if not device:
                raise NotFound(
                    "Device with user ID = {}, scan ID = {} and target ID = {} not found for deleting.".format(user_id, scan_id, target_id))
            db.session.delete(device)
            db.session.commit()
            logging.debug("Device {} deleted.".format(device.ID))

    # Method for querying single device  from the database
    @staticmethod
    def db_getUserDeviceId(user_id, device_id):
        # Note that we are not catching any exceptions here.
        # We want to catch them on higher level.

        device = Device.query.join(TargetResult).join(
            ScanResult).join(User).filter(User.ID == user_id,
                                          Device.ID == device_id).all()
        return device

    # Method for querying all devices by single user from the database
    @staticmethod
    def db_getUserDevices(user_id):
        # Note that we are not catching any exceptions here.
        # We want to catch them on higher level.

        devices = Device.query.join(TargetResult).join(
            ScanResult).join(User).filter(User.ID == user_id).all()

        device_list = []
        for device in devices:
            device_d = {
                'device_id': device.ID,
                'mac_address_64bit': device.MAC_Address_64_bit_,
                # 'CustomName': device.CustomName,
                'os': device.OS,
                'comment': device.Comment,
                'extra_info': device.ExtraInfo,
                'modified_at': device.ModifiedAt.strftime("%Y-%m-%d %H:%M:%S")
            }
            # print("device_d: " + str(device_d))
            device_list.append(device_d)
        return device_list

    # Method for querying target device
    @staticmethod
    def db_getTargetDevice(user_id, scan_id, target_id):
        """
        Get device information of single target result.

        :param user_id: Indentification number of the user
        :param scan_id: Identification number of the scan
        :param target_id: Identification number of the target
        """

        target = DBInterface.db_getSingleTargetOfScanOfUser(
            user_id, scan_id, target_id)
        if not target:
            return None
        device = target.Device
        if not device:
            return None
        device_d = {
            'device_id': device.ID,
            'MAC_address_64bit': device.MAC_Address_64_bit_,
            # 'CustomName': device.CustomName,
            'OS': device.OS,
            'Comment': device.Comment,
            'ExtraInfo': device.ExtraInfo,
            'ModifiedAt': device.ModifiedAt.strftime("%Y-%m-%d %H:%M:%S")
        }
        # print("device_d: " + str(device_d))
        return device_d

    @staticmethod
    def db_getTargetRoute(user_id, scan_id, target_id):
        """
        Get route for target of corresponding user and scan

        :param user_id: Indentification number of the user
        :param scan_id: Identification number of the scan
        :param target_id: Identification number of the target
        """
        # Get correct target
        target = DBInterface.db_getSingleTargetOfScanOfUser(
            user_id, scan_id, target_id)

        if not target:
            return None
        route = target.traceroute
        # logging.error(route)
        if not route:
            return None

        # Get 'hops' of target to appen on final route
        hops = []
        for hop in route.hops:
            hop_d = {
                'HOP_Nro': hop.HOP_Nro,
                'RTT': hop.RTT,
                'DNS': hop.DNS,
                'IP': hop.IP
            }
            hops.append(hop_d)
        route_d = {
            'User_ID': user_id,
            'Scan_ID': scan_id,
            'Target_ID': target.ID,
            'CreatedAt': route.CreatedAt,
            'items': hops
        }
        return route
