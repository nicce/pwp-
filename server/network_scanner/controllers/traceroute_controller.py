import connexion
import six

from network_scanner.models.route_info_get import RouteInfoGet  # noqa: E501
from network_scanner import util


def api_users_user_id_scans_scan_id_targets_target_id_route_delete(user_id, scan_id, target_id, accept=None):  # noqa: E501
    """Delete traceroute

    Deletes the route item # noqa: E501

    :param user_id: User identifier
    :type user_id: 
    :param scan_id: Scan identifier
    :type scan_id: 
    :param target_id: Target identifier
    :type target_id: 
    :param accept: e.g. application/vnd.mason+json
    :type accept: str

    :rtype: None
    """
    return 'do some magic!'


def api_users_user_id_scans_scan_id_targets_target_id_route_get(user_id, scan_id, target_id, accept=None):  # noqa: E501
    """Get traceroute information

    Retrieves the traceroute information. # noqa: E501

    :param user_id: User identifier
    :type user_id: 
    :param scan_id: Scan identifier
    :type scan_id: 
    :param target_id: Target identifier
    :type target_id: 
    :param accept: e.g. application/vnd.mason+json
    :type accept: str

    :rtype: InlineResponse20013
    """
    return 'do some magic!'
