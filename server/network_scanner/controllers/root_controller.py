from network_scanner.models.generic_response import GenericResponse
from network_scanner.new_models.Controls import Controls
from network_scanner.masonBuilderUtil import masonBuilderUtil
from network_scanner.uri_conf import ROOT


def api_get(accept=None):  # noqa: E501
    """
    Get controls from the root of the server.

    :param accept: e.g. application/vnd.mason+json
    :type accept: str

    :rtype: GenericResponse
    """
    # Response object
    SuccessfulResponse = GenericResponse()
    # Generate controls
    responseControls = Controls()
    responseControls.addAddUserControl()
    responseControls.addAllUsersControl()
    responseControls.addSelfControl(ROOT)
    responseControls.addAuthControl()
    responseControls.addLogoutControl()

    # Fill response object
    SuccessfulResponse.controls = responseControls
    SuccessfulResponse.information = 'This is the root of the API. '\
        'Check controls for moving forward.'
    SuccessfulResponse.namespaces = masonBuilderUtil.create_namespace_obj()

    return SuccessfulResponse, 200
