import connexion
import six

from network_scanner.models.basic_auth_post import BasicAuthPost
from network_scanner.models.generic_response import GenericResponse
from network_scanner.new_models.Controls import Controls
from network_scanner.models.auth_session_response import AuthSessionResponse
from network_scanner.masonBuilderUtil import masonBuilderUtil
import logging
from network_scanner.db.DBInterface import DBInterface
from network_scanner import util
from network_scanner.uri_conf import AUTH_URI, ROOT, USERS_URI


def api_auth_get(accept=None):  # noqa: E501
    """Get login form

    Gets the login form information. 

    :param accept: e.g. application/vnd.mason+json
    :type accept: str

    :rtype: GenericResponse
    """
    # Create response object
    SuccessfulResponse = GenericResponse()
    # Create controls
    responseControls = Controls()
    responseControls.addSelfControl(AUTH_URI)
    responseControls.addAllUsersControl()
    responseControls.addLoginUserControl()
    responseControls.addUpControl(ROOT, 'Root of the API')

    # Fill response object
    SuccessfulResponse.controls = responseControls
    SuccessfulResponse.namespaces = masonBuilderUtil.create_namespace_obj()
    SuccessfulResponse.information = 'This route is used for logging in with'\
        ' the username and password. Please, check attached schema.'

    return SuccessfulResponse, 200


def api_auth_post(accept=None, body=None):  # noqa: E501
    """
    Validate login data. On succesful validation,
    temporal OAuth2 token will be generated. It should
    be used for requests. It is valid for one hour.

    Validates the login credentials # noqa: E501

    :param accept: e.g. application/vnd.mason+json
    :type accept: str
    :param body: 
    :type body: dict | bytes

    :rtype: None
    """

    logging.debug("Acquired authentication POST as JSON")
    # At this point, JSON is already validated against schema
    # based on OpenAPI spec
    if connexion.request.is_json:
        body = BasicAuthPost.from_dict(connexion.request.get_json())

        session = DBInterface.db_validateBasicAuth(
            body.user_name, body.password)
        response_body = AuthSessionResponse.from_dict(session)

        return response_body, 303, {'Location': '{}{}/'.format(
            USERS_URI, response_body.user_id), 'Authorization':
            'Bearer ' + response_body.session_token}
    else:
        # We should not get here, as far as JSON validator
        #  against OpenAPI works
        logging.debug("Authentication attempt was not JSON")
        raise UnsupportedMediaType("Not JSON!")
