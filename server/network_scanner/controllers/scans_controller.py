import connexion
import six

from network_scanner.models.edit_scan_or_target import EditScanOrTarget  # noqa: E501
# All scans model
from network_scanner.models.generic_collection_response import GenericCollectionResponse  # noqa: E501
from network_scanner.new_models.Controls import Controls
from network_scanner.db.DBInterface import DBInterface
# Single scan model
from network_scanner.models.scan_item_get_response import ScanItemGetResponse  # noqa: E501
from network_scanner import util
from sqlalchemy.exc import IntegrityError, OperationalError
from network_scanner.uri_conf import SCANS_URI, USERS_URI, PROFILE_SCAN
from network_scanner.masonBuilderUtil import masonBuilderUtil
from werkzeug.exceptions import InternalServerError


def api_users_user_id_scans_get(user_id, accept=None):  # noqa: E501
    """List all scans
    Get the list of user scans # noqa: E501

    :param user_id: User identifier
    :type user_id: 
    :param accept: e.g. application/vnd.mason+json
    :type accept: str

    :rtype: GenericCollectionResponse

    """

    try:
        scan_items = DBInterface.db_getAllScansOfUser(user_id)
    except OperationalError as e:
        raise InternalServerError(
            "Database broken... please try again later. : {}".format(e.args))
    else:
        SuccessfulResponse = GenericCollectionResponse()
        # Create controls for main response
        responseControls = Controls()
        responseControls.addNewScanControl(user_id)
        self_uri = '{}{}{}'.format(USERS_URI, user_id, SCANS_URI)
        up_uri = '{}{}/'.format(USERS_URI, user_id)
        responseControls.addSelfControl(self_uri)
        responseControls.addUpControl(up_uri, 'Current user.')
        responseControls.addLogoutControl()
        # Create 'items' collection in scans response
        scan_items_d = []
        for scan_item in scan_items:
            scan_item_d = ScanItemGetResponse.from_dict(scan_item)
            # Create controls for each scan item
            itemControls = Controls()
            itemControls.addSelfControl('{}{}{}{}/'
                                        .format(USERS_URI, user_id,
                                                SCANS_URI,
                                                scan_item_d.scan_id))
            itemControls.addProfileControl(PROFILE_SCAN)
            scan_item_d.controls = itemControls
            # Append single scan item to 'items' collection of response
            scan_items_d.append(scan_item_d)

        # Create final response
        SuccessfulResponse.controls = responseControls
        SuccessfulResponse.items = scan_items_d
        SuccessfulResponse.namespaces = masonBuilderUtil.create_namespace_obj()
        return SuccessfulResponse


def api_users_user_id_scans_scan_id_get(user_id, scan_id, accept=None):  # noqa: E501
    """Get scan information

    Retrieves the information of requested scan. # noqa: E501

    :param user_id: User identifier
    :type user_id: 
    :param scan_id: Scan identifier
    :type scan_id: 
    :param accept: e.g. application/vnd.mason+json
    :type accept: str

    :rtype: ScanItemGetResponse
    """
    try:
        scanresult = DBInterface.db_getSingleScanOfUser(user_id, scan_id)
    except OperationalError as e:
        raise InternalServerError(
            "Database broken... please try again later. : {}".format(e.args))
    else:
        SuccessfulResponse = ScanItemGetResponse.from_dict(scanresult)
        responseControls = Controls()
        responseControls.addSelfControl('{}{}{}{}/'
                                        .format(USERS_URI, user_id,
                                                SCANS_URI,
                                                SuccessfulResponse.scan_id))
        responseControls.addProfileControl(PROFILE_SCAN)
        up_uri = '{}{}{}'.format(USERS_URI, user_id, SCANS_URI)
        responseControls.addUpControl(up_uri, 'Scan collection.')
        responseControls.addLogoutControl()
        responseControls.addTargetsAllControl(user_id, scan_id)
        responseControls.addEditScanControl(user_id, scan_id)
        responseControls.addDeleteScanControl(user_id, scan_id)
        # Fill final response
        SuccessfulResponse.namespaces = masonBuilderUtil.create_namespace_obj()
        SuccessfulResponse.controls = responseControls
        return SuccessfulResponse


def api_users_user_id_scans_scan_id_patch(user_id, scan_id, accept=None, body=None):  # noqa: E501
    """Edit scan information

    Edit scan information # noqa: E501

    :param user_id: User identifier
    :type user_id: 
    :param scan_id: Scan identifier
    :type scan_id: 
    :param accept: e.g. application/vnd.mason+json
    :type accept: str
    :param body: 
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = EditScanOrTarget.from_dict(connexion.request.get_json())
        DBInterface.db_editSingleScan(body, user_id, scan_id)
    return None, 204


def api_users_user_id_scans_scan_id_delete(user_id, scan_id, accept=None):  # noqa: E501
    """Delete scan

    Deletes the scan, and all associated targets &amp; ports. # noqa: E501

    :param user_id: User identifier
    :type user_id: 
    :param scan_id: Scan identifier
    :type scan_id: 
    :param accept: e.g. application/vnd.mason+json
    :type accept: str

    :rtype: None
    """
    DBInterface.db_deleteSingleScan(user_id, scan_id)
    return None, 204
