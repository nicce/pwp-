from typing import List
import logging

from connexion import request
from network_scanner.new_models.ErrorResponse import ErrorResponse
from network_scanner.new_models.ErrorMessage import ErrorMessage401,\
    ErrorMessage403

from network_scanner.db.DBInterface import DBInterface
from sqlalchemy.exc import IntegrityError, OperationalError
from werkzeug.exceptions import Forbidden, InternalServerError, NotFound

from urllib.parse import unquote, urlparse
from pathlib import PurePosixPath


def info_from_basic(username, password, required_scopes):
    """
    NOTE: This method not in use.


    Check and retrieve authentication information from basic auth.
    Returned value will be passed in 'token_info' parameter of your operation
    function, if there is one.
    'sub' or 'uid' will be set in 'user' parameter of your operation function,
    if there is one.

    :param username login provided by Authorization header
    :type username: str
    :param password password provided by Authorization header
    :type password: str
    :param required_scopes Always None. Used for other authentication method
    :type required_scopes: None
    :return: Information attached to user or None if credentials are invalid
    or does not allow access to called API
    :rtype: dict | None
    """
    return {'uid': 'user_id'}


def info_from_oauth2(token) -> dict:
    """
    Validate and decode token.
    Returned value will be passed in 'token_info' parameter of your operation
    function, if there is one. 'sub' or 'uid' will be set in 'user' parameter
    of your operation function, if there is one.
    'scope' or 'scopes' will be passed to scope validation function.

    :param token Token provided by Authorization header
    :type token: str
    :return: Decoded token information or None if token is invalid
    :rtype: dict | None
    """
    logging.debug("Trying to validate token in security controller.")
    logging.debug("Token is {}".format(token))

    try:
        user = DBInterface.db_validateToken(token)
    except OperationalError as e:
        raise InternalServerError(str(e))
    else:
        if user:
            logging.debug("Token was valid.")
            logging.debug("UID is {} and admin status is {}".format(
                user.ID, user.isAdmin))

            # Request object
            # https://werkzeug.palletsprojects.com/en/0.15.x/wrappers/#werkzeug.wrappers.BaseRequest
            """
            Following implementation might not be the most secure
            It parses the userID from uri, and check if it matches
            the userID of token OpenAPI OAuth2 lacks dynamic scopes
            to my knowledge, cannot generate scope for each user, hence
            we have to parse the userID from the URI...
            More secure solution would be just check it in the each operation
            function but that includes lot of repetition. In that way
            we are not controlling everything from single place, and are more
            error prone for mistakes
            """
            URIuserID = None
            url = request.path
            uriLIST = PurePosixPath(
                unquote(urlparse(url).path))
            try:
                if len(uriLIST.parts) > 3:
                    if uriLIST.parts[1] == 'api' and uriLIST.parts[2]\
                            == 'users':
                        URIuserID = int(uriLIST.parts[3])
                        """
                        Following part is unnecessary in real life
                        We are giving precise information in learning purposes
                        How to know if something is not found, if actually
                        there is no access into it?
                        Or in best scenario, return always NotFound,
                        if forbidden, unauthorized or not found
                        """
                        if not DBInterface.db_getSingleUser(URIuserID):
                            raise NotFound("Accessed user not found.")
            except ValueError:
                logging.debug(
                    "Request did not try to access user dependant route.")

            scopes = {
                'scopes': ['admin', 'generic-user', 'specific-user']
                if user.isAdmin else ['generic-user'],
                'uid': user.ID
            }
            if URIuserID == user.ID:
                logging.debug("Adding user to 'specific-user' group.")
                scopes.get('scopes').append('specific-user')
            return scopes
        else:
            logging.debug("Token was invalid.")
            # Returning None leads to 401 Unauthorized response
            # - not valid token
            return None


def validate_scope_oauth2(required_scopes, token_scopes):
    """
    Validate required scopes are included in token scope

    :param required_scopes Required scope to access called API
    :type required_scopes: List[str]
    :param token_scopes Scope present in token
    :type token_scopes: List[str]
    :return: True if access to called API is allowed
    :rtype: bool
    """
    # Admin has access to everything
    if 'admin' in token_scopes:
        return True

    logging.debug("Trying to validate scopes...")
    logging.debug("Required scopes are {}".format(required_scopes))
    logging.debug("Granted scopes are {}".format(token_scopes))
    return set(required_scopes).issubset(set(token_scopes))
