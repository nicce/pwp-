import logging
from network_scanner.db.DBInterface import DBInterface
from network_scanner.models.generic_response import GenericResponse
from network_scanner.new_models.Controls import Controls
from sqlalchemy.exc import IntegrityError
from network_scanner.uri_conf import LOGOUT_URI, AUTH_URI
from network_scanner.masonBuilderUtil import masonBuilderUtil


def api_logout_post(token_info, accept=None, location=None):  # noqa: E501
    """
    Logout user route.
    Destroys all session tokens, related to owner of request bearer token.
    Afterwards basic auth or refresh token is required.

    User logout # noqa: E501

    :param accept: e.g. application/vnd.mason+json
    :type accept: str
    :param location: e.g. http://logout@yourserver.example.com/
    :type location: str

    :rtype: None
    """
    # Empty post method is not that bad practise
    # http://lists.w3.org/Archives/Public/ietf-http-wg/2010JulSep/0273.html
    uid = token_info.get("uid")
    try:
        DBInterface.db_destroyUserSession(uid)
    except IntegrityError as e:
        logging.debug("Session already non-existent.")
    finally:
        # Create response object
        successfulResponse = GenericResponse()
        # Create controls
        controls = Controls()
        controls.addSelfControl(LOGOUT_URI)
        controls.addAuthControl()
        # Fill response object
        successfulResponse.controls = controls
        successfulResponse.namespaces = masonBuilderUtil.create_namespace_obj()
        # Return response with redirect to auth route
        return successfulResponse, 303, {'Location': '{}'.format(AUTH_URI)}
