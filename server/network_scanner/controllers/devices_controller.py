import connexion
import six

from network_scanner.models.device_item_get_response import DeviceItemGetResponse  # noqa: E501
from network_scanner.models.generic_collection_response import GenericCollectionResponse  # noqa: E501
from network_scanner.models.device_item_get_response import DeviceItemGetResponse  # noqa: E501
from network_scanner import util
from network_scanner.models.edit_device_or_port import EditDeviceOrPort

from network_scanner.db.DBInterface import DBInterface
from flask import request
from sqlalchemy.exc import IntegrityError, OperationalError

from network_scanner.masonBuilderUtil import masonBuilderUtil as m

from network_scanner.new_models.ErrorResponse import ErrorResponse
from network_scanner.new_models.ErrorMessage import ErrorMessage500, \
    ErrorMessage404, ErrorMessage403
from network_scanner.new_models.Controls import Controls

from network_scanner.uri_conf import *
from werkzeug.exceptions import NotFound
import logging


def api_users_user_id_devices_device_id_delete(user_id, device_id, accept=None):  # noqa: E501
    """Delete device information

    Deletes device and everything related to it. # noqa: E501

    :param user_id: User identifier
    :type user_id: 
    :param device_id: Device identifier
    :type device_id: 
    :param accept: e.g. application/vnd.mason+json
    :type accept: str

    :rtype: None
    """

    DBInterface.db_deleteDevice(user_id=user_id, device_id=device_id)
    # Return response 204 with empty body
    return None, 204


def api_users_user_id_devices_device_id_get(user_id, device_id, accept=None):  # noqa: E501
    """Get device information

     # noqa: E501

    :param user_id: User identifier
    :type user_id: 
    :param device_id: Device identifier
    :type device_id: 
    :param accept: e.g. application/vnd.mason+json
    :type accept: str

    :rtype: DeviceItemGetResponse
    """
    # Check if device exist at first, to not create all controls for nothing
    device = DBInterface.db_getUserDeviceId(user_id, device_id)
    if not device:
        raise NotFound("Device as user {} for device id {} not found.".format(
            user_id, device_id))

    # building the common response
    SuccessfulResponse = DeviceItemGetResponse()
    # SuccessfulResponse = GenericCollectionResponse()
    SuccessfulResponse.namespaces = m.create_namespace_obj()
    deviceController = Controls()
    selfUri = '{}{}{}{}/'.format(
        USERS_URI, user_id, DEVICES_URI, device_id)
    deviceController.addSelfControl(selfUri)
    # scansUri = '{}{}{}'.format(USERS_URI, user_id, SCANS_URI)
    deviceController.addScansAllControl(user_id)
    upUri = '{}{}{}'.format(USERS_URI, user_id, DEVICES_URI)
    deviceController.addUpControl(upUri, "Up")
    deviceController.addDeleteDeviceControl(selfUri)
    deviceController.addEditDeviceControl(selfUri)
    deviceController.addProfileControl(PROFILE_DEVICE)
    portHistoryUri = '{}{}{}{}{}'.format(USERS_URI, user_id, DEVICES_URI,
                                         device_id, PORT_HISTORY_URI)
    deviceController.addDevicePortHistoryControl(portHistoryUri)
    deviceController.addLogoutControl()
    SuccessfulResponse.controls = deviceController
    # adding item properties to the response
    SuccessfulResponse.device_id = device[0].ID
    SuccessfulResponse.modified_at = device[0].ModifiedAt.strftime(
        "%Y-%m-%d %H:%M:%S")
    SuccessfulResponse.os = device[0].OS
    SuccessfulResponse.extra_info = device[0].ExtraInfo
    SuccessfulResponse.comment = device[0].Comment
    SuccessfulResponse.mac_address_64bit = device[0].MAC_Address_64_bit_
    # print("Device: " + str(device))

    return SuccessfulResponse


def api_users_user_id_devices_device_id_patch(user_id, device_id, accept=None, body=None):  # noqa: E501
    """Edit device information

    Edit device information # noqa: E501

    :param user_id: User identifier
    :type user_id: 
    :param device_id: Device identifier
    :type device_id: 
    :param accept: e.g. application/vnd.mason+json
    :type accept: str
    :param body: 
    :type body: dict | bytes

    :rtype: None
    """
    logging.debug("Acquired new target device PATCH as JSON")
    # At this point, JSON is already validated against schema
    # based on OpenAPI spec
    if connexion.request.is_json:
        body = EditDeviceOrPort.from_dict(connexion.request.get_json())
        try:
            DBInterface.db_editDevice(body,  user_id=user_id, device_id=device_id)
        except (IntegrityError) as e:
            # We are ginving a lot of information in response,
            # in general might be so good
            raise Conflict(e.args)
        else:
            return None, 204
    else:
        # We should not get here, as far as JSON validator
        #  against OpenAPI works
        logging.debug("Target device PATCH was not JSON")
        raise UnsupportedMediaType("Not JSON!")


def api_users_user_id_devices_get(user_id, accept=None, token_info=None):  # noqa: E501
    """Gets the list of devices

    Retrieves the information of devices. # noqa: E501

    :param user_id: User identifier
    :type user_id: 
    :param accept: e.g. application/vnd.mason+json
    :type accept: str

    :rtype: GenericCollectionResponse
    """

    devices = DBInterface.db_getUserDevices(user_id)
    # building the common response
    SuccessfulResponse = GenericCollectionResponse()
    SuccessfulResponse.namespaces = m.create_namespace_obj()
    devicesController = Controls()
    devicesUri = '{}{}{}'.format(USERS_URI, user_id, DEVICES_URI)
    devicesController.addSelfControl(devicesUri)
    devicesController.addLogoutControl()
    # scansUri = '{}{}{}'.format(USERS_URI, user_id, SCANS_URI)
    devicesController.addScansAllControl(user_id)
    upUri = '{}{}/'.format(USERS_URI, user_id)
    devicesController.addUpControl(upUri, "Up")
    SuccessfulResponse.controls = devicesController
    # building the items for the response
    items_with_hypermedia = []
    for device in devices:
        # print("Device: " + str(device))
        item = DeviceItemGetResponse()
        item.os = device["os"]
        item.extra_info = device["extra_info"]
        item.device_id = device["device_id"]
        item.comment = device["comment"]
        item.mac_address_64bit = device["mac_address_64bit"]
        item.modified_at = device["modified_at"]
        item.controls = Controls()
        selfCtrl = '{}{}{}{}/'.format(USERS_URI, user_id,
                                      DEVICES_URI, device.get("device_id"))
        item.controls.addSelfControl(selfCtrl)
        item.controls.addProfileControl(PROFILE_DEVICE)
        # print("item: " + str(item))
        items_with_hypermedia.append(item)
    SuccessfulResponse.items = items_with_hypermedia
    return SuccessfulResponse


def api_users_user_id_scans_scan_id_targets_target_id_device_delete(user_id, scan_id, target_id, accept=None):  # noqa: E501
    """Delete device information

    Delete device and ports related to it. # noqa: E501

    :param user_id: User&#39;s identification number
    :type user_id: int
    :param scan_id: Scan identification number
    :type scan_id: int
    :param target_id: Target identification number
    :type target_id: int
    :param accept: e.g. application/vnd.mason+json
    :type accept: str

    :rtype: None
    """
    DBInterface.db_deleteDevice(user_id=user_id, scan_id=scan_id, target_id=target_id)
    # Return response 204 with empty body
    return None, 204



def api_users_user_id_scans_scan_id_targets_target_id_device_get(user_id, scan_id, target_id, accept=None):  # noqa: E501
    """Get device information

    Get the device information # noqa: E501

    :param user_id: User&#39;s identification number
    :type user_id: int
    :param scan_id: Scan identification number
    :type scan_id: int
    :param target_id: Target identification number
    :type target_id: int
    :param accept: e.g. application/vnd.mason+json
    :type accept: str

    :rtype: DeviceItemGetResponse
    """
    # Check if device exist at first, to not create all controls for nothing
    device = DBInterface.db_getTargetDevice(user_id, scan_id, target_id)
    if not device:
        raise NotFound("Device as user {} for scan id {} and "
                       "target id {} not found.".format(
                           user_id, scan_id, target_id))

    # building the common response
    SuccessfulResponse = DeviceItemGetResponse.from_dict(device)
    SuccessfulResponse.namespaces = m.create_namespace_obj()
    deviceController = Controls()
    selfUri = '{}{}{}{}{}{}{}'.format(
        USERS_URI, user_id, SCANS_URI, scan_id, TARGETS_URI,
        target_id, DEVICE_URI)
    deviceController.addSelfControl(selfUri)
    # scansUri = '{}{}{}'.format(USERS_URI, user_id, SCANS_URI)
    deviceController.addScansAllControl(user_id)
    upUri = '{}{}{}{}{}{}/'.format(
        USERS_URI, user_id, SCANS_URI, scan_id, TARGETS_URI,
        target_id)
    deviceController.addUpControl(upUri, "Up")
    deviceController.addDeleteDeviceControl(selfUri)
    deviceController.addEditDeviceControl(selfUri)
    deviceController.addProfileControl(PROFILE_DEVICE)
    deviceController.addLogoutControl()
    SuccessfulResponse.controls = deviceController
    # adding item properties to the response
    # SuccessfulResponse.modified_at = device[0]["modified_at"]
    # SuccessfulResponse.os = device[0]["os"]
    # SuccessfulResponse.extra_info = device[0]["extra_info"]
    # SuccessfulResponse.device_id = device[0]["device_id"]
    # SuccessfulResponse.comment = device[0]["comment"]
    # SuccessfulResponse.mac_address_64bit = device[0]["mac_address_64bit"]
    # print("Device: " + str(device))

    return SuccessfulResponse


def api_users_user_id_scans_scan_id_targets_target_id_device_patch(user_id, scan_id, target_id, accept=None, body=None):  # noqa: E501
    """Edit device information

    Edit device information # noqa: E501

    :param user_id: User&#39;s identification number
    :type user_id: int
    :param scan_id: Scan identification number
    :type scan_id: int
    :param target_id: Target identification number
    :type target_id: int
    :param accept: e.g. application/vnd.mason+json
    :type accept: str
    :param body: 
    :type body: dict | bytes

    :rtype: None
    """
    logging.debug("Acquired new target device PATCH as JSON")
    # At this point, JSON is already validated against schema
    # based on OpenAPI spec
    if connexion.request.is_json:
        body = EditDeviceOrPort.from_dict(connexion.request.get_json())
        try:
            DBInterface.db_editDevice(body,  user_id=user_id, scan_id=scan_id, target_id=target_id)
        except (IntegrityError) as e:
            # We are ginving a lot of information in response,
            # in general might not be so good
            raise Conflict(e.args)
        else:
            return None, 204
    else:
        # We should not get here, as far as JSON validator
        #  against OpenAPI works
        logging.debug("Target device PATCH was not JSON")
        raise UnsupportedMediaType("Not JSON!")
