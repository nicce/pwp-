import connexion
import six

from network_scanner.models.edit_device_or_port import EditDeviceOrPort  # noqa: E501
from network_scanner.models.port_item_get import PortItemGet  # noqa: E501
from network_scanner.models.generic_collection_response import GenericCollectionResponse  # noqa: E501
from network_scanner import util


def api_users_user_id_devices_device_id_port_history_get(user_id, device_id, accept=None):  # noqa: E501
    """Get port history

     # noqa: E501

    :param user_id: User identifier
    :type user_id: 
    :param device_id: Device identifier
    :type device_id: 
    :param accept: e.g. application/vnd.mason+json
    :type accept: str

    :rtype: InlineResponse2007
    """
    return 'do some magic!'


def api_users_user_id_scans_scan_id_targets_target_id_device_ports_get(user_id, scan_id, target_id, accept=None):  # noqa: E501
    """List all ports

     # noqa: E501

    :param user_id: User&#39;s identification number
    :type user_id: int
    :param scan_id: Scan identification number
    :type scan_id: int
    :param target_id: Target identification number
    :type target_id: int
    :param accept: e.g. application/vnd.mason+json
    :type accept: str

    :rtype: InlineResponse2005
    """
    return 'do some magic!'


def api_users_user_id_scans_scan_id_targets_target_id_device_ports_port_delete(user_id, scan_id, target_id, port, accept=None):  # noqa: E501
    """Delete port information

    Delete port information. # noqa: E501

    :param user_id: User&#39;s identification number
    :type user_id: int
    :param scan_id: Scan identification number
    :type scan_id: int
    :param target_id: Target identification number
    :type target_id: int
    :param port: Port number of open port
    :type port: int
    :param accept: e.g. application/vnd.mason+json
    :type accept: str

    :rtype: None
    """
    return 'do some magic!'


def api_users_user_id_scans_scan_id_targets_target_id_device_ports_port_get(user_id, scan_id, target_id, port, accept=None):  # noqa: E501
    """Get port information

     # noqa: E501

    :param user_id: User&#39;s identification number
    :type user_id: int
    :param scan_id: Scan identification number
    :type scan_id: int
    :param target_id: Target identification number
    :type target_id: int
    :param port: Port number of open port
    :type port: int
    :param accept: e.g. application/vnd.mason+json
    :type accept: str

    :rtype: InlineResponse20012
    """
    return 'do some magic!'


def api_users_user_id_scans_scan_id_targets_target_id_device_ports_port_patch(user_id, scan_id, target_id, port, accept=None, body=None):  # noqa: E501
    """Edit port information

    Edit port information # noqa: E501

    :param user_id: User&#39;s identification number
    :type user_id: int
    :param scan_id: Scan identification number
    :type scan_id: int
    :param target_id: Target identification number
    :type target_id: int
    :param port: Port number of open port
    :type port: int
    :param accept: e.g. application/vnd.mason+json
    :type accept: str
    :param body: 
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = EditPort.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'
