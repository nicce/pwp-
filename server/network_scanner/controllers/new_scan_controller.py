import connexion
import six

from network_scanner.models.generic_response import GenericResponse
from network_scanner.new_models.Controls import Controls
from network_scanner import util
from network_scanner.masonBuilderUtil import masonBuilderUtil
from network_scanner.uri_conf import USERS_URI, NEWSCAN_URI
from network_scanner.models.new_scan_post import NewScanPost
from network_scanner.db.DBInterface import DBInterface


def api_user_user_id_scans_new_scan_get(user_id, accept=None):  # noqa: E501
    """Acquire scan options information

     # noqa: E501

    :param user_id: User identifier
    :type user_id: 
    :param accept: e.g. application/vnd.mason+json
    :type accept: str

    :rtype GenericResponse
    """
    SuccessfulResponse = GenericResponse()
    # Create controls
    responseControls = Controls()
    responseControls.addSelfControl(
        '{}{}{}'.format(USERS_URI, user_id, NEWSCAN_URI))
    responseControls.addLogoutControl()
    responseControls.addScansAllControl(user_id)
    responseControls.addMakeNewScanControl(user_id)
    # Fill final response
    SuccessfulResponse.namespaces = masonBuilderUtil.create_namespace_obj()
    SuccessfulResponse.controls = responseControls
    SuccessfulResponse.detailed_information = {
        'description': 'Scan is based on the usage of NMAP.'
        ' For full list of options, check attached url.',
        'href': 'https://nmap.org/book/man-briefoptions.html'
    }
    return SuccessfulResponse, 200


def api_user_user_id_scans_new_scan_post(user_id, accept=None, body=None):  # noqa: E501
    """New scan

    Create new scan for user with user_id. # noqa: E501

    :param user_id: User identifier
    :type user_id: 
    :param accept: e.g. application/vnd.mason+json
    :type accept: str
    :param body: 
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = NewScanPost.from_dict(connexion.request.get_json())
        new_scan_id = DBInterface.db_addNewScan(body, user_id)
    return None, 201, {'Location': '{}{}{}{}/'.format(
            USERS_URI, user_id, NEWSCAN_URI, new_scan_id)}
