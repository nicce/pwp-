import connexion
import six

from network_scanner.models.edit_scan_or_target import EditScanOrTarget  # noqa: E501
from network_scanner.models.target_item_get import TargetItemGet  # noqa: E501
from network_scanner.models.generic_collection_response import\
    GenericCollectionResponse

from network_scanner.models.device_item_get_response import\
    DeviceItemGetResponse
from network_scanner.models.route_info_get import RouteInfoGet
from network_scanner.db.DBInterface import DBInterface
from network_scanner.masonBuilderUtil import masonBuilderUtil
from network_scanner.uri_conf import USERS_URI, SCANS_URI, TARGETS_URI,\
    PROFILE_TARGET, DEVICE_URI, PROFILE_DEVICE, ROUTE_URI, PROFILE_ROUTE
from network_scanner.new_models.Controls import Controls


def api_users_user_id_scans_scan_id_targets_get(user_id, scan_id, accept=None):  # noqa: E501
    """List the targets scanned during scan

    Get the list of all scanned targets # noqa: E501

    :param user_id: User identifier
    :type user_id: 
    :param scan_id: Scan identifier
    :type scan_id: 
    :param accept: e.g. application/vnd.mason+json
    :type accept: str

    :rtype: GenericCollectionResponse

    """
    SuccessfulResponse = GenericCollectionResponse()
    # Create controls
    ResponseControls = Controls()
    self_uri = '{}{}{}{}{}'.format(
        USERS_URI, user_id, SCANS_URI, scan_id, TARGETS_URI)
    up_uri = '{}{}{}{}/'.format(USERS_URI, user_id, SCANS_URI, scan_id)
    ResponseControls.addSelfControl(self_uri)
    ResponseControls.addUpControl(
        up_uri, 'Single scan with ID {} of current user'.format(scan_id))
    ResponseControls.addScansAllControl(user_id)
    ResponseControls.addLogoutControl()

    # Fill final response object
    SuccessfulResponse.controls = ResponseControls
    SuccessfulResponse.namespaces = masonBuilderUtil.create_namespace_obj()
    target_response_list = []
    # Get targets from database
    target_items = DBInterface.db_getAllTargetsOfSingleScanOfUser(
        user_id, scan_id)
    for target in target_items:
        target_d = TargetItemGet.from_dict(target)
        # Create controls of single target
        targetControls = Controls()
        targetControls.addProfileControl(PROFILE_TARGET)
        targetControls.addSelfControl('{}{}{}{}{}{}/'.format(
            USERS_URI, user_id, SCANS_URI, scan_id,
            TARGETS_URI, target_d.target_id))
        target_d.controls = targetControls
        # Add device for target
        device_from_db = DBInterface.db_getTargetDevice(user_id, scan_id,
                                                        target_d.target_id)
        # If there is device, add it into the response
        if device_from_db:
            device = DeviceItemGetResponse.from_dict(
                device_from_db)
            # Controls of device
            deviceControls = Controls()
            deviceControls.addSelfControl('{}{}{}{}{}{}{}'.format(
                USERS_URI, user_id, SCANS_URI, scan_id,
                TARGETS_URI, target_d.target_id, DEVICE_URI))
            deviceControls.addProfileControl(PROFILE_DEVICE)
            device.controls = deviceControls
            target_d.device = device
        # Add route for target
        route_from_db = DBInterface.db_getTargetRoute(
            user_id, scan_id, target_d.target_id)

        # If there is route, add it into response
        # NOTE Currently in example database, there are no routes
        if route_from_db:
            route = RouteInfoGet.from_dict(
                route_from_db)

            # Controls of route
            routeControls = Controls()
            routeControls.addSelfControl('{}{}{}{}{}{}{}'.format(
                USERS_URI, user_id, SCANS_URI, scan_id,
                TARGETS_URI, target_d.target_id, ROUTE_URI))
            routeControls.addProfileControl(PROFILE_ROUTE)
            route.controls = routeControls

        # Apped target into items list of final response
        target_response_list.append(target_d)

    SuccessfulResponse.items = target_response_list
    return SuccessfulResponse, 200


def api_users_user_id_scans_scan_id_targets_target_id_delete(user_id, scan_id, target_id, accept=None):  # noqa: E501
    """Delete target from the scan

    Delete target and routes related to it. If device exists only in this target result, it is deleted as well. # noqa: E501

    :param user_id: User identifier
    :type user_id: 
    :param scan_id: Scan identifier
    :type scan_id: 
    :param target_id: Target identifier
    :type target_id: 
    :param accept: e.g. application/vnd.mason+json
    :type accept: str

    :rtype: None
    """
    return 'do some magic!'


def api_users_user_id_scans_scan_id_targets_target_id_get(user_id, scan_id, target_id, accept=None):  # noqa: E501
    """Get information of single target

    Retrieves the information of single target. Information contains already device and route information, as they have one-to-one relationship.  + Relation self # noqa: E501

    :param user_id: User identifier
    :type user_id: 
    :param scan_id: Scan identifier
    :type scan_id: 
    :param target_id: Target identifier
    :type target_id: 
    :param accept: e.g. application/vnd.mason+json
    :type accept: str

    :rtype: InlineResponse20010
    """
    return 'do some magic!'


def api_users_user_id_scans_scan_id_targets_target_id_patch(user_id, scan_id, target_id, accept=None, body=None):  # noqa: E501
    """Edit information of single target

    Only two fields are editable - comment and isTagged field. # noqa: E501

    :param user_id: User identifier
    :type user_id: 
    :param scan_id: Scan identifier
    :type scan_id: 
    :param target_id: Target identifier
    :type target_id: 
    :param accept: e.g. application/vnd.mason+json
    :type accept: str
    :param body: 
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = EditScanOrTarget.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'
