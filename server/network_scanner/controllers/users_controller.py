from werkzeug.exceptions import BadRequest, InternalServerError, \
    UnsupportedMediaType, Conflict, NotFound
import logging
from network_scanner.uri_conf import USERS_URI, PROFILE_USER, AUTH_URI,\
    DEVICES_URI, SCANS_URI
from network_scanner.masonBuilderUtil import masonBuilderUtil as m
from network_scanner.new_models.Controls import Controls
from sqlalchemy.exc import IntegrityError, OperationalError,\
    InvalidRequestError
from flask import request
from network_scanner.db.DBInterface import DBInterface
from network_scanner import util
from network_scanner.models.user_get_response import UserGetResponse
import traceback
import connexion
import six

from network_scanner.models.user_post_put import UserPostPut  # noqa: E501
from network_scanner.models.accesstoken import Accesstoken
# Model for returning all users. # Note that 2003_controls
# contains control types
from network_scanner.models.generic_collection_response import\
    GenericCollectionResponse

# User GET model


def api_users_get(token_info, accept=None):
    """List all users

    :param user: Current userID based on OAauth token validation
    in security_controller
    :param accept: e.g. application/vnd.mason+json
    :type accept: str
    :rtype: GenericCollectionResponse
    """
    # Scopes of validated token
    scopes = token_info.get("scopes")
    # userID of validated token
    uid = token_info.get("uid")
    SuccessfulResponse = GenericCollectionResponse()
    users_with_hypermedia = []

    # Create namespace
    SuccessfulResponse.namespaces = m.create_namespace_obj()
    # Create controls
    usersController = Controls()
    usersController.addSelfControl(USERS_URI)
    usersController.addLogoutControl()
    usersController.addAddUserControl()
    SuccessfulResponse.controls = usersController
    if 'admin' in scopes:
        try:
            users_from_db = DBInterface.db_getAllUsers()
        except (OperationalError) as e:
            """
            Database out of memory, table not existing
            Or other error not related to code itself
            This can be tested for example by changing the name
            of the database file of SQLite
            It should be noted, that returning error in response can be
            classified as Information leakage vulneralibity
            """
            # Connexion catches Error exception
            # InternalServerError has been overwritten
            # in file ErrorOverrides.py
            # Response is in hypermedia format
            raise InternalServerError(str(e))
        else:
            # Create items
            for user in users_from_db:
                user_d = UserGetResponse.from_dict(user)
                user_d.controls = Controls()
                user_d.controls.addSelfControl(
                    '{}{}/'.format(USERS_URI, user_d.id))
                user_d.controls.addProfileControl(PROFILE_USER)
                users_with_hypermedia.append(user_d)
            SuccessfulResponse.items = users_with_hypermedia
            if not SuccessfulResponse.items:
                logging.warning(
                    'User is authenticated but it is not existing in the \
                    database, as user list is empty.')
    else:
        try:
            logging.debug("Not admin, returning single user.")
            current_user = DBInterface.db_getSingleUser(uid)
        except (OperationalError) as e:
            raise InternalServerError(str(e.args))
        else:
            user_d = UserGetResponse.from_dict(current_user)
            user_d.controls = Controls()
            user_d.controls.addSelfControl(
                '{}{}/'.format(USERS_URI, user_d.id))
            user_d.controls.addProfileControl(PROFILE_USER)
            users_with_hypermedia.append(user_d)
            SuccessfulResponse.items = users_with_hypermedia

    return SuccessfulResponse


def api_users_user_id_get(user_id, token_info, accept=None):  # noqa: E501
    """User information

    Get the user information # noqa: E501

    :param user_id: user's unique identification number
    :type user_id: int
    :param accept: e.g. application/vnd.mason+json
    :type accept: str

    :rtype: UserGetresponse 
    """
    try:
        logging.debug("Returning single user...")
        current_user = DBInterface.db_getSingleUser(user_id)
        if not current_user:
            raise NotFound("User with userID {} not found.".format(user_id))
    except (OperationalError) as e:
        # Connexion catches Error exception and provides
        # correct response message
        raise InternalServerError(str(e))
    else:
        user_d = UserGetResponse.from_dict(current_user)
        user_d.namespaces = m.create_namespace_obj()
        user_d.controls = Controls()
        user_d.controls.addSelfControl(
            '{}{}/'.format(USERS_URI, user_id))
        user_d.controls.addProfileControl(PROFILE_USER)
        user_d.controls.addUpControl(USERS_URI, "User collection")
        user_d.controls.addLogoutControl()
        user_d.controls.addEditUserControl(user_id)
        user_d.controls.addDeleteUserControl(user_id)
        user_d.controls.addScansAllControl(user_id)
        user_d.controls.addDevicesAllControl(user_id)
        user_d.controls.addNewScanControl(user_id)

    return user_d


def api_users_post(accept=None, body=None):  # noqa: E501
    """Add new user

    Add a new user for collection. The new user reprentation must be valid against user schema. Secret is required if non-admin user is trying to add new admin user. # noqa: E501

    :param accept: e.g. application/vnd.mason+json
    :type accept: str
    :param body: 
    :type body: dict | bytes

    :rtype: Accesstoken
    """
    logging.debug("Acquired new user POST as JSON")
    # At this point, JSON is already validated against schema
    # based on OpenAPI spec
    if connexion.request.is_json:
        body = UserPostPut.from_dict(connexion.request.get_json())
        auth_info, new_user_id = DBInterface.db_addNewUser(body)
        response_body = Accesstoken()
        response_body = response_body.from_dict(auth_info)
        return response_body, 201, {'Location': '{}{}/'.format(
            USERS_URI, new_user_id)}
    else:
        # We should not get here, as far as JSON validator
        #  against OpenAPI works
        logging.debug("New user post was not JSON")
        raise UnsupportedMediaType("Not JSON!")


def api_users_user_id_put(user_id, accept=None, body=None):
    """Edit user information

    Replace the user&#39;s information with new information. # noqa: E501

    :param user_id: user&#39;s unique identification number
    :type user_id: int
    :param accept: e.g. application/vnd.mason+json
    :type accept: str
    :param body: 
    :type body: dict | bytes

    :rtype: None
    """
    logging.debug("Acquired new user PUT as JSON")
    # At this point, JSON is already validated against schema
    # based on OpenAPI spec
    if connexion.request.is_json:
        body = UserPostPut.from_dict(connexion.request.get_json())
        DBInterface.db_editSingleUser(body,  user_id)
        return None, 204
    else:
        # We should not get here, as far as JSON validator
        #  against OpenAPI works
        logging.debug("User PUT was not JSON")
        raise UnsupportedMediaType("Not JSON!")


def api_users_user_id_delete(user_id, accept=None):  # noqa: E501
    """Delete this user

    Deletes current user and everything related to it. # noqa: E501

    :param user_id: user&#39;s unique identification number
    :type user_id: int
    :param accept: e.g. application/vnd.mason+json
    :type accept: str

    :rtype: None
    """
    # At this point authentication is confirmed already to delete user
    DBInterface.db_deleteSingleUser(user_id)

    # Return response 204 with empty body
    return None, 204
