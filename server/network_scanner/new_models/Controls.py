from typing import List, Dict
from network_scanner.models.base_model_ import Model
from network_scanner.masonBuilderUtil import masonBuilderUtil
from network_scanner.new_models.PostPutDeviceControl \
    import PostPutDeviceControl
from network_scanner.new_models.PostPutUserControl import PostPutUserControl
from network_scanner.new_models.MakeNewScanControl import MakeNewScanControl
from network_scanner.new_models.ResourceControl import ResourceControl
from network_scanner import util
from network_scanner.uri_conf import *
from network_scanner.new_models.SchemaGenerator import Schema


class Controls (Model):
    """
    Class for creating all required hypermedia controls for response.

    How it works:

    Each link relation is defined here on top level.
    For each link relation, there is getter and setter.
    There is additionally add* method for each relation, which adds
    corresponding control for current Control instance, by taking possible
    variable paramaters.

    This class extends Model class. Model class is used to convert this
    class object into dictionary or further into JSON. (and vice versa)

    Attribute map class attributes are used to map class attributes into
    correct describing attribute names (keys) in dictionary or JSON.

    Types attributes are used to define value presentation type of each 
    attribute in dictionary or in JSON. (eg. int vs float (1 vs 1.0))

    """

    def __init__(self, _self=None, profile=None, up=None, netscanauth=None,
                 netscanlogin_user=None,
                 netscanlogout_user=None, netscanusers_all=None,
                 netscanadd_user=None, netscanedit_user=None,
                 netscandelete_user=None, netscannew_scan=None,
                 netscanscans_all=None, netscanmake_new_scan=None,
                 netscanedit_scan=None,
                 netscandelete_scan=None, netscantargets_all=None,
                 netscanedit_target=None, netscandelete_target=None,
                 netscandevices_all=None,
                 netscanedit_device=None, netscandelete_device=None,
                 netscanporthistory_device=None):

        self.openapi_types = {
            '_self': object,
            'profile': object,
            'up': object,
            'netscanauth': object,
            'netscanlogin_user': object,
            'netscanlogout_user': object,
            'netscanusers_all': object,
            'netscanadd_user': PostPutUserControl,
            'netscanedit_user': PostPutUserControl,
            'netscandelete_user': ResourceControl,
            'netscannew_scan': object,
            'netscanscans_all': object,
            'netscanmake_new_scan': object,
            'netscanedit_scan': ResourceControl,
            'netscandelete_scan': ResourceControl,
            'netscantargets_all': object,
            'netscanedit_target': ResourceControl,
            'netscandelete_target': ResourceControl,
            'netscandevices_all': object,
            'netscanedit_device': PostPutDeviceControl,
            'netscandelete_device': ResourceControl,
            'netscanporthistory_device': object,
        }

        self.attribute_map = {
            '_self': 'self',
            'profile': 'profile',
            'up': 'up',
            'netscanauth': 'netscan:auth',
            'netscanlogin_user': 'netscan:login-user',
            'netscanlogout_user': 'netscan:logout-user',
            'netscanusers_all': 'netscan:users-all',
            'netscanadd_user': 'netscan:add-user',
            'netscanedit_user': 'netscan:edit-user',
            'netscandelete_user': 'netscan:delete-user',
            'netscannew_scan': 'netscan:new-scan',
            'netscanscans_all': 'netscan:scans-all',
            'netscanmake_new_scan': 'netscan:make-new-scan',
            'netscanedit_scan': 'netscan:edit-scan',
            'netscandelete_scan': 'netscan:delete-scan',
            'netscantargets_all': 'netscan:targets-all',
            'netscanedit_target': 'netscan:edit-target',
            'netscandelete_target': 'netscan:delete-target',
            'netscandevices_all': 'netscan:devices-all',
            'netscanedit_device': 'netscan:edit-device',
            'netscandelete_device': 'netscan:delete-device',
            'netscanporthistory_device': 'netscan:port-history',
        }

        self.__self = _self
        self._profile = profile
        self._up = up
        self._netscanauth = netscanauth
        self._netscanlogin_user = netscanlogin_user
        self._netscanlogout_user = netscanlogout_user
        self._netscanusers_all = netscanusers_all
        self._netscanadd_user = netscanadd_user
        self._netscanedit_user = netscanedit_user
        self._netscandelete_user = netscandelete_user
        self._netscannew_scan = netscannew_scan
        self._netscanscans_all = netscanscans_all
        self._netscanmake_new_scan = netscanmake_new_scan
        self._netscanedit_scan = netscanedit_scan
        self._netscandelete_scan = netscandelete_scan
        self._netscantargets_all = netscantargets_all
        self._netscanedit_target = netscanedit_target
        self._netscandelete_target = netscandelete_target
        self._netscandevices_all = netscandevices_all
        self._netscanedit_device = netscanedit_device
        self._netscandelete_device = netscandelete_device
        self._netscanporthistory_device = netscanporthistory_device

    @classmethod
    def from_dict(cls, dikt) -> 'Controls':
        """
        Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The Controls object of current response context.  
        :rtype: Controls object
        """
        return util.deserialize_model(dikt, cls)

    @property
    def _self(self):
        return self.__self

    @_self.setter
    def _self(self, _self):
        self.__self = _self

    def addSelfControl(self, href, title=None):
        """Sets the self of of controls in current response context.
        :param href: The URI of current resource.
        :param title: The title of current resource.
        :type href: string
        :type title: string
        """
        self.__self = masonBuilderUtil.create_generic_ref(
            href, title if title else 'The current location as URI')

    @property
    def profile(self):
        return self._profile

    @profile.setter
    def profile(self, profile):
        self._profile = profile

    def addProfileControl(self, href, title=None):
        """
        Adds the profile for controls, describing
        the profile of resource context.

        :param href: The URI of profile.
        :param title: The title of profile.
        :type href: string
        :type title: string
        """
        self._profile = masonBuilderUtil.create_generic_ref(
            href, title if title else 'The profile of the resource.')

    @property
    def up(self):
        return self._up

    @up.setter
    def up(self, up):
        self._up = up

    # def addUpControl(self, href, title):
    #     self._up = {
    #         "href": href,
    #         "title": title if title else 'Up'
    #     }

    def addUpControl(self, href, title):
        """Adds the up control for current response context.

        :param href: The URI of upper resource
        :param title: The title of upper resource
        :type href: string
        :type title: string
        """
        self._up = masonBuilderUtil.create_generic_ref(href, title)

    @property
    def netscanauth(self):
        return self._netscanauth

    @netscanauth.setter
    def netscanauth(self, netscanauth):
        self._netscanauth = netscanauth

    def addAuthControl(self, href=None, title=None):
        """
        Adds the auth control for controls.

        :param href: The uri of Auth route
        :param title: The title of auth route
        :type href: string
        :type title: string
        """
        self._netscanauth = masonBuilderUtil.create_generic_ref(
            href if href else '/api/auth/', title if title else 'User Login')

    @property
    def netscanlogin_user(self):
        return self._netscanlogin_user

    @netscanlogin_user.setter
    def netscanlogin_user(self, netscanlogin_user):
        self._netscanlogin_user = netscanlogin_user

    def addLoginUserControl(self):
        """
        Generate netscan:login-user control and schema for it.

        ResourceControl helper class have been used to generate schema.
        """
        self._netscanlogin_user = ResourceControl()
        self._netscanlogin_user.href = AUTH_URI
        self._netscanlogin_user.method = 'POST'
        self._netscanlogin_user.title = 'Validate login credentials.'
        # Generate shcmea
        temp_schema = Schema()
        temp_schema.addProperty('userName', 'User name')
        temp_schema.addProperty('password', 'User password')
        temp_schema.required = ['userName', 'password']
        self._netscanlogin_user.schema = temp_schema

    @property
    def netscanlogout_user(self):
        return self._netscanlogout_user

    @netscanlogout_user.setter
    def netscanlogout_user(self, netscanlogout_user):
        self._netscanlogout_user = netscanlogout_user

    def addLogoutControl(self, href=None, title=None):
        """
        Create 'netscan:logout-user' for controls instance.

        :param href: The uri of Logout
        :param title: The title of Logout
        :type href: string
        :type title: string
        """
        self._netscanlogout_user = {
            "href": href if href else '/api/logout/',
            "title": title if title else 'User Logout'
        }

    @property
    def netscanusers_all(self):
        return self._netscanusers_all

    @netscanusers_all.setter
    def netscanusers_all(self, netscanusers_all):
        self._netscanusers_all = netscanusers_all

    def addAllUsersControl(self, href=None, title=None):
        """
        Adds the 'netscan:all-users control for controls instance.

        :param href: The uri of all users
        :param title: The title of all users
        :type href: string
        :type title: string
        """
        self._netscanusers_all = {
            "href": href if href else '/api/users/',
            "title": title if title else 'All users.'
        }

    @property
    def netscanadd_user(self):
        return self._netscanadd_user

    @netscanadd_user.setter
    def netscanadd_user(self, netscanadd_user):
        self._netscanadd_user = netscanadd_user

    def addAddUserControl(self):
        """
        Adds control of 'netscan:add-user'.

        This control is generated with helper class
        'PostPutUserControl'.
        """
        self._netscanadd_user = PostPutUserControl()

    @property
    def netscanedit_user(self):
        return self._netscanedit_user

    @netscanedit_user.setter
    def netscanedit_user(self, netscanedit_user):
        self._netscanedit_user = netscanedit_user

    def addEditUserControl(self, user_id):
        """
        Adds control for 'netscan:edit-user' in current
        context.

        :param user_id: User id of the user
        """
        self._netscanedit_user = PostPutUserControl()
        self._netscanedit_user.href = '{}{}/'.format(USERS_URI, user_id)
        self._netscanedit_user.method = 'PUT'
        self._netscanedit_user.title = 'Edit this user.'

    @property
    def netscandelete_user(self):
        return self._netscandelete_user

    @netscandelete_user.setter
    def netscandelete_user(self, netscandelete_user):
        if netscandelete_user is None:
            raise ValueError("Invalid value for `netscandelete_user`, must not be `None`")  # noqa: E501

        self._netscandelete_user = netscandelete_user

    def addDeleteUserControl(self, user_id):
        """
        Adds 'netscan:delete-user' control for current controls instance.

        :param user_id: Identification number of user
        """
        self._netscandelete_user = ResourceControl()
        self._netscandelete_user.title = 'Delete this user.'
        self._netscandelete_user.href = '{}{}/'.format(USERS_URI, user_id)
        self._netscandelete_user.method = 'DELETE'

    @property
    def netscannew_scan(self):
        return self._netscannew_scan

    @netscannew_scan.setter
    def netscannew_scan(self, netscannew_scan):
        self._netscannew_scan = netscannew_scan

    def addNewScanControl(self, user_id, title=None):
        """
        Adds control for 'netscan:new-scan' in current
        context.

        :param user_id: Identification number of the user
        :param title: Alternative title of getting new scan information.
        """
        self._netscannew_scan = {
            "href": '{}{}{}'.format(USERS_URI, user_id, NEWSCAN_URI),
            "title": title if title else 'Information for making new scan.'
        }

    @property
    def netscanmake_new_scan(self):
        return self._netscanmake_new_scan

    @netscanmake_new_scan.setter
    def netscanmake_new_scan(self, netscanmake_new_scan):
        self._netscanmake_new_scan = netscanmake_new_scan

    def addMakeNewScanControl(self, user_id):
        """
        Adds control 'netscan:make-new-scan' for current controls instance.
        Previous NewScan control is to acquire information about scanning.

        This control describes the POST schema of making the scan.

        :param user_id: User indentification number of user.

        """
        self._netscanmake_new_scan = MakeNewScanControl(user_id)

    @property
    def netscanscans_all(self):
        return self._netscanscans_all

    @netscanscans_all.setter
    def netscanscans_all(self, netscanscans_all):
        self._netscanscans_all = netscanscans_all

    def addScansAllControl(self, user_id, title=None):
        """Sets the self of of controls in current response context.
        :param href: The URI of scans all resource.
        :param title: The title of scans all resource.
        :type href: string
        :type title: string
        """
        self._netscanscans_all = {
            "href": '{}{}{}'.format(USERS_URI, user_id, SCANS_URI),
            "title": title if title else 'All scan reports for current user.'
        }

    @property
    def netscanedit_scan(self):
        return self._netscanedit_scan

    @netscanedit_scan.setter
    def netscanedit_scan(self, netscanedit_scan):
        self._netscanedit_scan = netscanedit_scan

    def addEditScanControl(self, user_id, scan_id):
        """
        Adds control 'netscan:edit-scan' current controls instance.

        Uses helper class ResourceControl to generate JSON schema.

        :param user_id: User indentification number of the user.
        :param scan_id: Scan identification number of the scan.
        """

        self._netscanedit_scan = ResourceControl()
        self._netscanedit_scan.href = '{}{}{}{}/'\
            .format(USERS_URI,
                    user_id, SCANS_URI,
                    scan_id)
        self._netscanedit_scan.method = 'PATCH'
        self._netscanedit_scan.title = 'Edit this scan.'
        schema = Schema()
        schema.addProperty('comment', 'Scan comment')
        schema.addProperty(
            'isTagged', 'Is this scan favorite or not', 'boolean')
        schema.required = ['isTagged']
        self._netscanedit_scan.schema = schema

    @property
    def netscandelete_scan(self):
        return self._netscandelete_scan

    @netscandelete_scan.setter
    def netscandelete_scan(self, netscandelete_scan):
        self._netscandelete_scan = netscandelete_scan

    def addDeleteScanControl(self, user_id, scan_id):
        """
        Adds control 'netscan:delete-scan' for current controls instance

        :param user_id: Identification number of the user
        :param scan_id: Identification number of the scan
        """
        self._netscandelete_scan = ResourceControl()
        self._netscandelete_scan.href = '{}{}{}{}/'\
            .format(USERS_URI,
                    user_id, SCANS_URI,
                    scan_id)
        self._netscandelete_scan.method = 'DELETE'
        self._netscandelete_scan.title = 'Delete this scan.'

    @property
    def netscantargets_all(self):
        return self._netscantargets_all

    @netscantargets_all.setter
    def netscantargets_all(self, netscantargets_all):
        self._netscantargets_all = netscantargets_all

    def addTargetsAllControl(self, user_id, scan_id, title=None):
        """
        Generates 'netscan:targets-all' control in current context.

        :param user_id: Identification number of the user.
        :param scan_id: Identification number of the scan
        """
        self._netscantargets_all = {
            "href": '{}{}{}{}{}'.format(USERS_URI, user_id,
                                        SCANS_URI, scan_id, TARGETS_URI),
            "title": title if title else 'All targets found by'
            ' scanning by this user.'
        }

    @property
    def netscanedit_target(self):
        return self._netscanedit_target

    @netscanedit_target.setter
    def netscanedit_target(self, netscanedit_target):
        self._netscanedit_target = netscanedit_target

    def addEditTargetControl(self, user_id, scan_id, target_id):
        """
        Adds control 'netscan:edit-target' for current controls instance.
        Method uses helper class ResourceControl to generate JSON schema

        :param user_id: Identification number of the user
        :param scan_id; Identification number of the scan
        :param target_id: Identification number of the target
        """
        self._netscanedit_target = ResourceControl()
        self._netscanedit_target.href = '{}{}{}{}{}{}/'\
            .format(USERS_URI,
                    user_id, SCANS_URI,
                    scan_id, TARGETS_URI, target_id)
        self._netscanedit_target.method = 'PATCH'
        self._netscanedit_target.title = 'Edit this target.'
        schema = Schema()
        schema.addProperty('comment', 'Target comment')
        schema.addProperty(
            'isTagged', 'Is this target favorite or not', 'boolean')
        schema.required = ['isTagged']
        self._netscanedit_target.schema = schema

    @property
    def netscandelete_target(self):
        return self._netscandelete_target

    @netscandelete_target.setter
    def netscandelete_target(self, netscandelete_target):
        self._netscandelete_target = netscandelete_target

    def addDeleteTargetControl(self, user_id, scan_id):
        """
        Adds control 'netscan:delete-target' for current control instance
        Method uses helper class ResourceControl to generate JSON schema.

        :param user_id: Identification number of the user
        :param scan_id: Identification number of the scan
        """
        self._netscandelete_target = ResourceControl()
        self._netscandelete_target.href = '{}{}{}{}{}{}/'\
            .format(USERS_URI,
                    user_id, SCANS_URI,
                    scan_id, TARGETS_URI, target_id)
        self._netscandelete_target.method = 'DELETE'
        self._netscandelete_target.title = 'Delete this target.'

    @property
    def netscandevices_all(self):
        return self._netscandevices_all

    @netscandevices_all.setter
    def netscandevices_all(self, netscandevices_all):
        self._netscandevices_all = netscandevices_all

    def addDevicesAllControl(self, user_id, title=None):
        """
        Adds control 'netscan:devices-all' for current controls instance.

        :param user_id: Identification number of the user.
        """
        self._netscandevices_all = {
            "href": '{}{}{}'.format(USERS_URI, user_id,
                                    DEVICES_URI),
            "title": title if title else 'All devices found by'
            ' scanning by this user.'
        }

    @property
    def netscanedit_device(self):
        return self._netscanedit_device

    @netscanedit_device.setter
    def netscanedit_device(self, netscanedit_device):
        self._netscanedit_device = netscanedit_device

    def addEditDeviceControl(self, uri):
        """
        Adds control 'netscan:edit-device' for current controls instance.
        Uses helper class PostPutDeviceControl for generating JSON schema.

        :param uri: Uri for device to be edited.
        """
        self._netscanedit_device = PostPutDeviceControl()
        self._netscanedit_device.href = uri
        self._netscanedit_device.method = 'PATCH'
        self._netscanedit_device.title = 'Edit this device.'

    @property
    def netscandelete_device(self):
        return self._netscandelete_device

    @netscandelete_device.setter
    def netscandelete_device(self, netscandelete_device):
        if netscandelete_device is None:
            raise ValueError("Invalid value for `netscandelete_device`, must not be `None`")  # noqa: E501

        self._netscandelete_device = netscandelete_device

    def addDeleteDeviceControl(self, uri):
        """
        Adds 'netscan:delete-device' control for current control instance.
        Uses ResourceControl helper class to generate JSON schema.

        :param uri: Uri for device to be deleted.
        """
        self._netscandelete_device = ResourceControl()
        self._netscandelete_device.title = 'Delete this device.'
        self._netscandelete_device.href = uri
        self._netscandelete_device.method = 'DELETE'

    @property
    def netscanporthistory_device(self):
        return self._netscanporthistory_device

    @netscanporthistory_device.setter
    def netscanporthistory_device(self, netscanporthistory_device):
        self._netscanporthistory_device = netscanporthistory_device

    def addDevicePortHistoryControl(self, href, title=None):
        """
        Adds control 'netscan:port-history' for current controls instance.

        :param href: The URI of port history  resource.
        :param title: The title of port history resource.
        :type href: string
        :type title: string
        """
        self._netscanporthistory_device = {
            "href": href,
            "title": title if title else 'The port history of this device.'
        }
