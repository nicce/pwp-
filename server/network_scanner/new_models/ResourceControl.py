# coding: utf-8
from typing import List, Dict  # noqa: F401
from network_scanner.models.base_model_ import Model
from network_scanner.new_models.SchemaGenerator import Schema
from network_scanner import util


class ResourceControl(Model):
    """
    Interface class for POST, PUT, PATCH and DELETE hypermedia controls
    """

    def __init__(self, schema=None, method=None, href=None,
                 encoding=None, title=None):
        self.openapi_types = {
            'schema': Schema,
            'method': str,
            'href': str,
            'encoding': str,
            'title': str
        }

        self.attribute_map = {
            'schema': 'schema',
            'method': 'method',
            'href': 'href',
            'encoding': 'encoding',
            'title': 'title'
        }
        self._schema = schema
        self._method = method
        self._href = href
        self._encoding = encoding if encoding else 'json'
        self._title = title

    @classmethod
    def from_dict(cls, dikt) -> 'ResourceControl':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: Returns model of resource control
        :rtype: ResourceControl
        """
        return util.deserialize_model(dikt, cls)

    @property
    def schema(self):
        return self._schema

    @schema.setter
    def schema(self, schema):
        self._schema = schema

    def addSchema(self):
        self._schema = Schema()

    @property
    def method(self):
        return self._method

    @method.setter
    def method(self, method):
        if method is None:
            raise ValueError("Invalid value for `method`, must not be `None`")  # noqa: E501

        self._method = method

    @property
    def href(self):
        return self._href

    @href.setter
    def href(self, href):
        if href is None:
            raise ValueError("Invalid value for `href`, must not be `None`")  # noqa: E501

        self._href = href

    @property
    def encoding(self):
        return self._encoding

    @encoding.setter
    def encoding(self, encoding):
        if encoding is None:
            raise ValueError("Invalid value for `encoding`, must not be `None`")  # noqa: E501

        self._encoding = encoding

    @property
    def title(self):
        return self._title

    @title.setter
    def title(self, title):
        if title is None:
            raise ValueError("Invalid value for `title`, must not be `None`")  # noqa: E501

        self._title = title
