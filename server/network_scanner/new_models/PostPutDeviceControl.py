# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401
from network_scanner.models.base_model_ import Model
from network_scanner.uri_conf import *
from network_scanner.new_models.SchemaGenerator import Schema
from network_scanner.new_models.ResourceControl import ResourceControl
from network_scanner import util


class PostPutDeviceControl(ResourceControl):

    """
    Class for creating edit-device control.

    To use control for PATCH request, just use setter to change href, method and title.
    """

    def __init__(self, **kwargs):
        super(PostPutDeviceControl, self).__init__(**kwargs)
        self.addSchema()
        # self._method = 'PATCH'
        # self._href = '{}{}{}{}{}{}{}'.format(USERS_URI, user_id, SCANS_URI, scan_id, TARGETS_URI, target_id, DEVICE_URI)
        self._encoding = 'json'
        # self._title = 'Edit this device.'

    def addSchema(self):
        self._schema = Schema()
        self._schema.required = [
            "comment"
        ]
        self._schema.addProperty('comment', 'Device comment.')
