# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401
from network_scanner.models.base_model_ import Model
from network_scanner.uri_conf import USERS_URI
from network_scanner.new_models.SchemaGenerator import Schema
from network_scanner.new_models.ResourceControl import ResourceControl
from network_scanner import util


class PostPutUserControl(ResourceControl):

    """
    Class for creating add-user control.

    To use control for PUT request, just use setter
    to change href, method and title.
    """

    def __init__(self, **kwargs):
        super(PostPutUserControl, self).__init__(**kwargs)
        self.addSchema()
        self._method = 'POST'
        self._href = USERS_URI
        self._encoding = 'json'
        self._title = 'Add a new user for collection.'

    def addSchema(self):
        self._schema = Schema()
        self._schema.required = [
            "Username",
            "Email",
            "NewPassword",
            "RepeatNewPassword"
        ]
        self._schema.addProperty('Username', 'Username of new user.'
                                 ' Can contain only ascii letters, numbers,'
                                 ' hyphens and underscores.',
                                 pattern='^[a-zA-Z0-9_.-]+$')
        self._schema.addProperty(
            'isAdmin', 'Tells if user is admin.', 'boolean')
        self._schema.addProperty(
            'Secret', 'Secret is required, if isAdmin is set'
            ' true. (New admin to be added)')
        self._schema.addProperty(
            'Email',   'Email address of the user. Must be in valid format.'
            ' Note double backslashes in pattern, another have to be removed'
            ' in actual use.', pattern='[\w-]+@([\w-] +\.)+[\w-] +')
        self._schema.addProperty('FirstName', 'First name of the user.')
        self._schema.addProperty('Surname', 'Surname of the user.')
        self._schema.addProperty('NewPassword', 'Give new password.')
        self._schema.addProperty(
            'RepeatNewPassword', 'Confirmation that same password'
            ' is given again.')
