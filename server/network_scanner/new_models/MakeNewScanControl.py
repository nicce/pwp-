# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401
from network_scanner.models.base_model_ import Model
from network_scanner.uri_conf import USERS_URI, NEWSCAN_URI
from network_scanner.new_models.SchemaGenerator import Schema
from network_scanner.new_models.ResourceControl import ResourceControl
from network_scanner import util


class MakeNewScanControl(ResourceControl):

    """
    Class for creating make-new-scan control.

    """

    def __init__(self, user_id, **kwargs):
        super(MakeNewScanControl, self).__init__(**kwargs)
        self.addSchema()
        self._method = 'POST'
        self._href = '{}{}{}'.format(USERS_URI, user_id, NEWSCAN_URI)
        self._encoding = 'json'
        self._title = 'Add a new scan for this user.'

    def addSchema(self):
        self._schema = Schema()
        self._schema.required = [
            "isSingleTarget",
            "target"
        ]
        self._schema.addProperty(
            'isSingleTarget', 'Describes if user is going to scan one target'
            ' or many.', 'boolean')
        self._schema.addProperty(
            'target', 'The domain address, IP -address or IP address range')
        self._schema.addProperty(
            'optionsForTargets', 'Give options to be run for given'
            ' address(es).')
        self._schema.addProperty(
            'arbitraryCommand',   'Give an arbitary command to be executed'
            ' in NMAP. Other details will be ignored from this JSON.')
        self._schema.addProperty(
            'arbitraryCommandInTxtFile', 'Attach base64 - encoded txt binary'
            ' file to be executed as command. Bybasses other options.')
