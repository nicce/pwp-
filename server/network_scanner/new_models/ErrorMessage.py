from network_scanner.models.base_model_ import Model
from typing import List, Dict


class ErrorMessage(Model):
    """
    Class for easy reuse of different kind of error responses 
    """

    def __init__(self, message=None, messages=None):
        self._description = 'Default error response model.'
        self._message = message if message else ""
        self._messages = messages if messages else []

        self.openapi_types = {
            'message': str,
            'messages': List[object]
        }

        self.attribute_map = {
            'message': '@message',
            'messages': '@messages'
        }

    @property
    def message(self):
        """
        Returns message of the object - Getter
        """
        return self._message

    @message.setter
    def message(self, message):
        """
        Sets message of the object - Setter
        """
        self._message = message

    @property
    def messages(self):
        """
        Returns messages of the object - Getter
        """
        return self._messages

    @messages.setter
    def messages(self, messages):
        """
        Sets messages of the object - Setter
        """
        self._messages = messages

    def appendMessage(self, message):
        """
        Add a message into the messages
        """
        self._messages.append(message)


class ErrorMessage400(ErrorMessage):
    """
    Response message generator for HTTP response code 400
    """

    def __init__(self, message=None, messages=None, **kwargs):
        super(ErrorMessage400, self).__init__(**kwargs)
        self._description = ' Response message generator class for HTTP response code 400'
        self._message = message if message else "Invalid JSON document."
        self._messages = messages if messages else [
            "JSON failed to validy in schema. Schema can be seen in this response."]

class ErrorMessage401(ErrorMessage):
    """
    Response message generator for HTTP response code 401
    """

    def __init__(self, message=None, messages=None, **kwargs):
        super(ErrorMessage401, self).__init__(**kwargs)
        self._description = ' Response message generator class for HTTP response code 401'
        self._message = message if message else "Unauthorized"
        self._messages = messages if messages else [
            "The request lacked proper authorization. Please make sure, that password, username or authentication token is correct."]

class ErrorMessage403(ErrorMessage):
    """
    Response message generator for HTTP response code 401
    """

    def __init__(self, message=None, messages=None, **kwargs):
        super(ErrorMessage403, self).__init__(**kwargs)
        self._description = ' Response message generator class for HTTP response code 403'
        self._message = message if message else "Forbidden"
        self._messages = messages if messages else [
            "The client is not authorized to access this resource although it might have valid credentials. Contact for admin or change user."]

class ErrorMessage404(ErrorMessage):
    """
    Response message generator for HTTP response code 404
    """

    def __init__(self, message=None, messages=None, **kwargs):
        super(ErrorMessage404, self).__init__(**kwargs)
        self._description = ' Response message generator class for HTTP response code 404'
        self._message = message if message else "Not found"
        self._messages = messages if messages else [
            "The server did not find anything that matches the request URI. Verify that you are querying something correct."]

class ErrorMessage409(ErrorMessage):
    """
    Response message generator for HTTP response code 409
    """

    def __init__(self, message=None, messages=None, **kwargs):
        super(ErrorMessage409, self).__init__(**kwargs)
        self._description = ' Response message generator class for HTTP response code 409'
        self._message = message if message else "Conflict"
        self._messages = messages if messages else [
            "The request could not be completed because it conflicted with another request or the server's configuration. For example the object added with POST method could already exist."]

class ErrorMessage415(ErrorMessage):
    """
    Response message generator for HTTP response code 415
    """

    def __init__(self, message=None, messages=None, **kwargs):
        super(ErrorMessage415, self).__init__(**kwargs)
        self._description = ' Response message generator class for HTTP response code 409'
        self._message = message if message else "Unsupported Media Type"
        self._messages = messages if messages else [
            "The API cannot process the media type of the request payload. Use JSON."]

class ErrorMessage500(ErrorMessage):
    """
    Response message generator for HTTP response code 500
    """

    def __init__(self, message=None, messages=None, **kwargs):
        super(ErrorMessage500, self).__init__(**kwargs)
        self._description = ' Response message generator class for HTTP response code 500'
        self._message = message if message else "Internal Server Error"
        self._messages = messages if messages else [
            "Ooops... Something went wrong and we don't know what exactly."]
