from typing import List, Dict
from network_scanner.models.base_model_ import Model
from network_scanner.masonBuilderUtil import masonBuilderUtil
from network_scanner.new_models.ErrorMessage import ErrorMessage
from network_scanner import util
from network_scanner.new_models.Controls import Controls
from network_scanner.uri_conf import PROFILE_ERROR
from network_scanner.new_models.Controls import Controls


class ErrorResponse(Model):

    def __init__(self, error=None, resource_url=None,
                 controls=None, namespaces=None):

        self.openapi_types = {
            'error': ErrorMessage,
            'resource_url': str,
            'controls': Controls,
            'namespaces': object
        }

        self.attribute_map = {
            'error': '@error',
            'resource_url': 'resource_url',
            'controls': '@controls',
            'namespaces': '@namespaces'
        }

        self._error = error if error else ErrorMessage()
        self._resource_url = resource_url
        self._controls = controls if controls else Controls(
            profile=masonBuilderUtil.create_generic_ref(
                PROFILE_ERROR, 'The profile of the error messages.'))
        self._namespaces = namespaces if namespaces else \
            masonBuilderUtil.create_namespace_obj()

    @classmethod
    def from_dict(cls, dikt) -> 'ErrorResponse':
        """Returns the dict as a model
        """
        return util.deserialize_model(dikt, cls)

    @property
    def error(self):
        """Gets the error of this Error response.
        """
        return self._error

    @error.setter
    def error(self, error):
        """Sets the error of this Error response.
        """
        if error is None:
            raise ValueError("Invalid value for `error`, must not be `None`")  # noqa: E501

        self._error = error

    @property
    def resource_url(self):
        """Gets the resource_url of this Error response.
        """
        return self._resource_url

    @resource_url.setter
    def resource_url(self, resource_url):
        """Sets the resource_url of this Errro response.
        """
        if resource_url is None:
            raise ValueError("Invalid value for `resource_url`, must not be `None`")  # noqa: E501

        self._resource_url = resource_url

    @property
    def controls(self):
        """Gets the controls of this Error response.
        """
        return self._controls

    @controls.setter
    def controls(self, controls):
        """Sets the controls of this Error response.
        """

        self._controls = controls

    @property
    def namespaces(self):
        """Gets the namespaces of this Error response.
        """
        return self._namespaces

    @namespaces.setter
    def namespaces(self, namespaces):
        """Sets the namespaces of this Error response.
        """

        self._namespaces = namespaces
