# coding: utf-8
from typing import List, Dict  # noqa: F401

from network_scanner.models.base_model_ import Model
from network_scanner import util
from network_scanner.masonBuilderUtil import masonBuilderUtil


class Schema(Model):

    def __init__(self, type=None, properties=None, required=None):  # noqa: E501

        self.openapi_types = {
            'type': str,
            'properties': object,
            'required': List[object]
        }

        self.attribute_map = {
            'type': 'type',
            'properties': 'properties',
            'required': 'required'
        }

        self._type = type if type else 'object'
        self._properties = properties if properties else {}
        self._required = required if required else []

    @classmethod
    def from_dict(cls, dikt) -> 'Schema':
        return util.deserialize_model(dikt, cls)

    @property
    def type(self):
        return self._type

    @type.setter
    def type(self, type):
        if type is None:
            raise ValueError("Invalid value for `type`, must not be `None`")  # noqa: E501

        self._type = type

    @property
    def properties(self):
        return self._properties

    @properties.setter
    def properties(self, properties):
        self._properties = properties

    def addProperty(self, name, description, type=None, pattern=None):
        """ Add a single property into the schema

        :param name: Name of the propery
        :param description: Description of the property
        :param type: type of the propery
        :param pattern: Validation pattern for propery
        :type name: string
        :type description: string
        :type type: string
        :type pattern: string
        """

        _property = masonBuilderUtil.create_single_property(
            description, type, pattern)
        self._properties[name] = _property

    @property
    def required(self):
        return self._required

    @required.setter
    def required(self, required):
        self._required = required
