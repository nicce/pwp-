# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from network_scanner.models.base_model_ import Model
from network_scanner import util
from network_scanner.new_models.Controls import Controls


class GenericCollectionResponse(Model):
    """
    Response model for collections.
    """

    def __init__(self, controls=None, namespaces=None, items=None):  # noqa: E501
        """GenericCollectionResponse - a model defined in OpenAPI

        :param controls: The controls of this GenericCollectionResponse.  # noqa: E501
        :type controls: GenericCollectionResponseControls
        :param namespaces: The namespaces of this GenericCollectionResponse.  # noqa: E501
        :type namespaces: InlineResponse200Namespaces
        :param items: The items of this GenericCollectionResponse.  # noqa: E501
        :type items: List[object]
        """
        self.openapi_types = {
            'controls': Controls,
            'namespaces': object,
            'items': List[object]
        }

        self.attribute_map = {
            'controls': '@controls',
            'namespaces': '@namespaces',
            'items': 'items'
        }

        self._controls = controls
        self._namespaces = namespaces
        self._items = items

    @classmethod
    def from_dict(cls, dikt) -> 'GenericCollectionResponse':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The inline_response_200_3 of this GenericCollectionResponse.  # noqa: E501
        :rtype: GenericCollectionResponse
        """
        return util.deserialize_model(dikt, cls)

    @property
    def controls(self):
        """Gets the controls of this GenericCollectionResponse.


        :return: The controls of this GenericCollectionResponse.
        :rtype: GenericCollectionResponseControls
        """
        return self._controls

    @controls.setter
    def controls(self, controls):
        """Sets the controls of this GenericCollectionResponse.


        :param controls: The controls of this GenericCollectionResponse.
        :type controls: GenericCollectionResponseControls
        """

        self._controls = controls

    @property
    def namespaces(self):
        """Gets the namespaces of this GenericCollectionResponse.


        :return: The namespaces of this GenericCollectionResponse.
        :rtype: InlineResponse200Namespaces
        """
        return self._namespaces

    @namespaces.setter
    def namespaces(self, namespaces):
        """Sets the namespaces of this GenericCollectionResponse.


        :param namespaces: The namespaces of this GenericCollectionResponse.
        :type namespaces: InlineResponse200Namespaces
        """

        self._namespaces = namespaces

    @property
    def items(self):
        """Gets the items of this GenericCollectionResponse.


        :return: The items of this GenericCollectionResponse.
        :rtype: List[object]
        """
        return self._items

    @items.setter
    def items(self, items):
        """Sets the items of this GenericCollectionResponse.


        :param items: The items of this GenericCollectionResponse.
        :type items: List[object]
        """

        self._items = items
