# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401
from network_scanner.new_models.Controls import Controls
from network_scanner.models.base_model_ import Model
from network_scanner import util


class RouteInfoGet(Model):
    """NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).

    Do not edit the class manually.
    """

    def __init__(self, scan_id=None, controls=None, created_at=None, namespaces=None, target_id=None, user_id=None, items=None):  # noqa: E501
        """RouteInfoGet - a model defined in OpenAPI

        :param scan_id: The scan_id of this RouteInfoGet.  # noqa: E501
        :type scan_id: int
        :param controls: The controls of this RouteInfoGet.  # noqa: E501
        :type controls: Controls
        :param created_at: The created_at of this RouteInfoGet.  # noqa: E501
        :type created_at: str
        :param namespaces: The namespaces of this RouteInfoGet.  # noqa: E501
        :type namespaces: object
        :param target_id: The target_id of this RouteInfoGet.  # noqa: E501
        :type target_id: int
        :param user_id: The user_id of this RouteInfoGet.  # noqa: E501
        :type user_id: int
        :param items: The items of this RouteInfoGet.  # noqa: E501
        :type items: List[object]
        """
        self.openapi_types = {
            'scan_id': int,
            'controls': Controls,
            'created_at': str,
            'namespaces': object,
            'target_id': int,
            'user_id': int,
            'items': List[object]
        }

        self.attribute_map = {
            'scan_id': 'Scan_ID',
            'controls': '@controls',
            'created_at': 'CreatedAt',
            'namespaces': '@namespaces',
            'target_id': 'Target_ID',
            'user_id': 'User_ID',
            'items': 'items'
        }

        self._scan_id = scan_id
        self._controls = controls
        self._created_at = created_at
        self._namespaces = namespaces
        self._target_id = target_id
        self._user_id = user_id
        self._items = items

    @classmethod
    def from_dict(cls, dikt) -> 'RouteInfoGet':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The inline_response_200_13 of this RouteInfoGet.  # noqa: E501
        :rtype: RouteInfoGet
        """
        return util.deserialize_model(dikt, cls)

    @property
    def scan_id(self):
        """Gets the scan_id of this RouteInfoGet.

        Number of scan, where route is related to.  # noqa: E501

        :return: The scan_id of this RouteInfoGet.
        :rtype: float
        """
        return self._scan_id

    @scan_id.setter
    def scan_id(self, scan_id):
        """Sets the scan_id of this RouteInfoGet.

        Number of scan, where route is related to.  # noqa: E501

        :param scan_id: The scan_id of this RouteInfoGet.
        :type scan_id: float
        """
        if scan_id is None:
            raise ValueError("Invalid value for `scan_id`, must not be `None`")  # noqa: E501

        self._scan_id = scan_id

    @property
    def controls(self):
        """Gets the controls of this RouteInfoGet.


        :return: The controls of this RouteInfoGet.
        :rtype: RouteInfoGetControls
        """
        return self._controls

    @controls.setter
    def controls(self, controls):
        """Sets the controls of this RouteInfoGet.


        :param controls: The controls of this RouteInfoGet.
        :type controls: RouteInfoGetControls
        """

        self._controls = controls

    @property
    def created_at(self):
        """Gets the created_at of this RouteInfoGet.

        Timestamp when route was added.`  # noqa: E501

        :return: The created_at of this RouteInfoGet.
        :rtype: str
        """
        return self._created_at

    @created_at.setter
    def created_at(self, created_at):
        """Sets the created_at of this RouteInfoGet.

        Timestamp when route was added.`  # noqa: E501

        :param created_at: The created_at of this RouteInfoGet.
        :type created_at: str
        """
        if created_at is None:
            raise ValueError("Invalid value for `created_at`, must not be `None`")  # noqa: E501

        self._created_at = created_at

    @property
    def namespaces(self):
        """Gets the namespaces of this RouteInfoGet.


        :return: The namespaces of this RouteInfoGet.
        :rtype: InlineResponse200Namespaces
        """
        return self._namespaces

    @namespaces.setter
    def namespaces(self, namespaces):
        """Sets the namespaces of this RouteInfoGet.


        :param namespaces: The namespaces of this RouteInfoGet.
        :type namespaces: InlineResponse200Namespaces
        """

        self._namespaces = namespaces

    @property
    def target_id(self):
        """Gets the target_id of this RouteInfoGet.

        Number of target, where route is related to.  # noqa: E501

        :return: The target_id of this RouteInfoGet.
        :rtype: float
        """
        return self._target_id

    @target_id.setter
    def target_id(self, target_id):
        """Sets the target_id of this RouteInfoGet.

        Number of target, where route is related to.  # noqa: E501

        :param target_id: The target_id of this RouteInfoGet.
        :type target_id: float
        """
        if target_id is None:
            raise ValueError("Invalid value for `target_id`, must not be `None`")  # noqa: E501

        self._target_id = target_id

    @property
    def user_id(self):
        """Gets the user_id of this RouteInfoGet.

        Number or user, where route is related to.  # noqa: E501

        :return: The user_id of this RouteInfoGet.
        :rtype: float
        """
        return self._user_id

    @user_id.setter
    def user_id(self, user_id):
        """Sets the user_id of this RouteInfoGet.

        Number or user, where route is related to.  # noqa: E501

        :param user_id: The user_id of this RouteInfoGet.
        :type user_id: float
        """
        if user_id is None:
            raise ValueError("Invalid value for `user_id`, must not be `None`")  # noqa: E501

        self._user_id = user_id

    @property
    def items(self):
        """Gets the items of this RouteInfoGet.


        :return: The items of this RouteInfoGet.
        :rtype: List[object]
        """
        return self._items

    @items.setter
    def items(self, items):
        """Sets the items of this RouteInfoGet.


        :param items: The items of this RouteInfoGet.
        :type items: List[object]
        """
        if items is None:
            raise ValueError("Invalid value for `items`, must not be `None`")  # noqa: E501

        self._items = items
