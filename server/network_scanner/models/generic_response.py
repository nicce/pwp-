# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401
from network_scanner.new_models.Controls import Controls
from network_scanner.models.base_model_ import Model
from network_scanner import util


class GenericResponse(Model):
    """
    Generic response object, includes controls and namespace,
    additionally some extra attributes depending on response.
    """

    def __init__(self, controls=None, information=None, detailed_information=None, namespaces=None):  # noqa: E501
        """I - a model defined in OpenAPI

        :param controls: The controls of this GenericResponse.
        :type controls: Controls
        :param information: The information of this GenericResponse
        :type information: str
        :param namespaces: The namespaces of this GenericResponse
        :type namespaces: object
        """
        self.openapi_types = {
            'controls': Controls,
            'information': str,
            'detailed_information': object,
            'namespaces': object
        }

        self.attribute_map = {
            'controls': '@controls',
            'information': 'information',
            'detailed_information': 'detailedInformation',
            'namespaces': '@namespaces'
        }

        self._controls = controls
        self._information = information
        self._detailed_information = detailed_information
        self._namespaces = namespaces

    @classmethod
    def from_dict(cls, dikt) -> 'GenericResponse':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The inline_response_200_1 of this GenericResponse.  # noqa: E501
        :rtype: GenericResponse
        """
        return util.deserialize_model(dikt, cls)

    @property
    def controls(self):
        """Gets the controls of this GenericResponse.


        :return: The controls of this GenericResponse.
        :rtype: Controls
        """
        return self._controls

    @controls.setter
    def controls(self, controls):
        """Sets the controls of this GenericResponse.


        :param controls: The controls of this GenericResponse.
        :type controls: Controls
        """

        self._controls = controls

    @property
    def information(self):
        """Gets the information of this GenericResponse.


        :return: The information of this GenericResponse.
        :rtype: str
        """
        return self._information

    @information.setter
    def information(self, information):
        """Sets the information of this GenericResponse.


        :param information: The information of this GenericResponse.
        :type information: str
        """

        self._information = information

    @property
    def detailed_information(self):
        """Gets the detailed_information of this GenericResponse.


        :return: The detailed_information of this GenericResponse.
        :rtype: object
        """
        return self._detailed_information

    @detailed_information.setter
    def detailed_information(self, detailed_information):
        """Sets the detailed_information of this GenericResponse.


        :param detailed_information: The detailed_information of
        this GenericResponse.
        :type detailed_information: object
        """

        self._detailed_information = detailed_information

    @property
    def namespaces(self):
        """Gets the namespaces of this GenericResponse.


        :return: The namespaces of this GenericResponse.
        :rtype: InlineResponse200Namespaces
        """
        return self._namespaces

    @namespaces.setter
    def namespaces(self, namespaces):
        """Sets the namespaces of this GenericResponse.


        :param namespaces: The namespaces of this GenericResponse.
        :type namespaces: object
        """

        self._namespaces = namespaces
