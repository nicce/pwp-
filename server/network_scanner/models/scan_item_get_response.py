# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from network_scanner.models.base_model_ import Model
from network_scanner.new_models.Controls import Controls
from network_scanner import util


class ScanItemGetResponse(Model):
    """
    NOTE: This class is auto generated
    by OpenAPI Generator (https://openapi-generator.tech).

    Do not edit the class manually.
    """

    def __init__(self, modified_at=None, comment=None, user_id=None, is_tagged=None, scan_params=None, controls=None, created_at=None, scan_id=None, namespaces=None, scan_in_progress=None):  # noqa: E501
        """ScanItemGetResponse - a model defined in OpenAPI

        :param modified_at: The modified_at of this ScanItemGetResponse.  # noqa: E501
        :type modified_at: str
        :param comment: The comment of this ScanItemGetResponse.  # noqa: E501
        :type comment: str
        :param user_id: The user_id of this ScanItemGetResponse.  # noqa: E501
        :type user_id: int
        :param is_tagged: The is_tagged of this ScanItemGetResponse.  # noqa: E501
        :type is_tagged: bool
        :param scan_params: The scan_params of this ScanItemGetResponse.  # noqa: E501
        :type scan_params: str
        :param controls: The controls of this ScanItemGetResponse.  # noqa: E501
        :type controls: ScanItemGetResponseControls
        :param created_at: The created_at of this ScanItemGetResponse.  # noqa: E501
        :type created_at: str
        :param scan_id: The scan_id of this ScanItemGetResponse.  # noqa: E501
        :type scan_id: float
        :param namespaces: The namespaces of this ScanItemGetResponse.  # noqa: E501
        :type namespaces: object 
        :param scan_in_progress: The scan_in_progress of this ScanItemGetResponse.  # noqa: E501
        :type scan_in_progress: bool
        """
        self.openapi_types = {
            'modified_at': str,
            'comment': str,
            'user_id': int,
            'is_tagged': bool,
            'scan_params': str,
            'controls': Controls,
            'created_at': str,
            'scan_id': int,
            'namespaces': object,
            'scan_in_progress': bool
        }

        self.attribute_map = {
            'modified_at': 'ModifiedAt',
            'comment': 'Comment',
            'user_id': 'user_id',
            'is_tagged': 'IsTagged',
            'scan_params': 'ScanParams',
            'controls': '@controls',
            'created_at': 'CreatedAt',
            'scan_id': 'scan_id',
            'namespaces': '@namespaces',
            'scan_in_progress': 'ScanInProgress'
        }

        self._modified_at = modified_at
        self._comment = comment
        self._user_id = user_id
        self._is_tagged = is_tagged
        self._scan_params = scan_params
        self._controls = controls
        self._created_at = created_at
        self._scan_id = scan_id
        self._namespaces = namespaces
        self._scan_in_progress = scan_in_progress

    @classmethod
    def from_dict(cls, dikt) -> 'ScanItemGetResponse':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The inline_response_200_9 of this ScanItemGetResponse.  # noqa: E501
        :rtype: ScanItemGetResponse
        """
        return util.deserialize_model(dikt, cls)

    @property
    def modified_at(self):
        """Gets the modified_at of this ScanItemGetResponse.

        Time when modified  # noqa: E501

        :return: The modified_at of this ScanItemGetResponse.
        :rtype: str
        """
        return self._modified_at

    @modified_at.setter
    def modified_at(self, modified_at):
        """Sets the modified_at of this ScanItemGetResponse.

        Time when modified  # noqa: E501

        :param modified_at: The modified_at of this ScanItemGetResponse.
        :type modified_at: str
        """

        self._modified_at = modified_at

    @property
    def comment(self):
        """Gets the comment of this ScanItemGetResponse.

        User generated comment for this scan.  # noqa: E501

        :return: The comment of this ScanItemGetResponse.
        :rtype: str
        """
        return self._comment

    @comment.setter
    def comment(self, comment):
        """Sets the comment of this ScanItemGetResponse.

        User generated comment for this scan.  # noqa: E501

        :param comment: The comment of this ScanItemGetResponse.
        :type comment: str
        """

        self._comment = comment

    @property
    def user_id(self):
        """Gets the user_id of this ScanItemGetResponse.


        :return: The user_id of this ScanItemGetResponse.
        :rtype: float
        """
        return self._user_id

    @user_id.setter
    def user_id(self, user_id):
        """Sets the user_id of this ScanItemGetResponse.


        :param user_id: The user_id of this ScanItemGetResponse.
        :type user_id: float
        """
        if user_id is None:
            raise ValueError("Invalid value for `user_id`, must not be `None`")  # noqa: E501

        self._user_id = user_id

    @property
    def is_tagged(self):
        """Gets the is_tagged of this ScanItemGetResponse.

        `If true, tagged as \"favorite\" result`  # noqa: E501

        :return: The is_tagged of this ScanItemGetResponse.
        :rtype: bool
        """
        return self._is_tagged

    @is_tagged.setter
    def is_tagged(self, is_tagged):
        """Sets the is_tagged of this ScanItemGetResponse.

        `If true, tagged as \"favorite\" result`  # noqa: E501

        :param is_tagged: The is_tagged of this ScanItemGetResponse.
        :type is_tagged: bool
        """

        self._is_tagged = is_tagged

    @property
    def scan_params(self):
        """Gets the scan_params of this ScanItemGetResponse.

        Parameters which started this scan.  # noqa: E501

        :return: The scan_params of this ScanItemGetResponse.
        :rtype: str
        """
        return self._scan_params

    @scan_params.setter
    def scan_params(self, scan_params):
        """Sets the scan_params of this ScanItemGetResponse.

        Parameters which started this scan.  # noqa: E501

        :param scan_params: The scan_params of this ScanItemGetResponse.
        :type scan_params: str
        """

        self._scan_params = scan_params

    @property
    def controls(self):
        """Gets the controls of this ScanItemGetResponse.


        :return: The controls of this ScanItemGetResponse.
        :rtype: ScanItemGetResponseControls
        """
        return self._controls

    @controls.setter
    def controls(self, controls):
        """Sets the controls of this ScanItemGetResponse.


        :param controls: The controls of this ScanItemGetResponse.
        :type controls: ScanItemGetResponseControls
        """
        if controls is None:
            raise ValueError("Invalid value for `controls`, must not be `None`")  # noqa: E501

        self._controls = controls

    @property
    def created_at(self):
        """Gets the created_at of this ScanItemGetResponse.

        Time when created.  # noqa: E501

        :return: The created_at of this ScanItemGetResponse.
        :rtype: str
        """
        return self._created_at

    @created_at.setter
    def created_at(self, created_at):
        """Sets the created_at of this ScanItemGetResponse.

        Time when created.  # noqa: E501

        :param created_at: The created_at of this ScanItemGetResponse.
        :type created_at: str
        """

        self._created_at = created_at

    @property
    def scan_id(self):
        """Gets the scan_id of this ScanItemGetResponse.


        :return: The scan_id of this ScanItemGetResponse.
        :rtype: float
        """
        return self._scan_id

    @scan_id.setter
    def scan_id(self, scan_id):
        """Sets the scan_id of this ScanItemGetResponse.


        :param scan_id: The scan_id of this ScanItemGetResponse.
        :type scan_id: float
        """
        if scan_id is None:
            raise ValueError("Invalid value for `scan_id`, must not be `None`")  # noqa: E501

        self._scan_id = scan_id

    @property
    def namespaces(self):
        """Gets the namespaces of this ScanItemGetResponse.


        :return: The namespaces of this ScanItemGetResponse.
        :rtype: InlineResponse200Namespaces
        """
        return self._namespaces

    @namespaces.setter
    def namespaces(self, namespaces):
        """Sets the namespaces of this ScanItemGetResponse.


        :param namespaces: The namespaces of this ScanItemGetResponse.
        :type namespaces: InlineResponse200Namespaces
        """

        self._namespaces = namespaces

    @property
    def scan_in_progress(self):
        """Gets the scan_in_progress of this ScanItemGetResponse.

        Describes if scan is still running, and there are no results yet.  # noqa: E501

        :return: The scan_in_progress of this ScanItemGetResponse.
        :rtype: bool
        """
        return self._scan_in_progress

    @scan_in_progress.setter
    def scan_in_progress(self, scan_in_progress):
        """Sets the scan_in_progress of this ScanItemGetResponse.

        Describes if scan is still running, and there are no results yet.  # noqa: E501

        :param scan_in_progress: The scan_in_progress of this ScanItemGetResponse.
        :type scan_in_progress: bool
        """

        self._scan_in_progress = scan_in_progress
