from network_scanner.models.base_model_ import Model


class masonBuilderUtil ():

    def __init__(self):
        self.description = 'Util for helping in generating'
        'different kind of dictionaries'

    @staticmethod
    def create_namespace_obj(name=None):
        namespace_obj = {
            "netscan": {
                "name": name if name else '/netscan/link-relations#'
            }
        }
        return namespace_obj

    @staticmethod
    def create_generic_ref(href, title):
        generic_obj = {
            "href": href,
            "title": title
        }
        return generic_obj

    @staticmethod
    def create_single_property(_description, _type=None, pattern=None):
        """
        Method for generating properties in JSON schema
        """
        _property = {
            'description': _description,
            'type': _type if _type else 'string',
        }
        if pattern is not None:
            _property['pattern'] = pattern
        return _property
