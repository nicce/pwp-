#!/usr/bin/env python3
from network_scanner.config import initConnexionApp


def main():
    ConnexionWrapper = initConnexionApp()
    ConnexionWrapper.addErrorHandlers()
    ConnexionWrapper.configFlaskApp()
    ConnexionWrapper.initApiConf()
    ConnexionWrapper.initDatabase()
    ConnexionWrapper.start()


if __name__ == '__main__':
    main()
