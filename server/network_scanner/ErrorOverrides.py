import connexion
from flask import Response, json
from connexion import request
from network_scanner.new_models.ErrorResponse import ErrorResponse
from network_scanner.new_models.ErrorMessage import ErrorMessage400, ErrorMessage401, ErrorMessage403, ErrorMessage404, ErrorMessage409, ErrorMessage415, ErrorMessage500

from connexion.decorators.validation import RequestBodyValidator, TypeValidationError, coerce_type
from connexion.problem import problem
from jsonschema.validators import validator_for
from werkzeug.exceptions import InternalServerError, UnsupportedMediaType, BadRequest


import collections
import copy
import functools
import logging
import sys

import six
from jsonschema import Draft4Validator, ValidationError, draft4_format_checker
from connexion.exceptions import ExtraParameterProblem
from connexion.http_facts import FORM_CONTENT_TYPES
from connexion.json_schema import Draft4RequestValidator, Draft4ResponseValidator
from connexion.utils import all_json, boolean, is_json_mimetype, is_null, is_nullable

"""
This file overrides some Connexion's default error responses with hypermedia ones
These responses can be thrown by using werkzeug.exceptions 

eg. raise Forbidden(str(e)) causes connexion to return Forbidden 403 with
response body as defined below in method Forbidden403.
"""
logger = logging.getLogger('connexion.decorators.validation')


class CustomRequestBodyValidator(RequestBodyValidator):
    """
    Modified version of Connexion's default RequestBodyValidator, overriding some functions
    """

    def __call__(self, function):
        """
        :type function: types.FunctionType
        :rtype: types.FunctionType
        """

        @functools.wraps(function)
        def wrapper(request):
            if all_json(self.consumes):
                data = request.json

                empty_body = not(request.body or request.form or request.files)
                if data is None and not empty_body and not self.is_null_value_valid:
                    try:
                        ctype_is_json = is_json_mimetype(
                            request.headers.get("Content-Type", ""))
                    except ValueError:
                        ctype_is_json = False

                    if ctype_is_json:
                        # Content-Type is json but actual body was not parsed
                        raise BadRequest(
                            "Request body is not valid JSON even thought Content-Type header defined so.")
                    else:
                        # the body has contents that were not parsed as JSON
                        raise UnsupportedMediaType("Invalid Content-type ({content_type}), expected JSON data".format(
                            content_type=request.headers.get(
                                "Content-Type", "")
                        ))
                logger.debug("%s validating schema...", request.url)
                schema_d = {}
                schema_d["properties"] = self.schema.get("properties", {})
                schema_d["example"] = self.schema.get("example", {})
                schema_d["required"] = self.schema.get("required", {})
                self.validate_schema(data, request.url, schema_d)
            elif self.consumes[0] in FORM_CONTENT_TYPES:
                data = dict(request.form.items()) or (
                    request.body if len(request.body) > 0 else {})
                # validator expects string..
                data.update(dict.fromkeys(request.files, ''))
                logger.debug('%s validating schema...', request.url)

                if self.strict_validation:
                    formdata_errors = self.validate_formdata_parameter_list(
                        request)
                    if formdata_errors:
                        raise ExtraParameterProblem(formdata_errors, [])

                if data:
                    props = self.schema.get("properties", {})

                    errs = []
                    for k, param_defn in props.items():
                        if k in data:
                            try:
                                data[k] = coerce_type(
                                    param_defn, data[k], 'requestBody', k)
                            except TypeValidationError as e:
                                errs += [str(e)]
                                print(errs)
                    if errs:
                        raise BadRequest(errs)

                self.validate_schema(data, request.url, props)

            response = function(request)
            return response

        return wrapper

    def validate_schema(self, data, url, props=None):
        # type: (dict, AnyStr) -> Union[ConnexionResponse, None]
        if self.is_null_value_valid and is_null(data):
            return None
        try:
            self.validator.validate(data)
        except ValidationError as exception:
            logger.error("{url} validation error: {error}".format(url=url,
                                                                  error=exception.message),
                         extra={'validator': 'body'})
            raise BadRequest([exception.message, props if props else None])


def BadRequest400(exception):
    """
    This function overrides Connexion's default 400 response error
    see: https://connexion.readthedocs.io/en/latest/exceptions.html
    Instead of providing default connextion Bad Request 400 response, we are 
    providing custom made with hypermedia.
    :param exception: Exception thrown by json validation in controllers 
    :rtype: Response json with ErrorResponse 400
    """
    error400 = ErrorResponse()
    error400.error = ErrorMessage400()
    error400.resource_url = request.path
    error400.error.appendMessage(str(exception))
    return Response(
        response=json.dumps(error400),
        status=400,
        mimetype="application/vnd.mason+json"
    )


def Unauthorized401(exception):
    """
    This function overrides exception OAuthResponseProblem
    see: https://connexion.readthedocs.io/en/latest/exceptions.html
    Instead of providing default connextion Unauthorized 401 response, we are 
    providing custom made with hypermedia.

    :param exception: Exception thrown by OAuth validation (see security_controller.py)
    :rtype: Response json with ErrorResponse 401
    """
    error401 = ErrorResponse()
    error401.error = ErrorMessage401()
    error401.resource_url = request.path
    error401.error.appendMessage(str(exception))
    return Response(
        response=json.dumps(error401),
        status=401,
        mimetype="application/vnd.mason+json"
    )


def UnauthorizedNoToken401(exception):
    """
    This function overrides exception OAuthResponseProblem
    see: https://connexion.readthedocs.io/en/latest/exceptions.html
    Instead of providing default connextion Unauthorized 401 response, we are 
    providing custom made with hypermedia.

    :param exception: Exception thrown by OAuth validation (see security_controller.py)
    :rtype: Response json with ErrorResponse 401
    """
    error401 = ErrorResponse()
    error401.error = ErrorMessage401()
    error401.controls.addAuthControl()
    error401.controls.addAddUserControl()
    error401.resource_url = request.path
    error401.error.appendMessage(str(exception))
    return Response(
        response=json.dumps(error401),
        status=401,
        mimetype="application/vnd.mason+json"
    )


def Forbidden403(exception):
    """
    This function overrides exception OAuthScopeProblem
    see: https://connexion.readthedocs.io/en/latest/exceptions.html
    Instead of providing default connextion Forbidden 403 response, we are 
    providing custom made with hypermedia.
    :param exception: Exception thrown by OAuth validation (see security_controller.py)
    :rtype: Response json with ErrorResponse 403
    """
    error403 = ErrorResponse()
    error403.error = ErrorMessage403()
    error403.resource_url = request.path
    error403.error.appendMessage(str(exception))
    return Response(
        response=json.dumps(error403),
        status=403,
        mimetype="application/vnd.mason+json"
    )


def NotFound404(exception):
    """
    This function overrides Connexion's default 404 response error
    see: https://connexion.readthedocs.io/en/latest/exceptions.html
    Instead of providing default connextion Not Found 404 response, we are 
    providing custom made with hypermedia.
    :param exception: Exception thrown by not finding URI
    :rtype: Response json with ErrorResponse 404
    """
    error404 = ErrorResponse()
    error404.error = ErrorMessage404()
    error404.resource_url = request.path
    error404.error.appendMessage(str(exception))
    return Response(
        response=json.dumps(error404),
        status=404,
        mimetype="application/vnd.mason+json"
    )


def Conflict409(exception):
    """
    This function overrides Connexion's default Conflict 409 response error
    see: https://connexion.readthedocs.io/en/latest/exceptions.html
    Instead of providing default connextion Conflict 409 response, we are 
    providing custom made with hypermedia.
    :param exception: Exception thrown some conflict, eg. Posting new data that already exists
    :rtype: Response json with ErrorResponse 409
    """
    error409 = ErrorResponse()
    error409.error = ErrorMessage409()
    error409.resource_url = request.path
    error409.error.appendMessage(str(exception))
    return Response(
        response=json.dumps(error409),
        status=409,
        mimetype="application/vnd.mason+json"
    )


def UnsupportedMedia415(exception):
    """
    This function overrides Connexion default unsupported media 415 response
    see: https://connexion.readthedocs.io/en/latest/exceptions.html
    Instead of providing default connextion Unsupported Media 415 repsonse, we are 
    providing custom made with hypermedia.
    :param exception: Exception thrown by from wrong media type eg. not JSON 
    :rtype: Response json with ErrorResponse 415 
    """
    error415 = ErrorResponse()
    error415.error = ErrorMessage415()
    error415.resource_url = request.path
    error415.error.appendMessage(str(exception))
    return Response(
        response=json.dumps(error415),
        status=415,
        mimetype="application/vnd.mason+json"
    )


def InternalError500(exception):
    """
    This function overrides Connexion default server error 500 response
    see: https://connexion.readthedocs.io/en/latest/exceptions.html
    Instead of providing default connextion Forbidden 403 repsonse, we are 
    providing custom made with hypermedia.
    :param exception: Exception thrown by any unexcepted error (this should not every happen)
    :rtype: Response json with ErrorResponse 500
    """
    error500 = ErrorResponse()
    error500.error = ErrorMessage500()
    error500.resource_url = request.path
    error500.error.appendMessage(str(exception))
    return Response(
        response=json.dumps(error500),
        status=500,
        mimetype="application/vnd.mason+json"
    )
