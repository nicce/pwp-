import connexion
import os
import shutil

from network_scanner import encoder
from flask_sqlalchemy import SQLAlchemy
import logging
from logging.config import dictConfig

import re
from connexion.resolver import RestyResolver
from connexion.exceptions import OAuthResponseProblem, OAuthScopeProblem,\
    OAuthProblem, InvalidSpecification, NonConformingResponse, ResolverError,\
    ProblemException, ExtraParameterProblem
from jsonschema.exceptions import RefResolutionError, ValidationError

from connexion.problem import problem
from network_scanner.db.models import db
from network_scanner.ErrorOverrides import CustomRequestBodyValidator,\
    BadRequest400, Unauthorized401, UnauthorizedNoToken401, Forbidden403,\
    NotFound404, Conflict409, UnsupportedMedia415, InternalError500

ADMIN_SECRET = "I am a true Admin."
DEFAULT_TOKEN_EXPIRING_TIME = 3600


class initConnexionApp(object):
    """
    Wrapper class for connexion app with complex configurations.
    Required for testing purposes, to create multiple instances 
    of configurations.

    Regular configuration file shares same instance
    """

    def __init__(self, debug=False, _LOG_PATH_ERROR=None, _LOG_PATH_INFO=None):
        """
        Constructor for initConnexin app class

        :param debug: Defines if the logging level is DEBUG or not,
        when it is in INFO - level instead. In debug level,
        all logging messages will be shown as output and stored into file.
        """

        # Configure logging paths
        self.LOG_PATH_ERROR = _LOG_PATH_ERROR if _LOG_PATH_ERROR\
            else 'logs/event_api_error_log.log'
        self.LOG_PATH_INFO = _LOG_PATH_INFO if _LOG_PATH_INFO\
            else 'logs/event_api_info_log.log'

        # Logging configurations, we might want to separete
        # error messages into own file (WIP)
        dictConfig({
            'version': 1,
            'formatters': {'default': {
                'format': '[%(asctime)s] %(levelname)s in '
                '%(module)s: %(message)s',
            }},
            'handlers': {'wsgi': {
                'class': 'logging.StreamHandler',
                'level': 'DEBUG' if debug else 'INFO',
                'stream': 'ext://flask.logging.wsgi_errors_stream',
                'formatter': 'default'
            }, 'file_info': {
                'class': 'logging.FileHandler',
                'level': 'DEBUG' if debug else 'INFO',
                'formatter': 'default',
                'filename': self.LOG_PATH_INFO
            }, 'file_error': {
                'class': 'logging.FileHandler',
                'level': 'ERROR',
                'formatter': 'default',
                'filename': self.LOG_PATH_ERROR
            }
            },
            'loggers': {'file_info': {
                'level': 'DEBUG' if debug else 'INFO',
                'handlers': ['file_info'],
                'propagate': 'no'
            }, 'file_error': {
                'level': 'ERROR',
                'handlers': ['file_error'],
                'propagate': 'yes'
            }},
            'root': {
                'level': 'DEBUG' if debug else 'INFO',
                'handlers': ['wsgi', 'file_info', 'file_error']
            }
        })
        # logging.basicConfig(level=logging.DEBUG)
        self.logging_file = logging.getLogger('file')
        self.logging_file.debug("Starting logging...")
        self.basedir = os.path.abspath(os.path.dirname(__file__))
        self.databaseFile = os.path.join(self.basedir, 'db/test.db')
        self.tempDatabaseFile = os.path.join(self.basedir, 'db/temporary.db')
        try:
            os.remove(self.tempDatabaseFile)
        except OSError:
            pass

        shutil.copyfile(self.databaseFile, self.tempDatabaseFile)
        logging.debug("Temporary database created at: " +
                      self.tempDatabaseFile)

        # Custom validator for JSON schemas
        # It enables custom responses for 400 and 415 response codes as well
        self.validator_map = {
            'body': CustomRequestBodyValidator,
        }

        # Connexion and flask application instance
        self.app = connexion.App(__name__, specification_dir='./openapi/')
        self.flask_app = self.app.app

    def addErrorHandlers(self):
        """
         Adding custom exception handlers for OAuth validation problems
        This includes custom made error responses with hypermedia
        Additionally, adding custom exception handlers
        for some common error responses.
        """
        self.app.add_error_handler(OAuthResponseProblem, Unauthorized401)
        self.app.add_error_handler(OAuthProblem, UnauthorizedNoToken401)
        self.app.add_error_handler(OAuthScopeProblem, Forbidden403)
        self.app.add_error_handler(400, BadRequest400)
        # app.add_error_handler(NonConformingResponse, BadRequest400)
        # app.add_error_handler(InvalidSpecification, BadRequest400)

        # If strict validation is ON, and there are
        # extra paramaters in request,
        # this exception will be thrown
        self.app.add_error_handler(ExtraParameterProblem, BadRequest400)
        self.app.add_error_handler(401, Unauthorized401)
        self.app.add_error_handler(403, Forbidden403)
        self.app.add_error_handler(404, NotFound404)
        self.app.add_error_handler(409, Conflict409)
        self.app.add_error_handler(415, UnsupportedMedia415)
        self.app.add_error_handler(500, InternalError500)

    def configFlaskApp(self):
        """
        Add flask configurations
        """
        self.flask_app.json_encoder = encoder.JSONEncoder
        # Determine if logs are printed or not to STDOUT
        # self.flask_app.config['SQLALCHEMY_ECHO'] = True
        self.flask_app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + \
            self.tempDatabaseFile
        self.flask_app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

        # Enable trailing slashes for routes
        self.flask_app.url_map.strict_slashes = False

    def initApiConf(self):
        self.app.add_api('openapi.yaml', arguments={
                         'title': 'Network Scanner API'},
                         strict_validation=True, validate_responses=False,
                         validator_map=self.validator_map)

    def initDatabase(self):
        # Database instance
        db.init_app(self.flask_app)

        # Set dabase logging level to ERROR
        sqla_logger = logging.getLogger('sqlalchemy')
        sqla_logger.setLevel(logging.ERROR)
        # sqla_logger.propagate = False

    def getConnexionInstance(self):
        return self.app

    def start(self, port=None):
        # Workaround to get server working with
        # 'use_reloader=True' in __main__.py
        os.environ['PYTHONPATH'] = os.getcwd()

        self.app.run(port=port if port else 8080,
                     debug=True, use_reloader=True)
