# coding: utf-8

from __future__ import absolute_import
from network_scanner.models.target_item_get import TargetItemGet
import unittest

from flask import json
from six import BytesIO

from network_scanner.models.edit_scan_or_target import EditScanOrTarget  # noqa: E501
from network_scanner.test import BaseTestCase
from network_scanner.test.fake_data import TOKEN_SINGLE_USER
import logging
from network_scanner.models.generic_collection_response import \
    GenericCollectionResponse


class TestTargetsController(BaseTestCase):
    """
    TargetsController integration test """

    def test_api_users_user_id_scans_scan_id_targets_get(self):
        """Test case for api_users_user_id_scans_scan_id_targets_get

        List the targets scanned during scan
        Validate content briefly.
        """
        request_uri = '/api/users/{user_id}/scans/{scan_id}/targets'.format(
            user_id=2, scan_id=4)
        headers = {
            'Accept': 'application/vnd.mason+json',
            # 'accept': application/vnd.mason+json,
            'Authorization': 'Bearer ' + TOKEN_SINGLE_USER
        }
        response = self.client.open(
            request_uri,
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

        TargetsResponse = GenericCollectionResponse.from_dict(response.json)
        assert TargetsResponse.namespaces
        assert TargetsResponse.items
        self.validateResponseControls(response, ['self', 'up',
                                                 'netscan:scans-all',
                                                 'netscan:logout-user'])
        # This is just simple test, validating only that atrribute exist
        # Not validating, if value is correct

        # NOTE THERE ARE NOT ROUTES IN EXAMPLE DATABASE SO NOT TESTING IT
        for item in TargetsResponse.items:
            target = TargetItemGet.from_dict(item)
            assert target.device_id
            assert target.scan_id
            assert target.target_domain_name
            assert target.i_pv4_address
            assert target.i_pv6_address
            assert target.reverse_dns
            assert target.modified_at

        logging.info("TEST_API_USRES_USER_ID_SCANS_"
                     "SCAN_ID_TARGETS_GET Successful.")

    @unittest.skip("Not implemented")
    def test_api_users_user_id_scans_scan_id_targets_target_id_delete(self):
        """Test case for api_users_user_id_scans_scan_id_targets_target_id_delete

        Delete target from the scan
        """
        headers = {
            'Accept': 'application/vnd.mason+json',
            # 'accept': application/vnd.mason+json,
            'Authorization': 'Bearer special-key',
        }
        response = self.client.open(
            '/api/users/{user_id}/scans/{scan_id}/targets/{target_id}'.format(
                user_id=3.4, scan_id=3.4, target_id=3.4),
            method='DELETE',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    @unittest.skip("Not implemented")
    def test_api_users_user_id_scans_scan_id_targets_target_id_get(self):
        """Test case for api_users_user_id_scans_scan_id_targets_target_id_get

        Get information of single target
        """
        headers = {
            'Accept': 'application/vnd.mason+json',
            # 'accept': application/vnd.mason+json,
            'Authorization': 'Bearer special-key',
        }
        response = self.client.open(
            '/api/users/{user_id}/scans/{scan_id}/targets/{target_id}'.format(
                user_id=3.4, scan_id=3.4, target_id=3.4),
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    @unittest.skip("Not implemented")
    def test_api_users_user_id_scans_scan_id_targets_target_id_patch(self):
        """Test case for api_users_user_id_scans_scan_id_targets_target_id_patch

        Edit information of single target
        """
        body = {}
        headers = {
            'Accept': 'application/vnd.mason+json',
            'Content-Type': 'application/json',
            # 'accept': application/vnd.mason+json,
            'Authorization': 'Bearer special-key',
        }
        response = self.client.open(
            '/api/users/{user_id}/scans/{scan_id}/targets/{target_id}'.format(
                user_id=3.4, scan_id=3.4, target_id=3.4),
            method='PATCH',
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    unittest.main()
