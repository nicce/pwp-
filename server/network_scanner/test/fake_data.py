"""
File containing some fake-data models for providing
re-usable data objects for testing.
"""

TOKEN_SINGLE_USER = 'qwerty'  # for John Doe, not admin, ID 2
TOKEN_ADMIN = '12312AB3info3i'

# New objects

USER_NOT_ADMIN = {
    "Username": "hackermannnnn",
    "isAdmin": True,
    "FirstName": "Hackerre the Real",
    "Surname": "The Man",
    "Email": "hackerman@everywheree.com",
    "NewPassword": "osidh32099jf0392jfd",
    "RepeatNewPassword": "osidh32099jf0392jfd",
    "Secret": "Nonsense"  # NOTE, not valid secret
}
USER_ADMIN = {
    "Username": "Unambigous",
    "isAdmin": True,
    "FirstName": "Truly",
    "Surname": "Relevant",
    "Email": "relevant@everywhere.com",
    "NewPassword": "osidh32099jf03jfd",
    "RepeatNewPassword": "osidh32099jf03jfd",
    "Secret": "I am a true Admin."  # NOTE Valid secret
}

SCAN_SINGLE_TARGET = {
    "isSingleTarget": True,
    "target": "192.168.1.1",
    "optionsForTargets": "-sV",
    "arbitaryCommand": None,
    "arbitraryCommandInTxtfile": None
}
SCAN_FROM_FILE = {
    "isSingleTarget": True,
    "target": "192.168.1.1",
    "optionsForTargets": "-sV",
    "arbitaryCommand": "JAJAJA",
    "arbitraryCommandInTxtfile": "TADAMDIMAOSI29U)9J092jd093u029)OIHJ"
}

# Existing objects
USER1_FROM_DB_ADMIN = {
    "ID": 1,
    "Username": "VeryFirstUser",
    "HashedPassword": "A3235454ef39U2v32093ve23gfij",
    "API_Key": "12312AB3info3i",
    "FirstName": "First",
    "Surname": "Second",
    "Email": "very.first@user.com",
    "isAdmin": True,
    "CreatedAt": "2019-03-27T15:08:08.906927Z",
    "LastLogin": None
}


USER2_FROM_DB_NOT_ADMIN = {
    "ID": 2,
    "Username": "SecondUser",
    "HashedPassword": "A3235454ef39U2v32093ve23gfij",
    "API_Key": "qwerty",
    "FirstName": "John",
    "Surname": "Doe",
    "Email": "john.doe@user.com",
    "isAdmin": False,
    "CreatedAt": "2019-03-27T15:08:09.111409Z",
    "LastLogin": None
}
