# coding: utf-8

from __future__ import absolute_import
import unittest

from flask import json
from six import BytesIO

from network_scanner.test import BaseTestCase


class TestTracerouteController(BaseTestCase):
    """TracerouteController integration test stubs"""

    @unittest.skip("Not implemented")
    def test_api_users_user_id_scans_scan_id_targets_target_id_route_delete(self):
        """Test case for api_users_user_id_scans_scan_id_targets_target_id_route_delete

        Delete traceroute
        """
        headers = {
            'Accept': 'application/vnd.mason+json',
            # 'accept': application/vnd.mason+json,
            'Authorization': 'Bearer special-key',
        }
        response = self.client.open(
            '/api/users/{user_id}/scans/{scan_id}/targets/{target_id}/route'.format(
                user_id=3.4, scan_id=3.4, target_id=3.4),
            method='DELETE',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    @unittest.skip("Not implemented")
    def test_api_users_user_id_scans_scan_id_targets_target_id_route_get(self):
        """Test case for api_users_user_id_scans_scan_id_targets_target_id_route_get

        Get traceroute information
        """
        headers = {
            'Accept': 'application/vnd.mason+json',
            # 'accept': application/vnd.mason+json,
            'Authorization': 'Bearer special-key',
        }
        response = self.client.open(
            '/api/users/{user_id}/scans/{scan_id}/targets/{target_id}/route'.format(
                user_id=3.4, scan_id=3.4, target_id=3.4),
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    unittest.main()
