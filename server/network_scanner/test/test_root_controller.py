# coding: utf-8

from __future__ import absolute_import
import unittest

from flask import json
import logging
from network_scanner.test import BaseTestCase
from network_scanner.uri_conf import ROOT


class TestRootController(BaseTestCase):
    """
    RootController integration test.
    Only GET is implemented. No authentication.
    No extra parameters.
    """

    def test_api_root_get(self):
        """
        Test case for api_get

        Get controls from the root of the server.
        """

        headers = {
            'Accept': 'application/vnd.mason+json',
        }
        response = self.client.open(
            ROOT,
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

        # Validate that body has core attributes
        self.assertTrue(response.json.get("@namespaces"))
        self.assertTrue(response.json.get("information"))
        self.validateResponseControls(response,
                                      ['self', 'netscan:users-all',
                                       'netscan:add-user',
                                       'netscan:auth', 'netscan:logout-user'])

        logging.info("TEST_API_ROOT_GET Succesful.")


if __name__ == '__main__':
    unittest.main()
