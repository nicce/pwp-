# Testing

This folder contains integration test cases to test API.
For each controller in the API, there is separate controller test file.

Additionally there are own test cases for authentication in file [test_oauth_authentication.py](test_oauth_authentication.py).

All requests excluding `/api/auth/`, `/api/logout/` and `/api/` route are requiring authentication. This is defined in the OpenAPI3 specification file. (which is somewhat long, there is security parameter with oauth and scope spec for required routes.) We are using framework called as *Connexion*, which is relying on specification on routes, parameters, json schema and security checks. In other words, API has been build around that spec file, trying to benefit from it as much as possible.

 Authorization for each resource is checked before actual endpoint, validating the Bearer token, and the userID from the route, where client is accessing. If token access and userID are match, access is granted, based on the scope of current token.

 Each token has scope where it is granting the access, basicly in this case there are just two scopes: *specific-user* and *admin*.

 In practise, this means that the client with specific-user scope can access only its own resources, whereas admin has access for everything.

Two functions in security_controller.py file are taking all actions for authenticating each request, therefore we are expecting, that when we are just testing authenticaion in users route, and not any uri extension on top of that, it should be enough at this case.

It could be more comprehensive to test nested routes as well.

### Coverage of tests 

By default, every successfull response is tested on those methods, what we have fully implemented.

As previously defined, authentication tests are separately tested, and so left out from any other tests. We won't see 401 or 403 response checks elsewhere.

**For GET methods**, basicly only succesfull response and 404 Not Found has been tested. We don't have any query parameters on routes. Response status and the content is checked briefly, that it is correct.

**For PUT/PATCH methods**, we have tested that request body is valid (valid json, correct content type header, required attributes are in there, are values valid, unique, non-existent), this leads to 400, 409 and 415 responses.
Tests are also checking, that once we fetch data again, it is what we just updated.

**For DELETE methods**, we have check successful delete and not found. Once something is deleted, it is confirmed that it is really missing.

There are also some more long-term tests, like testing the user creation process: Create user, check new user values, validate log-in process and new authentication credentials at once for example.



## Run tests: basic usage

To launch the all integration tests, use tox:
```
pip install tox
tox
```

*You can enable all debug prints while running tests, by editing file [__init__.py in test folder](network_scanner/test/__init__.py):*

*Change initConnexionApp class constructor argument into `debug=True`* Otherwise, logging is in INFO level, and very little information will be printed.

If you invoke tox like this:

```
tox -- network_scanner.test.test_oauth_authentication
```
This will only run tests from test_oauth_authentication.py

Or to run single test method from class:

```
tox -- network_scanner.test.test_oauth_authentication:TestOAuthAuthentication.test_valid_token_get_correct_user
```
Will run this single test method.

Code coverage analysis will be shown in the end of tests.

It should be noted, that everything is not implemented fully, and therefore overall codecoverage is rather low. For each fully implemented controller, code coverage should be more than 90%.

## For manual testing
We have provided example database named as test.db in [db folder](../db)
This database contains some predefined values. Those can be used for accessing data via API.
It is highly recommended to use some kind of database explorer, because example data is not so logical, and it might be hard to guess some of the values.

### Authentication

API is using at least partial OAuth 2.0 authentication (we are not sure, if we can call this implementation for that), and therefore access tokens are required, when accessing user specified resources.

Each user has permanent access key, and per-session created access key.

In this case, permanent access key in database is named as **API_Key**

Session based key can be created by logging in with basic method: username and password. This generates Bearer token, which is valid for one hour. This token can be used as Authorization header, type Bearer.

To see API keys of example users, you can view them [here](fake_data.py)

Unfortunately, we don't have example passwords. We are using bcrypt to hash passwords, and in example users, those are just random values, and there is no knowledge witch kind of password might generate them.

Session based keys can be tested by creating new user. Admin user is required to be created, if some data might be checked on this user. Currently addition of new scan data is not possible with API, since method is not implemented. (at least yet...)