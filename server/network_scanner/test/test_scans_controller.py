# coding: utf-8

from __future__ import absolute_import
import unittest

from flask import json
from six import BytesIO
import logging
from network_scanner.models.edit_scan_or_target import EditScanOrTarget  # noqa: E501
from network_scanner.test import BaseTestCase
from network_scanner.test.fake_data import TOKEN_SINGLE_USER


class TestScansController(BaseTestCase):
    """ScansController integration test stubs"""

    def test_api_users_user_id_scans_all_get(self):
        """Test case for api_users_user_id_scans_get

        List all scans
        """
        headers = {
            'Accept': 'application/vnd.mason+json',
            'Authorization': 'Bearer ' + TOKEN_SINGLE_USER
        }
        request_uri = '/api/users/{user_id}/scans'.format(user_id=2)
        response = self.client.open(
            request_uri,
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

        self.validateResponseControls(response,
                                      ['self', 'up', 'netscan:new-scan',
                                       'netscan:logout-user'])
        # TODO maybe check that items has correct data,
        response.json.get("items")[0].get("user_id") == 2

        logging.info("TEST_API_USERS_USER_SCANS_ALL_GET Succesful")

    def test_api_users_user_single_scan_get(self):
        """
        Test case for api_users_user_id_scans_scan_id_get

        Get single scan information
        """
        headers = {
            'Accept': 'application/vnd.mason+json',
            'Authorization': 'Bearer ' + TOKEN_SINGLE_USER
        }
        request_uri = '/api/users/{user_id}/scans/{scan_id}/'.format(
            user_id=2, scan_id=4)
        response = self.client.open(
            request_uri,
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

        self.validateResponseControls(response,
                                      ['self', 'up', 'profile',
                                       'netscan:edit-scan',
                                       'netscan:delete-scan',
                                       'netscan:targets-all',
                                       'netscan:logout-user'])
        # TODO maybe check that items has correct data,

        self.assertTrue(response.json.get("scan_id"))
        self.assertTrue(response.json.get("user_id"))
        self.assertTrue(response.json.get("ScanParams"))
        self.assertTrue(response.json.get("CreatedAt"))
        self.assertTrue(response.json.get("ModifiedAt"))
        self.assertTrue(response.json.get("ScanInProgress"))
        self.assertTrue(response.json.get("Comment"))
        self.assertTrue('IsTagged' in response.json)
        self.assertTrue(response.json.get("ScanInProgress"))
        response.json.get("user_id") == 2

        logging.info("TEST_API_USERS_USER_SINGLE_SCAN_GET Succesful")

    def test_api_users_user_single_scan_get_not_found(self):
        """
        Test case for api_users_user_id_scans_scan_id_get

        Get single scan information
        """
        headers = {
            'Accept': 'application/vnd.mason+json',
            'Authorization': 'Bearer ' + TOKEN_SINGLE_USER
        }
        request_uri = '/api/users/{user_id}/scans/{scan_id}/'.format(
            user_id=2, scan_id=9)
        response = self.client.open(
            request_uri,
            method='GET',
            headers=headers)
        self.assert404(response,
                       'Response body is : ' + response.data.decode('utf-8'))

        self.validateErrorResponseBody(response, request_uri, ['profile'])
        logging.info("TEST_API_USERS_USER_SINGLE_SCAN_GET_NOT_FOUND Succesful")

    def test_api_users_user_single_scan_delete(self):
        """
        Test case for api_users_user_id_scans_scan_id_delete

        Test case for deleting a single scan
        """
        headers = {
            'Accept': 'application/vnd.mason+json',
            'Authorization': 'Bearer ' + TOKEN_SINGLE_USER
        }
        request_uri = '/api/users/{user_id}/scans/{scan_id}/'.format(
            user_id=2, scan_id=4)

        # Let's check that object exist at first
        response = self.client.open(
            request_uri,
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

        # It exists, let's delete it
        response = self.client.open(
            request_uri,
            method='DELETE',
            headers=headers)

        assert response.status_code == 204

        # It shoud not exist anymore
        response = self.client.open(
            request_uri,
            method='GET',
            headers=headers)
        self.assert404(response,
                       'Response body is : ' + response.data.decode('utf-8'))
        self.validateErrorResponseBody(response, request_uri, ['profile'])
        logging.info("TEST_API_USERS_USER_SINGLE_SCAN_DELETE Succesful")

    def test_api_users_user_single_scan_delete_not_found(self):
        """
        Trying to delete a scan what does not exist.
        """
        headers = {
            'Accept': 'application/vnd.mason+json',
            'Authorization': 'Bearer ' + TOKEN_SINGLE_USER
        }
        request_uri = '/api/users/{user_id}/scans/{scan_id}/'.format(
            user_id=2, scan_id=3)

        # Let's check that object exist at first
        response = self.client.open(
            request_uri,
            method='DELETE',
            headers=headers)
        self.assert404(response,
                       'Response body is : ' + response.data.decode('utf-8'))

        self.validateErrorResponseBody(response, request_uri, ['profile'])
        logging.info(
            "TEST_API_USERS_USER_SINGLE_SCAN_NOT_FOUND_DELETE Succesful")

    def test_api_users_user_single_scan_patch(self):
        """
        Test case for api_users_user_id_scans_scan_id_patch

        Edit scan information. Change comment to another.
        """
        specialComment = "Wonderful scan result!!"
        body = {
            'comment': specialComment,
            'isTagged': False
        }
        request_uri = '/api/users/{user_id}/scans/{scan_id}/'.format(
            user_id=2, scan_id=4)
        headers = {
            'Accept': 'application/vnd.mason+json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + TOKEN_SINGLE_USER
        }
        # Edit scan result with our custom body
        response = self.client.open(
            request_uri,
            method='PATCH',
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')

        assert response.status_code == 204, "Response code in response was {}"\
            .format(response.status_code)
        # Content should be edited now. Let's see if that is so
        response = self.client.open(
            request_uri,
            method='GET',
            headers=headers)

        assert response.json.get('Comment') == specialComment

        logging.info(
            "TEST_API_USERS_USER_SINGLE_SCAN_PATCH Succesful")

    def test_api_users_user_single_scan_patch_not_found(self):
        """
        Test case for scan's controller patch method.
        Trying to non-existent scan result.

        Response should be 404 Not Found
        """
        specialComment = "Wonderful scan result!!"
        body = {
            'comment': specialComment,
            'isTagged': False
        }
        request_uri = '/api/users/{user_id}/scans/{scan_id}/'.format(
            user_id=2, scan_id=3)
        headers = {
            'Accept': 'application/vnd.mason+json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + TOKEN_SINGLE_USER
        }

        response = self.client.open(
            request_uri,
            method='PATCH',
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')

        response.status_code == 404
        self.validateErrorResponseBody(response, request_uri, ['profile'])

        logging.info(
            "TEST_API_USERS_USER_SINGLE_SCAN_PATCH_NOT_FOUND Succesful")

    def test_api_users_user_single_scan_patch_missing_required(self):
        """
        Test case for scan's controller patch method.
        Trying to edit without isTagged field.

        Response should be 400 Bad Request
        """
        specialComment = "Wonderful scan result!!"
        body = {
            'comment': specialComment
        }
        request_uri = '/api/users/{user_id}/scans/{scan_id}/'.format(
            user_id=2, scan_id=4)
        headers = {
            'Accept': 'application/vnd.mason+json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + TOKEN_SINGLE_USER
        }

        response = self.client.open(
            request_uri,
            method='PATCH',
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')

        response.status_code == 400
        self.validateErrorResponseBody(response, request_uri, ['profile'])

        logging.info(
            "TEST_API_USERS_USER_SINGLE_SCAN_PATCH_MISSING_REQUIRED Succesful")

    def test_api_users_user_single_scan_patch_invalid_content_type(self):
        """
        Test case for scan's controller patch method.
        Trying to edit with wrong content type header

        Response should be 415 Unsupported Media Type
        """
        specialComment = "Wonderful scan result!!"
        body = {
            'comment': specialComment,
            'isTagged': False
        }
        request_uri = '/api/users/{user_id}/scans/{scan_id}/'.format(
            user_id=2, scan_id=4)
        headers = {
            'Accept': 'application/vnd.mason+json',
            'Content-Type': 'application/xml',
            'Authorization': 'Bearer ' + TOKEN_SINGLE_USER
        }

        response = self.client.open(
            request_uri,
            method='PATCH',
            headers=headers,
            data=json.dumps(body),
            content_type='application/xml')

        response.status_code == 415
        self.validateErrorResponseBody(response, request_uri, ['profile'])

        logging.info(
            "TEST_API_USERS_USER_SINGLE_SCAN_PATCH_INVALID_CONTENT_TYPE"
            " Succesful")

    def test_api_users_user_single_scan_patch_invalid_json(self):
        """
        Test case for scan's controller patch method.
        Trying to edit with invalid json

        Response should be 400 Bad Request
        """
        specialComment = "Wonderful scan result!!"
        body = "'comment': specialComment,'isTagged': False}"

        request_uri = '/api/users/{user_id}/scans/{scan_id}/'.format(
            user_id=2, scan_id=4)
        headers = {
            'Accept': 'application/vnd.mason+json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + TOKEN_SINGLE_USER
        }

        response = self.client.open(
            request_uri,
            method='PATCH',
            headers=headers,
            data=body,
            content_type='application/json')

        response.status_code == 400
        self.validateErrorResponseBody(response, request_uri, ['profile'])

        logging.info(
            "TEST_API_USERS_USER_SINGLE_SCAN_PATCH_INVALID_JSON"
            " Succesful")

    # IGNORED TEST: SQLite DOES NOT SUPPORT STRING LENGHT VALIDATION
    # Have to use postgresql or something

    # def test_api_users_user_single_scan_patch_too_large_comment(self):
    #     """
    #     Test case for scan's controller patch method.
    #     Trying to add too large comment.

    #     Response should be 400 Bad Request
    #     """
    #     specialComment = "Wonderful scan result!!" * 20
    #     body = {
    #         'comment': specialComment,
    #         'isTagged': False
    #     }

    #     request_uri = '/api/users/{user_id}/scans/{scan_id}/'.format(
    #         user_id=2, scan_id=4)
    #     headers = {
    #         'Accept': 'application/vnd.mason+json',
    #         'Content-Type': 'application/json',
    #         'Authorization': 'Bearer ' + TOKEN_SINGLE_USER
    #     }

    #     response = self.client.open(
    #         request_uri,
    #         method='PATCH',
    #         headers=headers,
    #         data=json.dumps(body),
    #         content_type='application/json')

    #     assert response.status_code == 400
    #     logging.info(response.get_data())
    #     # self.validateErrorResponseBody(response, request_uri, ['profile'])

    #     logging.info(
    #         "TEST_API_USERS_USER_SINGLE_SCAN_PATCH_TOO_LARGE_COMMENT"
    #         " Succesful")


if __name__ == '__main__':
    unittest.main()
