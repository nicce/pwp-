# coding: utf-8

from __future__ import absolute_import
import unittest
import logging
from flask import json
from six import BytesIO

from network_scanner.test import BaseTestCase
from network_scanner.uri_conf import AUTH_URI, LOGOUT_URI, USERS_URI
from network_scanner.test.fake_data import USER_NOT_ADMIN
from network_scanner.models.accesstoken import Accesstoken
from network_scanner.models.auth_session_response import AuthSessionResponse


class TestLogoutController(BaseTestCase):
    """
    LogoutController integration tests
    Workflow of test is following:
    1. Let's create user
    2. Log in with basic auth
    3. Get user information with new token
    4. Logout
    5. See, if user information is still accessible
    with this token. It should not.

    """

    def test_api_logout_post(self):
        """Test case for api_logout_post

        Logout user
        """
        # New user information
        body = USER_NOT_ADMIN
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        response = self.client.open(
            USERS_URI,
            method='POST',
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')

        # Confirm that user is created
        assert response.status_code == 201, "Response code in response was {}"\
            .format(response.status_code)

        # Validate briefly body of response
        self.validateAccesstoken(response)
        token_info = Accesstoken.from_dict(response.json)

        # Let's try to use auth path to get new session token
        # for OAUTH
        auth_body = {
            'userName': USER_NOT_ADMIN.get("Username"),
            'password': USER_NOT_ADMIN.get("NewPassword")
        }
        response = self.client.open(
            AUTH_URI,
            method='POST',
            headers=headers,
            data=json.dumps(auth_body),
            content_type='application/json')

        # Redirect response, we should get Location uri as well
        assert response.status_code == 303, "Response code in response was {}"\
            .format(response.status_code)
        session_info = AuthSessionResponse.from_dict(response.json)

        # Finally let's get user data with new token from location url
        headers["Authorization"] = 'Bearer ' + session_info.session_token
        user_location = response.headers['location']
        response = self.client.open(
            user_location,
            method='GET',
            headers=headers)

        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

        self.validateResponseControls(response,
                                      ['self', 'profile', 'up',
                                       'netscan:scans-all',
                                       'netscan:devices-all',
                                       'netscan:new-scan',
                                       'netscan:edit-user',
                                       'netscan:delete-user',
                                       'netscan:logout-user'])

        self.assertTrue(response.json.get('ID'))
        self.assertTrue(response.json.get('Username'))
        self.assertTrue(response.json.get('Email'))
        self.assertTrue(response.json.get('FirstName'))
        self.assertTrue(response.json.get('Surname'))
        self.assertTrue(response.json.get('CreatedAt'))

        user_intersect = set(response.json) & set(USER_NOT_ADMIN)

        # Let's see if original values and response values match
        for key in user_intersect:
            if key == 'isAdmin':
                continue
            assert response.json.get(key) == USER_NOT_ADMIN.get(
                key), "Keys were response: {} vs original: {}"\
                .format(response.json.get(key), USER_NOT_ADMIN.get(key))

        # OK, let's logout
        response = self.client.open(
            LOGOUT_URI,
            method='POST',
            headers=headers)

        assert response.status_code == 303, "Response code in response was {}"\
            .format(response.status_code)
        self.validateResponseControls(response,
                                      ['self', 'netscan:auth'])
        response = self.client.open(
            user_location,
            method='GET',
            headers=headers)

        # After logging out, token is not valid anymore.
        assert response.status_code == 401, "Response code in response was {}"\
            .format(response.status_code)

        logging.info("TEST_API_LOGOUT_POST Succesful.")


if __name__ == '__main__':
    unittest.main()
