# coding: utf-8

from __future__ import absolute_import
import unittest

from flask import json
from six import BytesIO

from network_scanner.models.edit_device_or_port import EditDeviceOrPort  # noqa: E501
from network_scanner.test import BaseTestCase


class TestPortsController(BaseTestCase):
    """PortsController integration test stubs"""

    @unittest.skip("Not implemented")
    def test_api_users_user_id_devices_device_id_port_history_get(self):
        """Test case for api_users_user_id_devices_device_id_port_history_get

        Get port history
        """
        headers = {
            'Accept': 'application/vnd.mason+json',
            # 'accept': application/vnd.mason+json,
            'Authorization': 'Bearer special-key',
        }
        response = self.client.open(
            '/api/users/{user_id}/devices/{device_id}/port-history'.format(
                user_id=3.4, device_id=3.4),
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    @unittest.skip("Not implemented")
    def test_api_users_user_id_scans_scan_id_targets_target_id_device_ports_get(self):
        """Test case for api_users_user_id_scans_scan_id_targets_target_id_device_ports_get

        List all ports
        """
        headers = {
            'Accept': 'application/vnd.mason+json',
            # 'accept': application/vnd.mason+json,
            'Authorization': 'Bearer special-key',
        }
        response = self.client.open(
            '/api/users/{user_id}/scans/{scan_id}/targets/{target_id}/device/ports'.format(
                user_id=56, scan_id=56, target_id=56),
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    @unittest.skip("Not implemented")
    def test_api_users_user_id_scans_scan_id_targets_target_id_device_ports_port_delete(self):
        """Test case for api_users_user_id_scans_scan_id_targets_target_id_device_ports_port_delete

        Delete port information
        """
        headers = {
            'Accept': 'application/vnd.mason+json',
            # 'accept': application/vnd.mason+json,
            'Authorization': 'Bearer special-key',
        }
        response = self.client.open(
            '/api/users/{user_id}/scans/{scan_id}/targets/{target_id}/device/ports/{port}'.format(
                user_id=56, scan_id=56, target_id=56, port=56),
            method='DELETE',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    @unittest.skip("Not implemented")
    def test_api_users_user_id_scans_scan_id_targets_target_id_device_ports_port_get(self):
        """Test case for api_users_user_id_scans_scan_id_targets_target_id_device_ports_port_get

        Get port information
        """
        headers = {
            'Accept': 'application/vnd.mason+json',
            # 'accept': application/vnd.mason+json,
            'Authorization': 'Bearer special-key',
        }
        response = self.client.open(
            '/api/users/{user_id}/scans/{scan_id}/targets/{target_id}/device/ports/{port}'.format(
                user_id=56, scan_id=56, target_id=56, port=56),
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    @unittest.skip("*/* not supported by Connexion. Use application/json instead. See https://github.com/zalando/connexion/pull/760")
    def test_api_users_user_id_scans_scan_id_targets_target_id_device_ports_port_patch(self):
        """Test case for api_users_user_id_scans_scan_id_targets_target_id_device_ports_port_patch

        Edit port information
        """
        body = {}
        headers = {
            'Accept': 'application/vnd.mason+json',
            'Content-Type': 'application/json',
            # 'accept': application/vnd.mason+json,
            'Authorization': 'Bearer special-key',
        }
        response = self.client.open(
            '/api/users/{user_id}/scans/{scan_id}/targets/{target_id}/device/ports/{port}'.format(
                user_id=56, scan_id=56, target_id=56, port=56),
            method='PATCH',
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    unittest.main()
