# coding: utf-8

from __future__ import absolute_import
import unittest

from flask import json
from six import BytesIO
from datetime import datetime
import math
import logging
from network_scanner.test import BaseTestCase
from network_scanner.uri_conf import *


class TestDevicesController(BaseTestCase):
    """DevicesController integration test stubs"""

    # @unittest.skip("Not implemented")
    def test_api_users_user_id_devices_device_id_delete(self):
        """Test case for api_users_user_id_devices_device_id_delete

        Delete device information
        """
        userID = 2
        device_id = 50
        uri = '{}{}{}{}/'.format(USERS_URI, userID, DEVICES_URI, device_id)
        headers = {
            'Accept': 'application/vnd.mason+json',
            # 'accept': application/vnd.mason+json,
            'Authorization': 'Bearer qwerty',
        }
        response = self.client.open(
            uri,
            method='GET',
            headers=headers)
        self.assertEqual(response.status_code, 200)

        response = self.client.open(
            uri,
            method='DELETE',
            headers=headers)
        self.assertEqual(response.status_code, 204)

        response = self.client.open(
            uri,
            method='GET',
            headers=headers)
        self.assertEqual(response.status_code, 404)

        logging.info(
            "TEST_API_USERS_USER_ID_DEVICES_DEVICE_ID_DELETE"
            " Succesful")

    def test_api_users_user_id_devices_device_id_get(self):
        """Test case for api_users_user_id_devices_device_id_get

        Get device information
        """

        headers = {
            'Accept': 'application/vnd.mason+json',
            # 'accept': application/vnd.mason+json,
            'Authorization': 'Bearer qwerty',  # Valid key for user_id=2: qwerty
        }
        userID = 2
        device_id = 50
        uri = '{}{}{}{}/'.format(USERS_URI, userID, DEVICES_URI, device_id)

        response = self.client.open(
            uri,
            method='GET',
            headers=headers)
        self.assertEqual(response.status_code, 200)
        body = json.loads(response.data)
        # Check @controls
        validItem = '{}{}{}{}/'.format(USERS_URI,
                                       userID, DEVICES_URI, device_id)
        self.assertEqual(body["@controls"]["self"]["href"], validItem)
        validItem = '{}{}{}'.format(USERS_URI, userID, DEVICES_URI)
        self.assertEqual(body["@controls"]["up"]["href"], validItem)
        validItem = '{}{}{}'.format(USERS_URI, userID, SCANS_URI)
        self.assertEqual(body["@controls"]
                         ["netscan:scans-all"]["href"], validItem)
        validItem = '{}'.format(LOGOUT_URI)
        self.assertEqual(body["@controls"]
                         ["netscan:logout-user"]["href"], validItem)
        validItem = '{}{}{}{}{}'.format(
            USERS_URI, userID, DEVICES_URI, device_id, PORT_HISTORY_URI)
        self.assertEqual(body["@controls"]
                         ["netscan:port-history"]["href"], validItem)
        validItem = PROFILE_DEVICE
        self.assertEqual(body["@controls"]
                         ["profile"]["href"], validItem)
        # Check @namespaces
        self.assertTrue(body["@namespaces"]["netscan"])
        # Check items
        self.assertTrue(body["device_id"])
        self.assertTrue(body["MAC_address_64bit"])
        self.assertTrue(body["OS"])
        self.assertTrue(body["Comment"])
        self.assertTrue(body["ExtraInfo"])
        self.assertTrue(body["ModifiedAt"])

        logging.info(
            "TEST_API_USERS_USER_ID_DEVICES_DEVICE_ID_GET"
            " Succesful")

    def test_api_users_user_id_devices_device_id_patch(self):
        """Test case for api_users_user_id_devices_device_id_patch

        Edit device information
        """
        headers = {
            'Accept': 'application/vnd.mason+json',
            # 'accept': application/vnd.mason+json,
            'Authorization': 'Bearer qwerty',  # Valid key for user_id=2: qwerty
            'Content-Type': 'application/json'
        }
        userID = 2
        deviceID = 36
        uri = '{}{}{}{}/'.format(USERS_URI, userID, DEVICES_URI, deviceID)
        comment = "Modified comment."
        body = {"comment": comment}
        modificationTime = datetime.utcnow()
        # Editing the device comment
        response = self.client.open(
            uri,
            method='PATCH',
            headers=headers,
            data=json.dumps(body))
        self.assertEqual(response.status_code, 204)

        headers = {
            'Accept': 'application/vnd.mason+json',
            # 'accept': application/vnd.mason+json,
            'Authorization': 'Bearer qwerty',  # Valid key for user_id=2: qwerty
        }

        # Checking the device comment
        response = self.client.open(
            uri,
            method='GET',
            headers=headers)
        self.assertEqual(response.status_code, 200)
        modifiedBody = json.loads(response.data)
        modifiedAt = datetime.strptime(
            modifiedBody["ModifiedAt"], "%Y-%m-%d %H:%M:%S")
        timeDelta = modifiedAt - modificationTime

        self.assertEqual(modifiedBody["Comment"], comment)
        # comment was modified less than 2 seceonds ago
        self.assertTrue(math.fabs(timeDelta.total_seconds()) < 2)

        logging.info(
            "TEST_API_USERS_USER_ID_DEVICES_DEVICE_ID_PATCH"
            " Succesful")

    def test_api_users_user_id_devices_get(self):
        """Test case for api_users_user_id_devices_get

        Gets the list of devices
        """
        userID = 2
        headers = {
            'Accept': 'application/vnd.mason+json',
            # 'accept': application/vnd.mason+json,
            'Authorization': 'Bearer qwerty',  # Valid key for user_id=2: qwerty
        }
        uri = '{}{}{}'.format(USERS_URI, userID, DEVICES_URI)

        response = self.client.open(
            uri,
            method='GET',
            headers=headers)
        self.assertEqual(response.status_code, 200)
        body = json.loads(response.data)
        # Check @controls
        validItem = '{}{}{}'.format(USERS_URI, userID, DEVICES_URI)
        self.assertEqual(body["@controls"]["self"]["href"], validItem)
        validItem = '{}{}/'.format(USERS_URI, userID)
        self.assertEqual(body["@controls"]["up"]["href"], validItem)
        validItem = '{}{}{}'.format(USERS_URI, userID, SCANS_URI)
        self.assertEqual(body["@controls"]
                         ["netscan:scans-all"]["href"], validItem)
        validItem = '{}'.format(LOGOUT_URI)
        self.assertEqual(body["@controls"]
                         ["netscan:logout-user"]["href"], validItem)
        # Check @namespaces
        self.assertTrue(body["@namespaces"]["netscan"])
        # Check items
        self.assertTrue(body["items"][0]["device_id"])
        self.assertTrue(body["items"][0]["MAC_address_64bit"])
        self.assertTrue(body["items"][0]["OS"])
        self.assertTrue(body["items"][0]["Comment"])
        self.assertTrue(body["items"][0]["ExtraInfo"])
        self.assertTrue(body["items"][0]["ModifiedAt"])
        devId = body["items"][0]["device_id"]
        validItem = '{}{}{}{}/'.format(USERS_URI, userID, DEVICES_URI, devId)
        self.assertEqual(body["items"][0]["@controls"]
                         ["self"]["href"], validItem)
        validItem = PROFILE_DEVICE
        self.assertEqual(body["items"][0]["@controls"]
                         ["profile"]["href"], validItem)

        logging.info(
            "TEST_API_USERS_USER_ID_DEVICES_GET"
            " Succesful")

    def test_api_users_user_id_scans_scan_id_targets_target_id_device_delete(self):
        """Test case for api_users_user_id_scans_scan_id_targets_target_id_device_delete

        Delete device information
        """
        userID = 2
        scanID = 4
        targetID = 32
        uri = '{}{}{}{}{}{}{}'.format(
            USERS_URI, userID, SCANS_URI, scanID, TARGETS_URI, targetID, DEVICE_URI)
        headers = {
            'Accept': 'application/vnd.mason+json',
            # 'accept': application/vnd.mason+json,
            'Authorization': 'Bearer qwerty',
        }
        response = self.client.open(
            uri,
            method='GET',
            headers=headers)
        self.assertEqual(response.status_code, 200)

        response = self.client.open(
            uri,
            method='DELETE',
            headers=headers)
        self.assertEqual(response.status_code, 204)

        response = self.client.open(
            uri,
            method='GET',
            headers=headers)
        self.assertEqual(response.status_code, 404)

        logging.info(
            "TEST_API_USERS_USER_ID_SCANS_SCAN_ID_TARGETS_TARGET_ID_DEVICE_DELETE"
            " Succesful")

    def test_api_users_user_id_scans_scan_id_targets_target_id_device_get(self):
        """Test case for api_users_user_id_scans_scan_id_targets_target_id_device_get

        Get device information
        """
        headers = {
            'Accept': 'application/vnd.mason+json',
            # 'accept': application/vnd.mason+json,
            'Authorization': 'Bearer qwerty',  # Valid key for user_id=2: qwerty
        }
        userID = 2
        scanID = 4
        targetID = 40
        uri = '{}{}{}{}{}{}{}'.format(
            USERS_URI, userID, SCANS_URI, scanID, TARGETS_URI, targetID, DEVICE_URI)

        response = self.client.open(
            uri,
            method='GET',
            headers=headers)
        self.assertEqual(response.status_code, 200)
        body = json.loads(response.data)
        # Check @controls
        validItem = '{}{}{}{}{}{}{}'.format(
            USERS_URI, userID, SCANS_URI, scanID, TARGETS_URI, targetID, DEVICE_URI)
        self.assertEqual(body["@controls"]
                         ["netscan:delete-device"]["href"], validItem)
        self.assertEqual(body["@controls"]
                         ["netscan:edit-device"]["href"], validItem)
        self.assertEqual(body["@controls"]["self"]["href"], validItem)
        validItem = '{}{}{}{}{}{}/'.format(USERS_URI,
                                           userID, SCANS_URI, scanID, TARGETS_URI, targetID)
        self.assertEqual(body["@controls"]["up"]["href"], validItem)
        validItem = '{}{}{}'.format(USERS_URI, userID, SCANS_URI)
        self.assertEqual(body["@controls"]
                         ["netscan:scans-all"]["href"], validItem)
        validItem = '{}'.format(LOGOUT_URI)
        self.assertEqual(body["@controls"]
                         ["netscan:logout-user"]["href"], validItem)
        validItem = PROFILE_DEVICE
        self.assertEqual(body["@controls"]
                         ["profile"]["href"], validItem)
        # Check @namespaces
        self.assertTrue(body["@namespaces"]["netscan"])
        # Check items
        self.assertTrue(body["device_id"])
        self.assertTrue(body["MAC_address_64bit"])
        self.assertTrue(body["OS"])
        self.assertTrue(body["Comment"])
        self.assertTrue(body["ExtraInfo"])
        self.assertTrue(body["ModifiedAt"])

        logging.info(
            "TEST_API_USERS_USER_ID_SCANS_SCAN_ID_TARGETS_TARGET_ID_DEVICE_GET"
            " Succesful")

    def test_api_users_user_id_scans_scan_id_targets_target_id_device_patch(self):
        """Test case for api_users_user_id_scans_scan_id_targets_target_id_device_patch

        Edit device information
        """

        headers = {
            'Accept': 'application/vnd.mason+json',
            # 'accept': application/vnd.mason+json,
            'Authorization': 'Bearer qwerty',  # Valid key for user_id=2: qwerty
            'Content-Type': 'application/json'
        }
        userID = 2
        scanID = 4
        targetID = 40
        uri = '{}{}{}{}{}{}{}'.format(
            USERS_URI, userID, SCANS_URI, scanID, TARGETS_URI, targetID, DEVICE_URI)
        comment = "Modified comment."
        body = {"comment": comment}
        modificationTime = datetime.utcnow()
        # Editing the device comment
        response = self.client.open(
            uri,
            method='PATCH',
            headers=headers,
            data=json.dumps(body))
        self.assertEqual(response.status_code, 204)

        headers = {
            'Accept': 'application/vnd.mason+json',
            # 'accept': application/vnd.mason+json,
            'Authorization': 'Bearer qwerty',  # Valid key for user_id=2: qwerty
        }

        # Checking the device comment

        response = self.client.open(
            uri,
            method='GET',
            headers=headers)
        self.assertEqual(response.status_code, 200)
        modifiedBody = json.loads(response.data)
        modifiedAt = datetime.strptime(
            modifiedBody["ModifiedAt"], "%Y-%m-%d %H:%M:%S")
        timeDelta = modifiedAt - modificationTime

        self.assertEqual(modifiedBody["Comment"], comment)
        # comment was modified less than 2 seconds ago
        self.assertTrue(math.fabs(timeDelta.total_seconds()) < 2)

        logging.info(
            "TEST_API_USERS_USER_ID_SCANS_SCAN_ID_TARGETS_TARGET_ID_DEVICE_PATCH"
            " Succesful")

    def test_api_users_user_id_devices_device_id_patch_errors(self):
        """Test case for api_users_user_id_devices_device_id_patch with errors

        Errors tested:
        1. Missing JSON field
        2. Invalid JSON datatype (should be "String")
        3. invalid content type in header
        4. Device not found
        """

        headers = {
            'Accept': 'application/vnd.mason+json',
            # 'accept': application/vnd.mason+json,
            'Authorization': 'Bearer qwerty',  # Valid key for user_id=2: qwerty
            'Content-Type': 'application/json'
        }
        userID = 2
        deviceID = 36
        uri = '{}{}{}{}/'.format(USERS_URI, userID, DEVICES_URI, deviceID)

        # Checking the initial state of the "comment" field
        response = self.client.open(
            uri,
            method='GET',
            headers=headers)
        initialBody = json.loads(response.data)

        # TESTING Missing field
        comment = "Modified comment."
        body = {"comment1": comment}  # correct would be "comment"
        response = self.client.open(
            uri,
            method='PATCH',
            headers=headers,
            data=json.dumps(body))
        self.assertEqual(response.status_code, 400)

        # TESTING Invalid datatype
        comment = 3.4  # correct would be "3.4"
        body = {"comment": comment}
        response = self.client.open(
            uri,
            method='PATCH',
            headers=headers,
            data=json.dumps(body))
        self.assertEqual(response.status_code, 400)

        # TESTING Invalid content type ("Content-Type" header parameter missing)
        headers = {
            'Accept': 'application/vnd.mason+json',
            # 'accept': application/vnd.mason+json,
            'Authorization': 'Bearer qwerty',  # Valid key for user_id=2: qwerty
            # 'Content-Type': 'application/json'
        }
        comment = "Modified comment."
        body = {"comment": comment}
        response = self.client.open(
            uri,
            method='PATCH',
            headers=headers,
            data=json.dumps(body))
        self.assertEqual(response.status_code, 415)

        # Testing device not found
        headers = {
            'Accept': 'application/vnd.mason+json',
            # 'accept': application/vnd.mason+json,
            'Authorization': 'Bearer qwerty',  # Valid key for user_id=2: qwerty
            'Content-Type': 'application/json'
        }
        deviceID = 999
        uri = '{}{}{}{}/'.format(USERS_URI, userID, DEVICES_URI, deviceID)
        body = {"comment": comment}
        response = self.client.open(
            uri,
            method='PATCH',
            headers=headers,
            data=json.dumps(body))
        self.assertEqual(response.status_code, 404)

        # Checking that the "comment" field has not bee modified
        deviceID = 36
        uri = '{}{}{}{}/'.format(USERS_URI, userID, DEVICES_URI, deviceID)
        response = self.client.open(
            uri,
            method='GET',
            headers=headers)
        modifiedBody = json.loads(response.data)
        self.assertEqual(modifiedBody["Comment"], initialBody["Comment"])

        logging.info(
            "TEST_API_USERS_USER_ID_DEVICES_DEVICE_ID_PATCH_ERRORS"
            " Succesful")

    def test_api_users_user_id_scans_scan_id_targets_target_id_device_patch_errors(self):
        """T
        est case for api_users_user_id_scans_scan_id_targets_target
        _id_device_patch
         with errors

        Errors tested:
        1. Missing JSON field
        2. Invalid JSON datatype (should be "String")
        3. invalid content type in header
        4. Device not found
        """

        headers = {
            'Accept': 'application/vnd.mason+json',
            # 'accept': application/vnd.mason+json,
            'Authorization': 'Bearer qwerty',  # Valid key for user_id=2: qwerty
            'Content-Type': 'application/json'
        }
        userID = 2
        scanID = 4
        targetID = 40
        uri = '{}{}{}{}{}{}{}'.format(
            USERS_URI, userID, SCANS_URI, scanID, TARGETS_URI, targetID, DEVICE_URI)

        # Checking the initial state of the "comment" field
        response = self.client.open(
            uri,
            method='GET',
            headers=headers)
        initialBody = json.loads(response.data)

        # TESTING Missing field
        comment = "Modified comment."
        body = {"comment1": comment}  # correct would be "comment"
        response = self.client.open(
            uri,
            method='PATCH',
            headers=headers,
            data=json.dumps(body))
        self.assertEqual(response.status_code, 400)

        # TESTING Invalid datatype
        comment = 3.4  # correct would be "3.4"
        body = {"comment": comment}
        response = self.client.open(
            uri,
            method='PATCH',
            headers=headers,
            data=json.dumps(body))
        self.assertEqual(response.status_code, 400)

        # TESTING Invalid content type ("Content-Type" header parameter missing)
        headers = {
            'Accept': 'application/vnd.mason+json',
            # 'accept': application/vnd.mason+json,
            'Authorization': 'Bearer qwerty',  # Valid key for user_id=2: qwerty
            # 'Content-Type': 'application/json'
        }
        comment = "Modified comment."
        body = {"comment": comment}
        response = self.client.open(
            uri,
            method='PATCH',
            headers=headers,
            data=json.dumps(body))
        self.assertEqual(response.status_code, 415)

        headers = {
            'Accept': 'application/vnd.mason+json',
            # 'accept': application/vnd.mason+json,
            'Authorization': 'Bearer qwerty',  # Valid key for user_id=2: qwerty
            'Content-Type': 'application/json'
        }
        # Testing device not found
        targetID = 2
        uri = '{}{}{}{}{}{}{}'.format(
            USERS_URI, userID, SCANS_URI, scanID, TARGETS_URI, targetID, DEVICE_URI)
        body = {"comment": comment}
        response = self.client.open(
            uri,
            method='PATCH',
            headers=headers,
            data=json.dumps(body))
        self.assertEqual(response.status_code, 404)

        # Checking that the "comment" field has not bee modified
        targetID = 40
        uri = '{}{}{}{}{}{}{}'.format(
            USERS_URI, userID, SCANS_URI, scanID, TARGETS_URI, targetID, DEVICE_URI)
        response = self.client.open(
            uri,
            method='GET',
            headers=headers)
        modifiedBody = json.loads(response.data)
        self.assertEqual(modifiedBody["Comment"], initialBody["Comment"])

        logging.info(
            "TEST_API_USERS_USER_ID_SCANS_SCAN_ID_TARGETS_TARGET"
            "_ID_DEVICE_PATCH_ERRORS"
            " Succesful")


if __name__ == '__main__':
    unittest.main()
