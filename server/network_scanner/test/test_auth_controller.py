# coding: utf-8

from __future__ import absolute_import
import unittest

from flask import json
from six import BytesIO
import logging
from network_scanner.test import BaseTestCase
from network_scanner.uri_conf import AUTH_URI
from network_scanner.test.fake_data import USER_NOT_ADMIN, USER1_FROM_DB_ADMIN
from network_scanner.models.accesstoken import Accesstoken
from network_scanner.models.auth_session_response import AuthSessionResponse


class TestAuthController(BaseTestCase):
    """
    AuthController integration test stubs
    """

    def test_api_auth_get(self):
        """
        Test case for api_auth_get

        Get controls and information
        from the auth route.
        """

        headers = {
            'Accept': 'application/vnd.mason+json',
        }
        response = self.client.open(
            AUTH_URI,
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

        # Validate that body has core attributes
        self.assertTrue(response.json.get("@namespaces"))
        self.assertTrue(response.json.get("information"))
        self.validateResponseControls(response,
                                      ['self', 'netscan:users-all',
                                       'up',
                                       'netscan:login-user'])

        logging.info("TEST_API_AUTH_GET Successful.")

    def test_api_auth_post_valid_username_password(self):
        """
        Test case for api_auth_post

        Post valid username and password.
        Get new access token as response, which lasts one hour.
        Use token to get user information.

        We have to create new user for this test,
        because brypt is used, and test data is not based on valid hash.
        """

        # New user information
        body = USER_NOT_ADMIN
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        response = self.client.open(
            '/api/users/',
            method='POST',
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')

        # Confirm that user is created
        assert response.status_code == 201, "Response code in response was {}"\
            .format(response.status_code)

        # Validate briefly body of response
        self.validateAccesstoken(response)
        token_info = Accesstoken.from_dict(response.json)

        # Let's try to use auth path to get new session token
        # for OAUTH
        auth_body = {
            'userName': USER_NOT_ADMIN.get("Username"),
            'password': USER_NOT_ADMIN.get("NewPassword")
        }
        response = self.client.open(
            '/api/auth/',
            method='POST',
            headers=headers,
            data=json.dumps(auth_body),
            content_type='application/json')

        # Redirect response, we should get Location uri as well
        assert response.status_code == 303, "Response code in response was {}"\
            .format(response.status_code)
        session_info = AuthSessionResponse.from_dict(response.json)

        # Finally let's get user data with new token from location url
        headers["Authorization"] = 'Bearer ' + session_info.session_token
        response = self.client.open(
            response.headers['location'],
            method='GET',
            headers=headers)

        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

        self.validateResponseControls(response,
                                      ['self', 'profile', 'up',
                                       'netscan:scans-all',
                                       'netscan:devices-all',
                                       'netscan:new-scan',
                                       'netscan:edit-user',
                                       'netscan:delete-user',
                                       'netscan:logout-user'])

        self.assertTrue(response.json.get('ID'))
        self.assertTrue(response.json.get('Username'))
        self.assertTrue(response.json.get('Email'))
        self.assertTrue(response.json.get('FirstName'))
        self.assertTrue(response.json.get('Surname'))
        self.assertTrue(response.json.get('CreatedAt'))

        user_intersect = set(response.json) & set(USER_NOT_ADMIN)

        # Let's see if original values and response values match
        for key in user_intersect:
            if key == 'isAdmin':
                continue
            assert response.json.get(key) == USER_NOT_ADMIN.get(
                key), "Keys were response: {} vs original: {}"\
                .format(response.json.get(key), USER_NOT_ADMIN.get(key))

        logging.info("TEST_API_AUTH_POST_VALID_USERNAME_PASSWORD Succesful.")

    def test_api_auth_post_invalid_username(self):
        """
        Testcase for invalid username. Let's try with non-existing user.
        It should return 401 Unauthorized.
        """
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        auth_body = {
            'userName': 'nonexistent',
            'password': USER_NOT_ADMIN.get("NewPassword")
        }

        request_uri = '/api/auth/'
        response = self.client.open(
            request_uri,
            method='POST',
            headers=headers,
            data=json.dumps(auth_body),
            content_type='application/json')

        assert response.status_code == 401, "Response code in response was {}"\
            .format(response.status_code)
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])
        logging.info("TEST_API_AUTH_POST_INVALID_USERNAME Succesful.")

    def test_api_auth_post_invalid_password(self):
        """
        Testcase for invalid password.
        It should return 401 Unauthorized.
        """
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        auth_body = {
            'userName': USER1_FROM_DB_ADMIN.get('Username'),
            'password': 'SomePassword'
        }
        request_uri = '/api/auth/'
        response = self.client.open(
            request_uri,
            method='POST',
            headers=headers,
            data=json.dumps(auth_body),
            content_type='application/json')

        assert response.status_code == 401, "Response code in response was {}"\
            .format(response.status_code)
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])
        logging.info("TEST_API_AUTH_POST_INVALID_PASSWORD Succesful.")

    def test_api_auth_post_missing_username(self):
        """
        Testcase for missing username.
        It should return 400 Bad Request.
        """
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        auth_body = {
            'password': 'Username is missing'
        }
        request_uri = '/api/auth/'
        response = self.client.open(
            request_uri,
            method='POST',
            headers=headers,
            data=json.dumps(auth_body),
            content_type='application/json')

        assert response.status_code == 400, "Response code in response was {}"\
            .format(response.status_code)
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])
        logging.info("TEST_API_AUTH_POST_MISSING_USERNAME Succesful.")

    def test_api_auth_post_missing_password(self):
        """
        Testcase for missing password.
        It should return 400 Bad Request.
        """
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        auth_body = {
            'userName': USER1_FROM_DB_ADMIN.get('Username')
        }
        request_uri = '/api/auth/'
        response = self.client.open(
            request_uri,
            method='POST',
            headers=headers,
            data=json.dumps(auth_body),
            content_type='application/json')

        assert response.status_code == 400, "Response code in response was {}"\
            .format(response.status_code)
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])
        logging.info("TEST_API_AUTH_POST_MISSING_PASSWORD Succesful.")

    def test_api_auth_post_invalid_content_type(self):
        """
        Testcase for invalid content type header.
        It should return 415 Unsupported Media
        """
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/xml',
        }
        auth_body = {
            'userName': USER1_FROM_DB_ADMIN.get('Username'),
            'password': 'SomePassword'
        }
        request_uri = '/api/auth/'
        response = self.client.open(
            request_uri,
            method='POST',
            headers=headers,
            data=json.dumps(auth_body),
            content_type='application/xml')

        assert response.status_code == 415, "Response code in response was {}"\
            .format(response.status_code)
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])
        logging.info("TEST_API_AUTH_POST_INVALID_CONTENT_TYPE Succesful.")

    def test_api_auth_post_invalid_content_json(self):
        """
        Testcase for invalid content. Content does not match
        header. In this case, broken json.
        It should return 400 Bad Request
        """
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        auth_body = "{'userName USER1_FROM_DB_ADMIN.get('Username')"\
            ",'password': 'SomePassword'}"
        request_uri = '/api/auth/'
        response = self.client.open(
            request_uri,
            method='POST',
            headers=headers,
            data=auth_body,
            content_type='application/json')

        assert response.status_code == 400, "Response code in response was {}"\
            .format(response.status_code)
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])

        logging.info("TEST_API_AUTH_POST_INVALID_CONTENT_JSON Succesful.")


if __name__ == '__main__':
    unittest.main()
