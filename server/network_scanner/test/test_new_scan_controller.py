# coding: utf-8

from __future__ import absolute_import
import unittest

from flask import json
from six import BytesIO
import logging
from network_scanner.test import BaseTestCase
from network_scanner.test.fake_data import TOKEN_SINGLE_USER,\
    SCAN_SINGLE_TARGET, USER2_FROM_DB_NOT_ADMIN, SCAN_FROM_FILE


class TestNewScanController(BaseTestCase):
    """
    NewScanController integration test stubs

    Tests acquiring the information to make new scan
    and tests creation of new scan.
    """

    def test_api_user_user_id_scans_new_scan_get(self):
        """
        Test case for api_user_user_id_scans_new_scan_get

        Acquire scan options information
        """
        headers = {
            'Accept': 'application/vnd.mason+json',
            'Authorization': 'Bearer ' + TOKEN_SINGLE_USER
        }
        response = self.client.open(
            '/api/users/{}/new-scan'.format(USER2_FROM_DB_NOT_ADMIN.get("ID")),
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))
        assert response.json.get('@namespaces')
        assert response.json.get('detailedInformation')
        assert response.json.get('detailedInformation').get('description')
        assert response.json.get('detailedInformation').get('href')
        self.validateResponseControls(response, [
                                      'self', 'netscan:logout-user',
                                      'netscan:scans-all',
                                      'netscan:make-new-scan'])
        logging.info("TEST_API_USER_USER_ID_SCANS_NEW_SCAN_GET Successful.")

    def test_api_user_user_id_scans_new_scan_post_single_target(self):
        """
        Test case for api_user_user_id_scans_new_scan_post

        Posting, with two null attributes. Should be fine.
        """
        body = SCAN_SINGLE_TARGET

        headers = {
            'Accept': 'application/vnd.mason+json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + TOKEN_SINGLE_USER
        }
        request_uri = '/api/users/{}/new-scan'.format(
            USER2_FROM_DB_NOT_ADMIN.get("ID"))
        response = self.client.open(
            request_uri,
            method='POST',
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')
        assert response.status_code == 201, "Response code in response was {}"\
            .format(response.status_code)
        assert response.headers.get('location')
        logging.info(
            "TEST_API_USER_USER_ID_SCANS_NEW_SCAN"
            "_POST_SINGLE_TARGET Successful.")

    def test_api_user_user_id_scans_new_scan_post_commands_from_file(self):
        """
        Test case for api_user_user_id_scans_new_scan_post

        Testing the use of all attributes.
        """
        body = SCAN_FROM_FILE

        headers = {
            'Accept': 'application/vnd.mason+json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + TOKEN_SINGLE_USER
        }
        request_uri = '/api/users/{}/new-scan'.format(
            USER2_FROM_DB_NOT_ADMIN.get("ID"))
        response = self.client.open(
            request_uri,
            method='POST',
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')
        assert response.status_code == 201, "Response code in response was {}"\
            .format(response.status_code)
        assert response.headers.get('location')
        logging.info("TEST_API_USER_USER_ID_SCANS_NEW_SCAN_POST_"
                     "COMMAND_FROM_FILE Successful.")

    def test_api_user_user_id_scans_new_scan_post_invalid_content_type(self):
        """
        Test case for api_user_user_id_scans_new_scan_post

        Testing, that API accepts only valid contenty type bodies
        """
        body = SCAN_SINGLE_TARGET

        headers = {
            'Accept': 'application/vnd.mason+json',
            'Content-Type': 'application/xml',
            'Authorization': 'Bearer ' + TOKEN_SINGLE_USER
        }
        request_uri = '/api/users/{}/new-scan'.format(
            USER2_FROM_DB_NOT_ADMIN.get("ID"))
        response = self.client.open(
            request_uri,
            method='POST',
            headers=headers,
            data=json.dumps(body),
            content_type='application/xml')
        assert response.status_code == 415, "Response code in response was {}"\
            .format(response.status_code)
        self.validateErrorResponseBody(
            response, request_uri, ['profile'])
        logging.info(
            "TEST_API_USER_USER_ID_SCANS_NEW_SCAN"
            "_POST_INVALID_CONTENT_TYPE Successful.")

    def test_api_user_user_id_scans_new_scan_post_invalid_json(self):
        """
        Test case for api_user_user_id_scans_new_scan_post

        Testing, that API accepts only valid JSONs
        """
        body = "{target':'test}"

        headers = {
            'Accept': 'application/vnd.mason+json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + TOKEN_SINGLE_USER
        }
        request_uri = '/api/users/{}/new-scan'.format(
            USER2_FROM_DB_NOT_ADMIN.get("ID"))
        response = self.client.open(
            request_uri,
            method='POST',
            headers=headers,
            data=body,
            content_type='application/json')
        assert response.status_code == 400, "Response code in response was {}"\
            .format(response.status_code)
        self.validateErrorResponseBody(
            response, request_uri, ['profile'])
        logging.info(
            "TEST_API_USER_USER_ID_SCANS_NEW_SCAN"
            "_POST_INVALID_JSON Successful.")

    def test_api_user_user_id_scans_new_scan_post_missing_required(self):
        """
        Test case for api_user_user_id_scans_new_scan_post

        Testing required attributes for POST method.
        """

        # Test first required attribute
        body = SCAN_SINGLE_TARGET.copy()
        del body["isSingleTarget"]

        headers = {
            'Accept': 'application/vnd.mason+json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + TOKEN_SINGLE_USER
        }
        request_uri = '/api/users/{}/new-scan'.format(
            USER2_FROM_DB_NOT_ADMIN.get("ID"))
        response = self.client.open(
            request_uri,
            method='POST',
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')
        assert response.status_code == 400, "Response code in response was {}"\
            .format(response.status_code)
        self.validateErrorResponseBody(
            response, request_uri, ['profile'])

        # Test second required attribute
        body = SCAN_SINGLE_TARGET.copy()
        del body["target"]

        headers = {
            'Accept': 'application/vnd.mason+json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + TOKEN_SINGLE_USER
        }
        request_uri = '/api/users/{}/new-scan'.format(
            USER2_FROM_DB_NOT_ADMIN.get("ID"))
        response = self.client.open(
            request_uri,
            method='POST',
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')
        assert response.status_code == 400, "Response code in response was {}"\
            .format(response.status_code)
        self.validateErrorResponseBody(
            response, request_uri, ['profile'])
            
        logging.info(
            "TEST_API_USER_USER_ID_SCANS_NEW_SCAN"
            "_POST_MISSING_REQUIRED Successful.")


if __name__ == '__main__':
    unittest.main()
