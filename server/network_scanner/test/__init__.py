#!/usr/bin/env python3
from flask_testing import TestCase
from network_scanner.config import initConnexionApp
from network_scanner.new_models.ErrorResponse import ErrorResponse
from network_scanner.models.accesstoken import Accesstoken


class BaseTestCase(TestCase):

    def create_app(self):
        ConnexionWrapper = initConnexionApp(debug=False)
        ConnexionWrapper.addErrorHandlers()
        ConnexionWrapper.configFlaskApp()
        ConnexionWrapper.initApiConf()
        ConnexionWrapper.initDatabase()
        return ConnexionWrapper.getConnexionInstance().app

    def validateAccesstoken(self, response):
        """
        Method for validating accesstoken.
        Reducing amount of same code in tests.

        :param response: Flask response object
        """
        session = Accesstoken.from_dict(response.json)
        self.assertTrue(session.access_token)
        self.assertTrue(session.scope)
        self.assertTrue(session.refresh_token)
        assert session.token_type == 'Bearer'
        self.assertTrue(session.expires_in)

    def validateErrorResponseBody(self, response, request_uri,
                                  list_of_control_names=None):
        """
        Method for validating error response body briefly.
        Checking also, that response has required controls,
        and request URI matching the resource_url in
        error response.

        :param response: Flask response object
        :param request_uri: URI what request was accessing
        :param list_of_control_names: List of controls that
        response should contain.
        """
        ErrorResponseBody = ErrorResponse.from_dict(response.json)
        self.assertTrue(ErrorResponseBody.namespaces)
        self.assertTrue(ErrorResponseBody.controls)
        self.assertTrue(ErrorResponseBody.error)
        self.assertTrue(ErrorResponseBody.error.messages)
        self.assertTrue(ErrorResponseBody.error.message)
        self.assertEqual(ErrorResponseBody.resource_url, request_uri)
        if list_of_control_names:
            self.validateResponseControls(response, list_of_control_names)

    def validateResponseControls(self, response, list_of_control_names):
        """
        Method for validating, that response contains
        required controls.

        :param response: Flask response object
        :param list_of_control_names: List of controls that
        response should contain.
        """
        for control in list_of_control_names:
            assert response.json.get("@controls").get(control), \
                "Control {} not found.".format(control)
