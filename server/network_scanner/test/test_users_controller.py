# coding: utf-8
import os
import unittest
import logging
from flask import json
from six import BytesIO

from network_scanner.models.user_post_put import UserPostPut  # noqa: E501
from network_scanner.test import BaseTestCase
from network_scanner.config import DEFAULT_TOKEN_EXPIRING_TIME
from network_scanner.uri_conf import USERS_URI, _ROOT_HOSTNAME
from network_scanner.models.accesstoken import Accesstoken
from network_scanner.test.fake_data import USER_ADMIN, USER_NOT_ADMIN, \
    USER1_FROM_DB_ADMIN, USER2_FROM_DB_NOT_ADMIN
TOKEN_SINGLE_USER = 'qwerty'  # for John Doe, not admin, ID 2
TOKEN_ADMIN = '12312AB3info3i'  # for Very first user, is admin, ID 1


class TestUsersController(BaseTestCase):
    """UsersController integration tests"""

    def test_broken_database_users(self):
        """
        Let's remove the database file, and see what happens!
        We are excepting Internal Server Error in any query
        related to users.
        """

        basedir = os.path.abspath(os.path.dirname(__file__))
        os.remove(os.path.abspath(os.path.join(basedir, os.pardir)) +
                  '/db/temporary.db')

        headers = {
            'Accept': 'application/vnd.mason+json',
            # Valid key for single user 2: qwerty
            'Authorization': 'Bearer ' + TOKEN_SINGLE_USER,
        }
        response = self.client.open(
            USERS_URI,
            method='GET',
            headers=headers)
        self.assert500(response,
                       'Response body is : ' + response.data.decode('utf-8'))
        logging.info("TEST_BROKEN_DATABASE_USERS Succesful.")

    def test_api_users_get(self):
        """Test case for api_users_get

        List all users with valid token (Not admin)
        Should be single user, with userID 2
        """
        headers = {
            'Accept': 'application/vnd.mason+json',
            # Valid key for single user 2: qwerty
            'Authorization': 'Bearer ' + TOKEN_SINGLE_USER,
        }
        response = self.client.open(
            USERS_URI,
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

        # Validate that body has core attributes
        self.assertTrue(response.json.get("@namespaces"))
        self.validateResponseControls(response,
                                      ['self', 'netscan:add-user',
                                       'netscan:logout-user'])
        self.assertTrue(response.json.get("items"))

        # Validating that list has corresponsding item for token
        assert response.json.get('items')[0].get('ID') == 2
        self.assertTrue(response.json.get('items')[0].get('@controls'))

        # Validate that self control is for Users
        self.assertEqual(response.json["@controls"]["self"]["href"], '{}'
                         .format(USERS_URI))

        logging.info("TEST_API_USERS_GET Succesful.")

    def test_api_users_get_list_with_admin(self):
        """
        Getting all users with admin token
        This test is an exception. All other authentication
        tests are in separe file.
        Excepting 200 response, all users
        """
        headers = {
            'Accept': 'application/vnd.mason+json',
            'Authorization': 'Bearer ' + TOKEN_ADMIN,
        }
        response = self.client.open(
            USERS_URI,
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

        # Validate that body has core attributes
        self.assertTrue(response.json.get("@namespaces"))
        self.validateResponseControls(response,
                                      ['self', 'netscan:add-user',
                                       'netscan:logout-user'])
        # Check that all 2 users are in response
        assert len(response.json.get('items')) == 2

        logging.info("TEST_API_USERS_GET_LIST_WITH_ADMIN Succesful.")

    def test_api_users_user_id_get(self):
        """Test case for api_users_user_id_get

        User information
        """
        headers = {
            'Accept': 'application/vnd.mason+json',
            # 'accept': application/vnd.mason+json,
            # Qwerty is user number 2
            'Authorization': 'Bearer qwerty',
        }
        response = self.client.open(
            '/api/users/{user_id}'.format(user_id=2),
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

        self.validateResponseControls(response,
                                      ['self', 'profile', 'up',
                                       'netscan:scans-all',
                                       'netscan:devices-all',
                                       'netscan:new-scan',
                                       'netscan:edit-user',
                                       'netscan:delete-user',
                                       'netscan:logout-user'])

        self.assertTrue(response.json.get('ID'))
        self.assertTrue(response.json.get('Username'))
        self.assertTrue(response.json.get('Email'))
        self.assertTrue(response.json.get('FirstName'))
        self.assertTrue(response.json.get('Surname'))
        self.assertTrue(response.json.get('CreatedAt'))
        # Not admin, returning false with get
        self.assertTrue('isAdmin' in response.json)

        # Checking that response contains correct values for same attributes
        user_intersect = set(response.json) & set(USER2_FROM_DB_NOT_ADMIN)
        for key in user_intersect:
            # logging.info("Response value: {}  DB value: {}".format(
            #    response.json.get(key),
            #    USER2_FROM_DB_NOT_ADMIN.get(key)))
            if key == 'LastLogin' or key == 'CreatedAt':
                continue
            assert response.json.get(key) == USER2_FROM_DB_NOT_ADMIN.get(key),\
                "Key was {}".format(key)
            # logging.info(key)

        logging.info("TEST_API_USERS_USER_ID_GET Succesful.")

    def test_api_users_post_validate_user_and_new_token(self):
        """
        Test case for creating a new user.
        This test creates a user,
        get's a newly created authentication token,
        and with it tries to access this new user with GET request,
        by using Location header uri,
        and confirms that user matches what was created.
        """

        body = USER_NOT_ADMIN
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        response = self.client.open(
            '/api/users/',
            method='POST',
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')

        assert response.status_code == 201, "Response code in response was {}"\
            .format(response.status_code)
        self.validateAccesstoken(response)
        test_token = Accesstoken()
        test_token = test_token.from_dict(response.json)
        assert test_token.token_type == 'Bearer'
        assert test_token.refresh_token != test_token.access_token
        assert test_token.scope == 'specific-user'
        assert test_token.expires_in == DEFAULT_TOKEN_EXPIRING_TIME
        # logging.info(response.headers['Location'])
        assert response.headers['Location'] == 'http://localhost/api/users/3/'

        # Add newly aqcuired token to headers
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': '{} '.format(response.json["token_type"]) +
            response.json['access_token']
        }

        # GET new user
        response = self.client.open(
            '/api/users/{user_id}'.format(user_id=3),
            method='GET',
            headers=headers)

        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

        assert response.json.get('Username') == USER_NOT_ADMIN.get('Username')
        assert response.json.get('isAdmin') is False
        assert response.json.get(
            'FirstName') == USER_NOT_ADMIN.get('FirstName')
        assert response.json.get('Surname') == USER_NOT_ADMIN.get('Surname')
        assert response.json.get('Email') == USER_NOT_ADMIN.get('Email')

        logging.info("TEST_API_USERS_POST_VALIDATE_USER_AND_NEW_TOKEN"
                     " Succesful.")

    def test_api_users_post_invalid_content_type_header(self, method='POST'):
        """
        Test case for creating a new user.
        This test tries to create new user
        with invalid content type header.
        """

        body = USER_NOT_ADMIN

        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/xml',
        }
        request_uri = '/api/users/'
        response = self.client.open(
            request_uri,
            method=method,
            headers=headers,
            data=json.dumps(body),
            content_type='application/xml')

        assert response.status_code == 415, "Response code in response was {}"\
            .format(response.status_code)
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])
        logging.info("TEST_API_USERS_POST_INVALID_CONTENT_TYPE_HEADER"
                     " Succesful.")

    def test_api_users_post_invalid_json_format(self, method='POST'):
        """
        Test case for creating a new user.
        This test tries to create new user
        with invalid json format.
        """

        body = '{"Username":"test", "Email":"not.valid@atall.com",'
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        request_uri = '/api/users/'
        response = self.client.open(
            request_uri,
            method=method,
            headers=headers,
            data=body,
            content_type='application/json')

        assert response.status_code == 400, "Response code in response was {}"\
            .format(response.status_code)
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])
        logging.info("TEST_API_USERS_POST_INVALID_JSON_FORMAT"
                     " Succesful.")

    def test_api_users_post_missing_required(self, method='POST'):
        """
        Test case for creating a new user.
        Post is missing required attributes.
        Testing all attributes one by one.
        """

        body = USER_NOT_ADMIN.copy()
        del body["Username"]
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        request_uri = '/api/users/'

        response = self.client.open(
            request_uri,
            method=method,
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')

        assert response.status_code == 400, "Response code in response was {}"\
            .format(response.status_code)
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])

        body = USER_NOT_ADMIN.copy()
        del body["Email"]

        response = self.client.open(
            request_uri,
            method=method,
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')

        assert response.status_code == 400, "Response code in response was {}"\
            .format(response.status_code)
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])

        body = USER_NOT_ADMIN.copy()
        del body["NewPassword"]

        response = self.client.open(
            request_uri,
            method=method,
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')

        assert response.status_code == 400, "Response code in response was {}"\
            .format(response.status_code)
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])

        body = USER_NOT_ADMIN.copy()
        del body["RepeatNewPassword"]

        response = self.client.open(
            request_uri,
            method=method,
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')

        assert response.status_code == 400, "Response code in response was {}"\
            .format(response.status_code)
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])

        logging.info("TEST_API_USERS_POST_MISSING_REQUIRED"
                     " Succesful.")

    def test_api_users_post_not_matching_passwords(self, method='POST'):
        """
        Test case for creating a new user.
        This test tries to create new user
        with non-matching passwords.
        """

        body = USER_NOT_ADMIN.copy()
        body["RepeatNewPassword"] = 'Hahaha'
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        request_uri = '/api/users/'
        response = self.client.open(
            request_uri,
            method=method,
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')

        assert response.status_code == 400, "Response code in response was {}"\
            .format(response.status_code)
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])
        logging.info("TEST_API_USERS_POST_NOT_MATCHING_PASSWORDS"
                     " Succesful.")

    def test_api_users_post_recreating_unique_values(self, method='POST'):
        """
        Test case for creating a new user.
        This test tries to create new user with attribute values,
        which exists already and must be unique.
        """

        body = USER_NOT_ADMIN.copy()
        # Already exist, username must be unique
        body["Username"] = 'VeryFirstUser'
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        request_uri = '/api/users/'
        response = self.client.open(
            request_uri,
            method=method,
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')

        assert response.status_code == 409, "Response code in response was {}"\
            .format(response.status_code)
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])

        body = USER_NOT_ADMIN.copy()
        # Already exist, email must be unique
        body["Email"] = 'very.first@user.com'
        response = self.client.open(
            request_uri,
            method=method,
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')

        assert response.status_code == 409, "Response code in response was {}"\
            .format(response.status_code)
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])

        logging.info("TEST_API_USERS_POST_RECREATING_UNIQUE_VALUES"
                     " Succesful.")

    def test_api_users_put_invalid_content_type_header(self):
        """Test case for api_users_user_id_put

        Edit user information, with invalid content-type header.
        Expecting 415 Response
        """
        """
        Test case for creating a new user.
        This test tries to create new user
        with invalid content type header.
        """

        body = USER_NOT_ADMIN

        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/xml',
            'Authorization': 'Bearer ' + TOKEN_SINGLE_USER
        }
        request_uri = '/api/users/2/'
        response = self.client.open(
            request_uri,
            method='PUT',
            headers=headers,
            data=json.dumps(body),
            content_type='application/xml')

        assert response.status_code == 415, "Response code in response was {}"\
            .format(response.status_code)
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])
        logging.info("TEST_API_USERS_PUT_INVALID_CONTENT_TYPE_HEADER"
                     " Succesful.")

    def test_api_users_put_invalid_json_format(self):
        """
        Test case for api_users_user_id_put

        Edit user information, with invalid json.
        Expecting 400 Response
        """
        """
        Test case for creating a new user.
        This test tries to create new user
        with invalid content type header.
        """

        body = '{"Username":"test", "Email":"not.valid@atall.com",'

        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + TOKEN_SINGLE_USER
        }
        request_uri = '/api/users/2/'
        response = self.client.open(
            request_uri,
            method='PUT',
            headers=headers,
            data=body,
            content_type='application/json')

        assert response.status_code == 400, "Response code in response was {}"\
            .format(response.status_code)
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])
        logging.info("TEST_API_USERS_PUT_INVALID_JSON_FORMAT"
                     " Succesful.")

    def test_api_users_put_missing_required(self, method='PUT'):
        """
        Test case for editing existing user.
        Put is missing required attributes.
        Testing all attributes one by one.
        """

        request_uri = '/api/users/2/'
        body = USER_NOT_ADMIN.copy()
        del body["Username"]

        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + TOKEN_SINGLE_USER
        }

        response = self.client.open(
            request_uri,
            method=method,
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')

        assert response.status_code == 400, "Response code in response was {}"\
            .format(response.status_code)
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])

        body = USER_NOT_ADMIN.copy()
        del body["Email"]

        response = self.client.open(
            request_uri,
            method=method,
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')

        assert response.status_code == 400, "Response code in response was {}"\
            .format(response.status_code)
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])

        body = USER_NOT_ADMIN.copy()
        del body["NewPassword"]

        response = self.client.open(
            request_uri,
            method=method,
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')

        assert response.status_code == 400, "Response code in response was {}"\
            .format(response.status_code)
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])

        body = USER_NOT_ADMIN.copy()
        del body["RepeatNewPassword"]

        response = self.client.open(
            request_uri,
            method=method,
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')

        assert response.status_code == 400, "Response code in response was {}"\
            .format(response.status_code)
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])

        logging.info("TEST_API_USERS_PUT_MISSING_REQUIRED"
                     " Succesful.")

    def test_api_users_put_not_matching_passwords(self, method='PUT'):
        """
        Test case for editing existing user.
        This test tries to edit user
        with non-matching passwords.
        """

        body = USER_NOT_ADMIN.copy()
        body["RepeatNewPassword"] = 'Hahaha'
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + TOKEN_SINGLE_USER
        }
        request_uri = '/api/users/2/'
        response = self.client.open(
            request_uri,
            method=method,
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')

        assert response.status_code == 400, "Response code in response was {}"\
            .format(response.status_code)
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])
        logging.info("TEST_API_USERS_PUT_NOT_MATCHING_PASSWORDS"
                     " Succesful.")

    def test_api_users_put_recreating_unique_values(self, method='PUT'):
        """
        Test case for creating a new user.
        This test tries to create new user with attribute values,
        which exists already and must be unique.
        """

        body = USER_NOT_ADMIN.copy()
        # Already exist, username must be unique
        # User 2 trying to take username of user 1
        body["Username"] = 'VeryFirstUser'
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + TOKEN_SINGLE_USER
        }
        request_uri = '/api/users/2/'
        response = self.client.open(
            request_uri,
            method=method,
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')

        assert response.status_code == 409, "Response code in response was {}"\
            .format(response.status_code)
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])

        body = USER_NOT_ADMIN.copy()
        # Already exist, email must be unique
        body["Email"] = 'very.first@user.com'
        response = self.client.open(
            request_uri,
            method=method,
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')

        assert response.status_code == 409, "Response code in response was {}"\
            .format(response.status_code)
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])

        logging.info("TEST_API_USERS_PUT_RECREATING_UNIQUE_VALUES"
                     " Succesful.")

    def test_api_users_user_id_delete(self):
        """
        Test case for api_users_user_id_delete

        Delete this user
        Testing, if token is still valid after deleting.
        Testing, if user still exists by querying with admin rights
        """
        headers = {
            'Accept': 'application/vnd.mason+json',
            'Authorization': 'Bearer qwerty',
        }
        response = self.client.open(
            '/api/users/{user_id}'.format(user_id=2),
            method='DELETE',
            headers=headers)

        # Check for response code
        assert response.status_code == 204

        request_uri = '/api/users/{user_id}'.format(user_id=2)
        # Make get request to see if user still exists
        response = self.client.open(
            request_uri,
            method='GET',
            headers=headers)

        # Token should not be valid anymore
        self.assert401(response,
                       'Response body is : ' + response.data.decode('utf-8'))

        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])
        # New request with admin rights
        headers = {
            'Accept': 'application/vnd.mason+json',
            'Authorization': 'Bearer ' + TOKEN_ADMIN,
        }
        response = self.client.open(
            request_uri,
            method='DELETE',
            headers=headers)

        # We should get Not Found response
        self.assert404(response,
                       'Response body is : ' + response.data.decode('utf-8'))

        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])
        logging.info("TEST_API_USERS_USER_ID_DELETE Succesful.")


if __name__ == '__main__':
    unittest.main()
