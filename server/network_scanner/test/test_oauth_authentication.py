
from __future__ import absolute_import
import unittest
import logging
from flask import json
from six import BytesIO

from network_scanner.test import BaseTestCase
from network_scanner.uri_conf import USERS_URI
from network_scanner.test.fake_data import USER_ADMIN, USER_NOT_ADMIN,\
    TOKEN_ADMIN, TOKEN_SINGLE_USER
# for Very first user, is admin, ID 1


class TestOAuthAuthentication(BaseTestCase):
    """
    OAuth 2 general integration tests.

    NOTE: Our OAuth2 is not perfect implementation how it is supposed
    to work. We have some permanent tokens, and OAuth 2 is implemented
    on same server, accessing to same database where are other resources.

    Some authentication tests are in test_users_controller as well.
    In general, in most of the endpoints, valid token is required for
    accessing the resource, and at least possibility to access for that
    resource is additionally tested.

    All authentication is going through several functions, and
    relying on validating the Token VS User, therefore we except
    that tests here are enough to validy each access point in the API,
    if we test those functions.

    Even though it could be more comprehensive to test on each
    endpoint as well.

    TESTS IN test_users_controller:
    List all users with admin token
    List all users with single user token -> returning single user,
    which is corresponding the token.

    """

    def test_valid_token_get_correct_user(self):
        """Test case for valid token and show user information

        """
        headers = {
            'Accept': 'application/vnd.mason+json',
            'Authorization': 'Bearer ' + TOKEN_SINGLE_USER
        }
        response = self.client.open(
            '/api/users/2/',
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

        assert response.json.get("ID") == 2
        logging.info("TEST_VALID_TOKEN_GET_CORRECT_USER Successful.")

    def test_valid_token_get_incorrect_user(self):
        """Test case for valid token and for wrong user
        Excepting 403 Forbidden response

        """
        headers = {
            'Accept': 'application/vnd.mason+json',
            'Authorization': 'Bearer ' + TOKEN_SINGLE_USER
        }
        # NOTE user ID not correct
        request_uri = '/api/users/1/'
        response = self.client.open(
            request_uri,
            method='GET',
            headers=headers)
        self.assert403(response,
                       'Response body is : ' + response.data.decode('utf-8'))
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])
        logging.info("TEST_VALID_TOKEN_GET_INCORRECT_USER Successful.")

    def test_users_get_invalid_token(self):
        """
        Accessing to all users with invalid token.
        Excepting 401 Unauthorized response, no users
        Includes controls for auth route and guide for adding new user
        """
        headers = {
            'Accept': 'application/vnd.mason+json',
            'Authorization': 'Bearer qwertyy',
        }
        response = self.client.open(
            USERS_URI,
            method='GET',
            headers=headers)
        self.assert401(response,
                       'Response body is : ' + response.data.decode('utf-8'))
        self.validateErrorResponseBody(response, USERS_URI,
                                       ['profile'])
        logging.info("TEST_USERS_GET_INVALID_TOKEN Successful.")

    def test_invalid_token_get_single_user(self):
        """
        Test case for valid token and show single user information
        """
        headers = {
            'Accept': 'application/vnd.mason+json',
            'Authorization': 'Bearer ' + '1234567890'
        }
        request_uri = '/api/users/1/'
        response = self.client.open(
            request_uri,
            method='GET',
            headers=headers)
        self.assert401(response,
                       'Response body is : ' + response.data.decode('utf-8'))
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])
        logging.info("TEST_INVALID_TOKEN_GET_SINGLE_USER Successful.")

    def test_invalid_header_type_get_single_user(self):
        """
        Test case for valid token and show single user information
        """
        headers = {
            'Accept': 'application/vnd.mason+json',
            'Authorization': 'Basic ' + '1234567890'
        }
        request_uri = '/api/users/1/'
        response = self.client.open(
            request_uri,
            method='GET',
            headers=headers)
        self.assert401(response,
                       'Response body is : ' + response.data.decode('utf-8'))
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])
        logging.info("TEST_INVALID_HEADER_TYPE_GET_SINGLE_USER Successful.")

    def test_no_token_get_single_user(self):
        """
        Test case for not providing token.
        Error controls include in this case add-user and auth instructions.

        """
        headers = {
            'Accept': 'application/vnd.mason+json'
        }
        request_uri = '/api/users/1/'
        response = self.client.open(
            request_uri,
            method='GET',
            headers=headers)
        self.assert401(response,
                       'Response body is : ' + response.data.decode('utf-8'))
        self.validateErrorResponseBody(response, request_uri,
                                       ['netscan:auth', 'netscan:add-user',
                                        'profile'])
        logging.info("TEST_NO_TOKEN_GET_SINGLE_USER Successful.")

    def test_with_admin_token_not_found_single_user(self):
        """
        Test case for admin accessing non-existent user
        """
        headers = {
            'Accept': 'application/vnd.mason+json',
            'Authorization': 'Bearer ' + TOKEN_ADMIN
        }
        request_uri = '/api/users/3/'
        response = self.client.open(
            request_uri,
            method='GET',
            headers=headers)
        self.assert404(response,
                       'Response body is : ' + response.data.decode('utf-8'))
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])
        logging.info("TEST_WITH_ADMIN_TOKEN_NOT_FOUND_SINGLE_USER Successful.")

    def test_admin_token_get_single_user(self):
        """
        Test case for valid admin token and show single user information,
        different than admin
        """
        headers = {
            'Accept': 'application/vnd.mason+json',
            'Authorization': 'Bearer ' + TOKEN_ADMIN
        }
        # Admin has user_id 1
        response = self.client.open(
            '/api/users/2/',
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))
        assert response.json.get("ID") == 2
        logging.info("TEST_ADMIN_TOKEN_GET_SINGLE_USER Successful.")

    """
    Tests for POST method starting.
    """

    def test_invalid_token_post_new_user(self):
        """
        To create a new user, no valid token is required.
        But let's try to create user with invalid token anyway.
        """

        body = USER_NOT_ADMIN

        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + '1234567890'
        }
        response = self.client.open(
            '/api/users/',
            method='POST',
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')

        # 'Created' is valid response
        assert response.status_code == 201
        self.validateAccesstoken(response)
        logging.info("TEST_INVALID_TOKEN_POST_NEW_USER Successful.")

    def test_no_token_post_new_user(self):
        """
        To create a new user, no valid token is required.
        But let's try to create user without token anyway.
        """

        body = USER_NOT_ADMIN

        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        response = self.client.open(
            '/api/users/',
            method='POST',
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')

        # 'Created' is valid response
        assert response.status_code == 201
        self.validateAccesstoken(response)
        logging.info("TEST_NO_TOKEN_POST_NEW_USER Successful.")

    """
    Tests for PUT method starting
    """

    def test_valid_token_put_existing_user(self):
        """
        Edit single user with single user token.
        Token for user 2, target user 2.
        Expecting 204 response from edit, new content
        validated to be correct.
        """

        body = USER_NOT_ADMIN
        body["Username"] = "Edited_username"
        body["Email"] = "New.Email@byPut.com"

        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + TOKEN_SINGLE_USER
        }
        request_uri = '/api/users/2/'
        response = self.client.open(
            request_uri,
            method='PUT',
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')

        # Check if response code is valid (no content)
        assert response.status_code == 204

        # Let's validate the new data
        response = self.client.open(
            request_uri,
            method='GET',
            headers=headers
        )
        assert response.status_code == 200

        assert response.json.get('Username') == 'Edited_username'
        assert response.json.get('Email') == 'New.Email@byPut.com'
        logging.info("TEST_VALID_TOKEN_PUT_EXISTING_USER Successful.")

    def test_invalid_token_put_existing_user(self):
        """
        Edit single user with invalid user token.
        Expecting Unauthorized 401 response.
        """

        body = USER_NOT_ADMIN
        body["Username"] = "Edited_username"
        body["Email"] = "New.Email@byPut.com"

        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + '1234567890'
        }
        request_uri = '/api/users/2/'
        response = self.client.open(
            request_uri,
            method='PUT',
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')

        assert response.status_code == 401
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])

        logging.info("TEST_INVALID_TOKEN_PUT_EXISTING_USER Successful.")

    def test_valid_token_put_wrong_existing_user(self):
        """
        Edit single user with invalid user token.
        Expecting Forbidden 403 response.
        """

        body = USER_NOT_ADMIN
        body["Username"] = "Edited_username"
        body["Email"] = "New.Email@byPut.com"

        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + TOKEN_SINGLE_USER
            # User_id 2 for token
        }
        request_uri = '/api/users/1/'
        response = self.client.open(
            request_uri,
            method='PUT',
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')

        assert response.status_code == 403
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])

        logging.info("TEST_VALID_TOKEN_PUT_WRONG_EXISTING_USER Successful.")

    def test_admin_token_put_existing_user(self):
        """
        Edit single user with single user token.
        Expecting 204 response from edit, new content
        validated to be correct.
        """

        body = USER_NOT_ADMIN
        body["Username"] = "Edited_username"
        body["Email"] = "New.Email@byPut.com"

        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + TOKEN_ADMIN
        }
        # NOTE user_id of admin is 1
        request_uri = '/api/users/2/'
        response = self.client.open(
            request_uri,
            method='PUT',
            headers=headers,
            data=json.dumps(body),
            content_type='application/json')

        # Check if response code is valid (no content)
        assert response.status_code == 204

        # Let's validate the new data
        response = self.client.open(
            request_uri,
            method='GET',
            headers=headers
        )
        assert response.status_code == 200

        assert response.json.get('Username') == 'Edited_username'
        assert response.json.get('Email') == 'New.Email@byPut.com'
        logging.info("TEST_ADMIN_TOKEN_PUT_EXISTING_USER Successful.")

    """
    Tests for DELETE method starting.
    """

    def test_invalid_and_no_token_delete_existing_user(self):
        """
        Delete single user with invalid user token.
        Expecting Unauthorized 401 response.
        """

        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + '1234567890'
        }
        request_uri = '/api/users/2/'
        response = self.client.open(
            request_uri,
            method='DELETE',
            headers=headers
        )

        assert response.status_code == 401
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])

        del headers["Authorization"]
        response = self.client.open(
            request_uri,
            method='DELETE',
            headers=headers
        )
        assert response.status_code == 401
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile', 'netscan:add-user',
                                        'netscan:auth'])

        logging.info(
            "TEST_INVALID_AND_NO_TOKEN_DELETE_EXISTING_USER Successful.")

    def test_valid_token_delete_existing_user(self):
        """
        Delete single user with valid user token.
        Expecting Succesful No Content 204 response.
        """

        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + TOKEN_SINGLE_USER
        }
        request_uri = '/api/users/2/'
        response = self.client.open(
            request_uri,
            method='DELETE',
            headers=headers
        )

        assert response.status_code == 204

        # Check with GET method, if user is still there
        # Token is not valid anymore, expecting 401 Unauthorized
        response = self.client.open(
            request_uri,
            method='GET',
            headers=headers
        )
        assert response.status_code == 401
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])
        logging.info("TEST_VALID_TOKEN_DELETE_EXISTING_USER Successful.")

    def test_admin_token_delete_existing_user(self):
        """
        Delete single user with admin token.
        Expecting Succesful No Content 204 response.
        User not found afterwards.
        """

        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + TOKEN_ADMIN
            # NOTE Admin user_id is 1
        }
        request_uri = '/api/users/2/'
        response = self.client.open(
            request_uri,
            method='DELETE',
            headers=headers
        )

        assert response.status_code == 204

        # Check with GET method, if user is still there
        response = self.client.open(
            request_uri,
            method='GET',
            headers=headers
        )
        # Admin has access to see if user is not existing
        assert response.status_code == 404
        self.validateErrorResponseBody(response, request_uri,
                                       ['profile'])
        logging.info("TEST_ADMIN_TOKEN_DELETE_EXISTING_USER Successful.")


if __name__ == '__main__':
    unittest.main()
