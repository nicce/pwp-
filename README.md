[![pipeline status](https://gitlab.com/Nicce/pwp-/badges/master/pipeline.svg)](https://gitlab.com/Nicce/pwp-/commits/master) [![coverage report](https://gitlab.com/Nicce/pwp-/badges/master/coverage.svg)](https://gitlab.com/Nicce/pwp-/commits/master)

# PWP SPRING 2019
# Network Scanner
## Group information
* Student 1. Niklas Saari niklas.saari(at)student.oulu.fi 
* Student 2. Antti Pruikkonen antti.pruikkonen(at)gmail.com
* Student 3. Mikko Uutela mikko.uutela(at)student.oulu.fi
* Student 4. Arttu Vuosku juvuosku(at)student.oulu.fi


## Introduction

Network Scanner is a web service, which enables user to scan speficied targets from the internet to acquire different kind of information. 
Target can be either:

  * Single IP - address (with sub-nets)
  * IP - address range (with sub-nets)
  * Single domain name

The service is based on usage of [NMAP](https://nmap.org/), and it is excepted to be installed on some local network device, preferable on Linux system. It supports multiple users and uses OAUTH 2.0 for user access authorization.

Everything what NMAP is able to do, so are users with this service. (As we talk about scan arguments and so on.)

Scan results are stored into the database, whereof users can retrieve those later on. They are able to add some extra information or comments into results as they will.

Different kind of filter options are offered, when previous scan results have been queried. Detailed API description for service can be found from [Wiki](https://gitlab.com/Nicce/pwp-/wikis/RESTful-API-design)

## Getting started

See [server folder](server) for server specific information.

See [client folder](client) for client specific information.

See [database folder](db) database related testing and model descriptions.

Server and database can be currently run in Docker as well.

## Database documentation
The database related files have their own folder. It can be found from [here.](db)

Model is based on Flask-SqlAlchemy and is running on either SQLite or PostgreSQL. 

It contains model and simple description. More precise documentation can be found from wiki.



## API Specification with API Blueprint and Apiary

Service has been built heavily by using design-first attitude, therefore it is not so traditional test driven way.

To be able make comprehensive design for API, some advanced tools have been used such as [API Blueprint](https://apiblueprint.org/)

We have written documentation by using this standard, but the Open API version have been also provided  by using conversion tool named as [api-spec-converter](https://github.com/LucyBot-Inc/api-spec-converter). Tool did not work flawlessly, so the result required some modifications before it was similar and validated against OpenAPI3 spec.

Both files can be found from [here.](api-specs)
It can be also viewed from the Apiary website itself from [here.](https://networkscanner.docs.apiary.io/#)

Recommended way for inspecting is the website. As long as documents are valid, versions are same in both locations.

## Continous Integration & Deployment

API Blueprint file is uploaded each time into Apiary, when some changes have been made. You will always find the most recent *working* version from there. This validates the schema as well.

### Testing

On each commit into master branch, following pipelines will be run:
  * API Blueprint publishing into Apiary - this will check for major syntax errors.
  * Database unit tests - this will check if our database models are still working in the limits of provided tests. Pytest is used.
  * Unit tests against API will be run. We are using [tox](https://github.com/tox-dev/tox), which is automation tool for testing. It creates virtual environment, and runs different kind of testing tools in paraller if defined so. As tool inside tox, we are using [nose tests](https://nose.readthedocs.io/en/latest/testing.html), which is in practise a collector for basic unittests. This enables easier organization of different kind of tests, and we don't have to create test suites manually.