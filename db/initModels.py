# coding: utf-8
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
# from sqlalchemy.exc import IntegrityError
from sqlalchemy.engine import Engine
from sqlalchemy import event

# Init Flask instance
app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///test.db"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

# Init database instance
db = SQLAlchemy(app)

# Sqlite requires some extra stuff for foreign keys
@event.listens_for(Engine, "connect")
def set_sqlite_pragma(dbapi_connection, connection_record):
    cursor = dbapi_connection.cursor()
    cursor.execute("PRAGMA foreign_keys=ON")
    cursor.close()


class User(db.Model):
    __tablename__ = 'Users'

    ID = db.Column(db.Integer, primary_key=True)
    Username = db.Column(db.String(100), nullable=False, unique=True)
    isAdmin = db.Column(db.Boolean, nullable=False, default=False)
    HashedPassword = db.Column(db.String(255), nullable=False)
    # Authorization type Bearer - OAuth2
    API_Key = db.Column(db.String(255), nullable=False, unique=True)
    FirstName = db.Column(db.String(100))
    Surname = db.Column(db.String(100))
    Email = db.Column(db.String(100), nullable=False, unique=True)
    CreatedAt = db.Column(db.DateTime, nullable=False)
    LastLogin = db.Column(db.DateTime, nullable=True)
    scanresults = db.relationship(
        "ScanResult", back_populates="User", cascade="all, delete-orphan")


class Device(db.Model):
    __tablename__ = 'Device'

    ID = db.Column(db.Integer, primary_key=True)
    MAC_Address_64_bit_ = db.Column('MAC-Address(64-bit)', db.String(23),
                                    unique=True, server_default=None)
    CustomName = db.Column(db.String(100))
    OS = db.Column(db.String(100))
    Comment = db.Column(db.String(200))
    ExtraInfo = db.Column(db.String(1000))
    ModifiedAt = db.Column(db.DateTime, nullable=False)

    targetresult = db.relationship(
        "TargetResult", back_populates="Device", cascade="all, delete-orphan")
    openports = db.relationship(
        "OpenPort", back_populates='openpDevice', cascade="all, delete-orphan")


class ScanResult(db.Model):
    __tablename__ = 'ScanResults'

    User_ID = db.Column(db.ForeignKey('Users.ID'), nullable=False)
    ID = db.Column(db.Integer, primary_key=True)
    UUID = db.Column(db.String(36), nullable=False, unique=True)
    ScanParams = db.Column(db.String(1000), nullable=False)
    ScanInProgress = db.Column(db.Boolean, nullable=False, default=True)
    Comment = db.Column(db.String(200))
    CreatedAt = db.Column(db.DateTime, nullable=False)
    IsTagged = db.Column(db.Boolean, nullable=False,
                        default=False)
    ModifiedAt = db.Column(db.DateTime, nullable=False)

    User = db.relationship('User', back_populates="scanresults")
    targetresults = db.relationship(
        'TargetResult', back_populates='ScanResult', cascade="all, delete-orphan")


class TargetResult(db.Model):
    __tablename__ = 'TargetResult'

    ID = db.Column(db.Integer, primary_key=True)
    Scan_ID = db.Column(db.ForeignKey('ScanResults.ID', ondelete='CASCADE'),
                        nullable=False)
    Device_ID = db.Column(db.ForeignKey('Device.ID', ondelete='CASCADE'),
                          nullable=False)
    TargetDomainName = db.Column(db.String(2000))
    IPv4_address = db.Column(db.String(15))
    IPv6_address = db.Column(db.String(39))
    Reverse_DNS = db.Column(db.String(2000))
    Comment = db.Column(db.String(200))
    IsTagged = db.Column(db.Boolean, nullable=False,
                         server_default=db.text("false"))
    ModifiedAt = db.Column(db.DateTime, nullable=False)

    Device = db.relationship(
        'Device', back_populates='targetresult', uselist=False)
    ScanResult = db.relationship('ScanResult', back_populates='targetresults')
    openports = db.relationship('OpenPort', cascade='all, delete-orphan')
    traceroute = db.relationship(
        'Traceroute', back_populates='TargetResult', uselist=False, cascade='all, delete-orphan')

# This table is required to implement many to many relationship
# Device can have multiple instances of available ports (from different scans)
# We can link device, target scan and available
# ports of that time with this table


class OpenPort(db.Model):
    __tablename__ = 'OpenPorts'

    ID = db.Column(db.Integer, primary_key=True)
    TargetResult_ID = db.Column(db.ForeignKey(
        'TargetResult.ID', ondelete='CASCADE'), nullable=False)
    Device_ID = db.Column(db.ForeignKey(
        'Device.ID', ondelete='CASCADE'), nullable=False)
    CreatedAt = db.Column(db.DateTime, nullable=False)
    ModifiedAt = db.Column(db.DateTime, nullable=False)
    openpDevice = db.relationship('Device', back_populates='openports')
    TargetResult = db.relationship(
        'TargetResult', cascade='all, delete-orphan', single_parent=True)
    ports = db.relationship(
        'SinglePort', back_populates='OpenPorts', cascade='all, delete-orphan')


class SinglePort(db.Model):
    __tablename__ = 'SinglePort'

    ID = db.Column(db.Integer, primary_key=True)
    OpenPorts_ID = db.Column(db.ForeignKey(
        'OpenPorts.ID', ondelete='CASCADE'), nullable=False)
    PortNumber = db.Column(db.Integer, nullable=False)
    Service = db.Column(db.String(50), nullable=False)
    Description = db.Column(db.String(200))
    Comment = db.Column(db.String(200))
    ModifiedAt = db.Column(db.DateTime, nullable=False)

    OpenPorts = db.relationship('OpenPort', back_populates='ports')


class Traceroute(db.Model):
    __tablename__ = 'Traceroute'

    ID = db.Column(db.Integer, primary_key=True)
    Target_ID = db.Column(db.ForeignKey('TargetResult.ID'), nullable=False)
    CreatedAt = db.Column(db.DateTime, nullable=False)
    TargetResult = db.relationship('TargetResult', back_populates='traceroute')
    hops = db.relationship(
        'HOP', back_populates='Traceroute', cascade='all, delete-orphan')


class HOP(db.Model):
    __tablename__ = 'HOP'

    ID = db.Column(db.Integer, primary_key=True)
    Traceroute_ID = db.Column(db.ForeignKey('Traceroute.ID'), nullable=False)
    HOP_Nro = db.Column(db.Integer, nullable=False)
    RTT = db.Column(db.Float, nullable=False)
    DNS = db.Column(db.String(2000))
    IP = db.Column(db.String(39), nullable=False)

    Traceroute = db.relationship('Traceroute', back_populates='hops')
