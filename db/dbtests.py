from initModels import app, db,  User, Device, ScanResult, TargetResult, OpenPort, SinglePort, Traceroute, HOP

from os import close, unlink, getcwd, mkdir
import pytest
import ipaddress
import uuid
import tempfile
from datetime import datetime

from sqlalchemy.engine import Engine
from sqlalchemy import event
from sqlalchemy.exc import IntegrityError, StatementError


@event.listens_for(Engine, "connect")
def set_sqlite_pragma(dbapi_connection, connection_record):
    cursor = dbapi_connection.cursor()
    cursor.execute("PRAGMA foreign_keys=ON")
    cursor.close()


@pytest.fixture
def db_handle():
    _dir = getcwd() + "/temp_db"
    try:
        mkdir(_dir)
    except FileExistsError:
        pass
    db_fd, db_fname = tempfile.mkstemp(suffix=".db", dir=_dir)
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" + db_fname
    app.config["TESTING"] = True

    with app.app_context():
        db.create_all()

    yield db

    close(db_fd)
    #unlink(db_fname)


def _get_user_one():
    return User(
        Username="VeryFirstUser",
        HashedPassword="A3235454ef39U2v32093ve23gfij",
        API_Key="12312AB3info3i",
        FirstName="First",
        Surname="Second",
        Email="very.first@user.com",
        CreatedAt=datetime.now(),
        LastLogin=None
    )


def _get_user_two():
    return User(
        Username="VerySecondUser",
        HashedPassword="A3223435454ef39U2v32093ve23gfij",
        API_Key="12312AB3irewrewnfo3i",
        FirstName="First",
        Surname="Third",
        Email="very.second@user.com",
        CreatedAt=datetime.now(),
        LastLogin=None
    )


def _get_scanresults():
    # targetresults = []
    return ScanResult(
        UUID="1409fj039RFJ0329J032",
        ScanParams="-p- -on 400-5000",
        Comment="This is test comment.",
        CreatedAt=datetime.now(),
        IsTagged=True,
        ModifiedAt=datetime.now()
    )


def _get_targetresult():
    return TargetResult(
        TargetDomainName='testdomain.com',
        IPv4_address='192.168.2.3',
        IPv6_address='2001:0db8:85a3:0000:0000:8a2e:0370:7334',
        Reverse_DNS='agt45-sr32.45.com',
        Comment='Very first target result.',
        IsTagged=False,
        ModifiedAt=datetime.now(),
    )


def _get_device():
    return Device(
        MAC_Address_64_bit_='0221.86ff.feb5.6e10',
        CustomName='iDevice',
        OS='OpenBSD',
        Comment='First device.',
        ExtraInfo='TLS Certificate Valid until 1-1-2020',
        ModifiedAt=datetime.now()
    )


def _get_openports():
    return OpenPort(
        CreatedAt=datetime.now(),
        ModifiedAt=datetime.now()
    )


def _get_singleport_one():
    return SinglePort(
        PortNumber=2345,
        Service='RealOne',
        Description='Used for real services.',
        Comment='Can this be a success?',
        ModifiedAt=datetime.now()
    )


def _get_singleport_two():
    return SinglePort(
        PortNumber=21,
        Service='ftp',
        Description='File transfer protocol',
        Comment='Looks insecure.',
        ModifiedAt=datetime.now()
    )


def _get_traceroute():
    return Traceroute(
        CreatedAt=datetime.now()
    )


def _get_hop_one():
    return HOP(
        HOP_Nro=1,
        RTT=5.5,
        DNS="dmth-s234.at.com",
        IP='123.45.6.7'
    )


def _get_hop_two():
    return HOP(
        HOP_Nro=2,
        RTT=22.3,
        DNS="dmth-s234.at.com",
        IP='223.35.6.7'

    )


def _get_hop_three():
    return HOP(
        HOP_Nro=3,
        RTT=13.2,
        DNS="dmtsdfh.dofih24.fi",
        IP='93.25.6.3'

    )

def create_scan():
    _uuid=uuid.uuid4()
    _comment = "Comment " + str(_uuid)
    _scanparams = "ScanParams " + str(_uuid)
    _createdAt = datetime.utcnow()
    _scan = ScanResult(
        UUID=str(_uuid), 
        Comment=_comment, 
        CreatedAt=_createdAt, 
        ModifiedAt=_createdAt,
        IsTagged = False,
        ScanParams=_scanparams)
    return _scan

def create_device(_mac64bit):
    _comment = "Comment " + str(_mac64bit)
    _extraInfo = "ExtraInfo " + str(_mac64bit)
    _customName = "CustomName " + str(_mac64bit)
    _os = "OS " + str(_mac64bit) 
    _modified = datetime.utcnow()
    
    _device = Device(
        MAC_Address_64_bit_ = str(_mac64bit),
        OS = _os,
        Comment = _comment,
        ExtraInfo = _extraInfo,
        ModifiedAt = _modified
    )
    return _device
        

def create_target(_ipAddress):
    _target = TargetResult(
        TargetDomainName = "DomainName " + str(_ipAddress),
        IsTagged = False,
        Reverse_DNS = "ReverseDNS " + str(_ipAddress),
        IPv6_address = str(_ipAddress),
        IPv4_address = str(_ipAddress), 
        Comment = "Comment " + str(_ipAddress), 
        ModifiedAt = datetime.utcnow()
    )
    return _target
    

def create_port(_port=80 ):
    port = SinglePort(
        PortNumber = _port, 
        Service = "Service " + str(_port), 
        Description = "Description " + str(_port), 
        Comment = "Comment " + str(_port), 
        ModifiedAt = datetime.utcnow()
    )
    return port
    


def test_create_user(db_handle):

    db_handle.session.add(_get_user_one())
    db_handle.session.commit()
    assert User.query.count() == 1
    assert User.query.first().Username == 'VeryFirstUser'


def test_create_all_instances(db_handle):

    # Create user, and structure for scanresults, device and targetresult

    user = _get_user_one()
    scanresults = _get_scanresults()
    device = _get_device()
    targetresult1 = _get_targetresult()

    # Let create single ports for open ports
    openports = _get_openports()
    port1 = _get_singleport_one()
    port2 = _get_singleport_two()
    openports.ports.append(port1)
    openports.ports.append(port2)

    # Add openports to session
    db_handle.session.add(port1)
    db_handle.session.add(port2)
    db_handle.session.add(openports)
    # Reference to correct targetresult as well
    openports.TargetResult = targetresult1

    # Traceroute
    traceroute = _get_traceroute()
    hop1 = _get_hop_one()
    hop2 = _get_hop_two()
    hop3 = _get_hop_three()
    db_handle.session.add(traceroute)
    db_handle.session.add(hop1)
    db_handle.session.add(hop2)
    db_handle.session.add(hop3)
    traceroute.hops.append(hop1)
    traceroute.hops.append(hop2)
    traceroute.hops.append(hop3)
    targetresult1.traceroute = traceroute

    # Link open ports to device
    device.openports.append(openports)
    db_handle.session.add(device)
    db_handle.session.add(targetresult1)
    targetresult1.Device = device

    # Link target results for all results
    scanresults.targetresults.append(targetresult1)

    db_handle.session.add(scanresults)
    user.scanresults.append(scanresults)
    db_handle.session.add(user)

    db_handle.session.commit()

    # Check if every object exists
    assert User.query.count() == 1
    assert ScanResult.query.count() == 1
    assert TargetResult.query.count() == 1
    assert Device.query.count() == 1
    assert OpenPort.query.count() == 1
    assert SinglePort.query.count() == 2
    assert Traceroute.query.count() == 1
    assert HOP.query.count() == 3

    # Generate objects from database
    db_User = User.query.first()
    db_ScanResult = ScanResult.query.first()
    db_TargetResult = TargetResult.query.first()
    db_Device = Device.query.first()
    db_OpenPorts = OpenPort.query.first()
    db_SinglePort = SinglePort.query.first()
    db_Traceroute = Traceroute.query.first()
    db_HOP1 = HOP.query.first()

    # Check relationships
    assert db_ScanResult in db_User.scanresults
    assert db_TargetResult.Device == db_Device
    assert db_TargetResult in db_ScanResult.targetresults
    assert db_OpenPorts in db_Device.openports
    assert db_SinglePort in db_OpenPorts.ports
    assert db_HOP1 in db_Traceroute.hops
    assert db_Traceroute == db_TargetResult.traceroute

    # Check if some objects are equal
    assert db_User == user
    assert port1 in db_Device.openports[0].ports
    assert port2 in db_Device.openports[0].ports


# User must have unique username, email and api-key
def test_create_uniq_user(db_handle):
    user1 = _get_user_one()
    user2 = _get_user_two()

    # Test with same username
    user2.Username = 'VeryFirstUser'
    db_handle.session.add(user1)
    db_handle.session.add(user2)
    with pytest.raises(StatementError):
        db_handle.session.commit()

    db_handle.session.rollback()

    # Test with same email

    user2 = _get_user_two()
    user2.Email = 'very.first@user.com'
    db_handle.session.add(user1)
    db_handle.session.add(user2)
    with pytest.raises(StatementError):
        db_handle.session.commit()

    db_handle.session.rollback()

    # Test with same API key

    user2 = _get_user_two()
    user2.API_Key = '12312AB3info3i'
    db_handle.session.add(user1)
    db_handle.session.add(user2)
    with pytest.raises(StatementError):
        db_handle.session.commit()


# If we delete single target result, eveyrhing
# related to result except device should be deleted
def test_targetresult_cascadeDelete(db_handle):
     # Create user, and structure for scanresults, device and targetresult

    user = _get_user_one()
    scanresults = _get_scanresults()
    device = _get_device()
    targetresult1 = _get_targetresult()

    # Let create single ports for open ports
    openports = _get_openports()
    port1 = _get_singleport_one()
    port2 = _get_singleport_two()
    openports.ports.append(port1)
    openports.ports.append(port2)

    # Add openports to session
    db_handle.session.add(port1)
    db_handle.session.add(port2)
    db_handle.session.add(openports)
    # Reference to correct targetresult as well
    openports.TargetResult = targetresult1

    # Traceroute
    traceroute = _get_traceroute()
    hop1 = _get_hop_one()
    hop2 = _get_hop_two()
    hop3 = _get_hop_three()
    db_handle.session.add(traceroute)
    db_handle.session.add(hop1)
    db_handle.session.add(hop2)
    db_handle.session.add(hop3)
    traceroute.hops.append(hop1)
    traceroute.hops.append(hop2)
    traceroute.hops.append(hop3)
    targetresult1.traceroute = traceroute

    # Link open ports to device
    device.openports.append(openports)
    db_handle.session.add(device)
    db_handle.session.add(targetresult1)
    targetresult1.Device = device

    # Link target results for all results
    scanresults.targetresults.append(targetresult1)

    db_handle.session.add(scanresults)
    user.scanresults.append(scanresults)
    db_handle.session.add(user)

    db_handle.session.commit()
    db_handle.session.delete(targetresult1)
    db_handle.session.commit()

    # If we delete single target result from all results,
    # everything related to it should be deleted, except device

    assert Device.query.count() == 1
    assert OpenPort.query.count() == 0
    assert SinglePort.query.count() == 0
    assert Traceroute.query.count() == 0
    assert HOP.query.count() == 0


# Target result must have only one traceroute
def test_traceroute_targetresult_relation(db_handle):
    user = _get_user_one()
    scanresults = _get_scanresults()
    device = _get_device()
    targetresult1 = _get_targetresult()

    # Let create single ports for open ports
    openports = _get_openports()
    port1 = _get_singleport_one()
    port2 = _get_singleport_two()
    openports.ports.append(port1)
    openports.ports.append(port2)

    # Add openports to session
    db_handle.session.add(port1)
    db_handle.session.add(port2)
    db_handle.session.add(openports)
    # Reference to correct targetresult as well
    openports.TargetResult = targetresult1

    # Traceroute
    traceroute = _get_traceroute()
    hop1 = _get_hop_one()
    hop2 = _get_hop_two()
    hop3 = _get_hop_three()
    db_handle.session.add(traceroute)
    db_handle.session.add(hop1)
    db_handle.session.add(hop2)
    db_handle.session.add(hop3)
    traceroute.hops.append(hop1)
    traceroute.hops.append(hop2)
    traceroute.hops.append(hop3)
    # targetresult1.traceroute = traceroute

    with pytest.raises(AttributeError):
        targetresult1.traceroute.append(traceroute)

    
def test_create_multiuser_db_with_multiple_scans(db_handle):
    """
    Tests that database creation is successfull with 2 users and multiple targets / devices / ports.
    """

    baseAddress="192.168.0.0"
    numberOfScans = 3
    numberOfTargets=10
    basePort=1
    numberOfPorts=10
    ipAddress = ipaddress.ip_address(baseAddress)
    mac_index = 0

    ### CREATE USER WITH MULTIPLE SCANS
    user = User(
        Username="VeryFirstUser",
        HashedPassword="A3235454ef39U2v32093ve23gfij",
        API_Key="12312AB3info3i",
        FirstName="First",
        Surname="Second",
        Email="very.first@user.com",
        CreatedAt=datetime.now(),
        LastLogin=None
        )


    for k in range(numberOfScans):
        scan = create_scan()
        db_handle.session.add(scan)
        user.scanresults.append(scan)
        for i in range(numberOfTargets):
            target = create_target(ipAddress)
            scan.targetresults.append(target)

            mac64 = "0123.45ff.fe67." + "{0:0=4d}".format(mac_index)
            mac_index += 1
            device = create_device(mac64)
            target.Device = device

            openports = OpenPort(CreatedAt=datetime.utcnow(), ModifiedAt=datetime.utcnow())
            target.openports.append(openports)
            device.openports.append(openports)

            for j in range(numberOfPorts):
                port = create_port(j)
                openports.ports.append(port)

            ipAddress += 1
    
    db_handle.session.commit()

    ### CREATE ANOTHER USER WITH MULTIPLE SCANS
    user = User(
        Username="SecondUser",
        HashedPassword="A3235454ef39U2v32093ve23gfij",
        API_Key="qwerty",
        FirstName="John",
        Surname="Doe",
        Email="john.doe@user.com",
        CreatedAt=datetime.now(),
        LastLogin=None
        )

    for k in range(numberOfScans):
        scan = create_scan()
        db_handle.session.add(scan)
        user.scanresults.append(scan)
        for i in range(numberOfTargets):
            target = create_target(ipAddress)
            scan.targetresults.append(target)

            mac64 = "0123.45ff.fe67." + "{0:0=4d}".format(mac_index)
            mac_index += 1
            device = create_device(mac64)
            target.Device = device

            openports = OpenPort(CreatedAt=datetime.utcnow(), ModifiedAt=datetime.utcnow())
            target.openports.append(openports)
            device.openports.append(openports)

            for j in range(numberOfPorts):
                port = create_port(j)
                openports.ports.append(port)

            ipAddress += 1
    
    db_handle.session.commit()

    assert User.query.count() == 2
    assert ScanResult.query.count() == 2 * numberOfScans
    assert TargetResult.query.count() == 2 * numberOfScans * numberOfTargets
    assert Device.query.count() == 2 * numberOfScans * numberOfTargets
    assert OpenPort.query.count() == 2 * numberOfScans * numberOfTargets
    assert SinglePort.query.count() == 2 * numberOfScans * numberOfTargets * numberOfPorts
    
def test_user_deletion_from_multiuser_db_with_multiple_scans(db_handle):
    """
    Tests that deletion of user from database performs as expected.  All user related scans and items are removed (except devices are not removed)
    """

    baseAddress="192.168.0.0"
    numberOfScans = 3
    numberOfTargets=10
    basePort=1
    numberOfPorts=10
    ipAddress = ipaddress.ip_address(baseAddress)
    mac_index = 0

    ### CREATE USER WITH MULTIPLE SCANS
    user = User(
        Username="VeryFirstUser",
        HashedPassword="A3235454ef39U2v32093ve23gfij",
        API_Key="12312AB3info3i",
        FirstName="First",
        Surname="Second",
        Email="very.first@user.com",
        CreatedAt=datetime.now(),
        LastLogin=None
        )


    for k in range(numberOfScans):
        scan = create_scan()
        db_handle.session.add(scan)
        user.scanresults.append(scan)
        for i in range(numberOfTargets):
            target = create_target(ipAddress)
            scan.targetresults.append(target)

            mac64 = "0123.45ff.fe67." + "{0:0=4d}".format(mac_index)
            mac_index += 1
            device = create_device(mac64)
            target.Device = device

            openports = OpenPort(CreatedAt=datetime.utcnow(), ModifiedAt=datetime.utcnow())
            target.openports.append(openports)
            device.openports.append(openports)

            for j in range(numberOfPorts):
                port = create_port(j)
                openports.ports.append(port)

            ipAddress += 1
    
    db_handle.session.commit()

    ### CREATE ANOTHER USER WITH MULTIPLE SCANS
    user = User(
        Username="SecondUser",
        HashedPassword="A3235454ef39U2v32093ve23gfij",
        API_Key="qwerty",
        FirstName="John",
        Surname="Doe",
        Email="john.doe@user.com",
        CreatedAt=datetime.now(),
        LastLogin=None
        )

    for k in range(numberOfScans):
        scan = create_scan()
        db_handle.session.add(scan)
        user.scanresults.append(scan)
        for i in range(numberOfTargets):
            target = create_target(ipAddress)
            scan.targetresults.append(target)

            mac64 = "0123.45ff.fe67." + "{0:0=4d}".format(mac_index)
            mac_index += 1
            device = create_device(mac64)
            target.Device = device

            openports = OpenPort(CreatedAt=datetime.utcnow(), ModifiedAt=datetime.utcnow())
            target.openports.append(openports)
            device.openports.append(openports)

            for j in range(numberOfPorts):
                port = create_port(j)
                openports.ports.append(port)

            ipAddress += 1
    
    db_handle.session.commit()
    
    user = User.query.first()
    db_handle.session.delete(user)
    
    ## After deletion Devices count should remain same. Other user items will be deleted. Device count should remain the same.
    assert User.query.count() == 1
    assert ScanResult.query.count() == 1 * numberOfScans
    assert TargetResult.query.count() == 1 * numberOfScans * numberOfTargets
    assert Device.query.count() == 2 * numberOfScans * numberOfTargets
    assert OpenPort.query.count() == 1 * numberOfScans * numberOfTargets
    assert SinglePort.query.count() == 1 * numberOfScans * numberOfTargets * numberOfPorts
    
def test_single_scan_deletion_from_multiuser_db_with_multiple_scans(db_handle):
    """
    Tests that after deletion of single ScanResult item other related items are also deleted.
    How ever Device count should remain the same
    """

    baseAddress="192.168.0.0"
    numberOfScans = 3
    numberOfTargets=10
    basePort=1
    numberOfPorts=10
    ipAddress = ipaddress.ip_address(baseAddress)
    mac_index = 0

    ### CREATE USER WITH MULTIPLE SCANS
    user = User(
        Username="VeryFirstUser",
        HashedPassword="A3235454ef39U2v32093ve23gfij",
        API_Key="12312AB3info3i",
        FirstName="First",
        Surname="Second",
        Email="very.first@user.com",
        CreatedAt=datetime.now(),
        LastLogin=None
        )


    for k in range(numberOfScans):
        scan = create_scan()
        db_handle.session.add(scan)
        user.scanresults.append(scan)
        for i in range(numberOfTargets):
            target = create_target(ipAddress)
            scan.targetresults.append(target)

            mac64 = "0123.45ff.fe67." + "{0:0=4d}".format(mac_index)
            mac_index += 1
            device = create_device(mac64)
            target.Device = device

            openports = OpenPort(CreatedAt=datetime.utcnow(), ModifiedAt=datetime.utcnow())
            target.openports.append(openports)
            device.openports.append(openports)

            for j in range(numberOfPorts):
                port = create_port(j)
                openports.ports.append(port)

            ipAddress += 1
    
    db_handle.session.commit()

    ### CREATE ANOTHER USER WITH MULTIPLE SCANS
    user = User(
        Username="SecondUser",
        HashedPassword="A3235454ef39U2v32093ve23gfij",
        API_Key="qwerty",
        FirstName="John",
        Surname="Doe",
        Email="john.doe@user.com",
        CreatedAt=datetime.now(),
        LastLogin=None
        )

    for k in range(numberOfScans):
        scan = create_scan()
        db_handle.session.add(scan)
        user.scanresults.append(scan)
        for i in range(numberOfTargets):
            target = create_target(ipAddress)
            scan.targetresults.append(target)

            mac64 = "0123.45ff.fe67." + "{0:0=4d}".format(mac_index)
            mac_index += 1
            device = create_device(mac64)
            target.Device = device

            openports = OpenPort(CreatedAt=datetime.utcnow(), ModifiedAt=datetime.utcnow())
            target.openports.append(openports)
            device.openports.append(openports)

            for j in range(numberOfPorts):
                port = create_port(j)
                openports.ports.append(port)

            ipAddress += 1
    
    db_handle.session.commit()
    
    scan = ScanResult.query.first()
    db_handle.session.delete(scan)
    
    ## After deletion Devices count should remain same. Other scan related items will be deleted. Device count should remain the same
    assert User.query.count() == 2
    assert ScanResult.query.count() == 2 * numberOfScans - 1
    assert TargetResult.query.count() == 2 * numberOfScans * numberOfTargets - numberOfTargets
    assert Device.query.count() == 2 * numberOfScans * numberOfTargets
    assert OpenPort.query.count() == 2 * numberOfScans * numberOfTargets - numberOfTargets
    assert SinglePort.query.count() == 2 * numberOfScans * numberOfTargets * numberOfPorts - (numberOfTargets * numberOfPorts)
    
def test_single_device_deletion_from_multiuser_db_with_multiple_scans(db_handle):
    """
    Tests that after deletion of single Device item other related items are also deleted.
    """

    baseAddress="192.168.0.0"
    numberOfScans = 3
    numberOfTargets=10
    basePort=1
    numberOfPorts=10
    ipAddress = ipaddress.ip_address(baseAddress)
    mac_index = 0

    ### CREATE USER WITH MULTIPLE SCANS
    user = User(
        Username="VeryFirstUser",
        HashedPassword="A3235454ef39U2v32093ve23gfij",
        API_Key="12312AB3info3i",
        FirstName="First",
        Surname="Second",
        Email="very.first@user.com",
        CreatedAt=datetime.now(),
        LastLogin=None
        )


    for k in range(numberOfScans):
        scan = create_scan()
        db_handle.session.add(scan)
        user.scanresults.append(scan)
        for i in range(numberOfTargets):
            target = create_target(ipAddress)
            scan.targetresults.append(target)

            mac64 = "0123.45ff.fe67." + "{0:0=4d}".format(mac_index)
            mac_index += 1
            device = create_device(mac64)
            target.Device = device

            openports = OpenPort(CreatedAt=datetime.utcnow(), ModifiedAt=datetime.utcnow())
            target.openports.append(openports)
            device.openports.append(openports)

            for j in range(numberOfPorts):
                port = create_port(j)
                openports.ports.append(port)

            ipAddress += 1
    
    db_handle.session.commit()

    ### CREATE ANOTHER USER WITH MULTIPLE SCANS
    user = User(
        Username="SecondUser",
        HashedPassword="A3235454ef39U2v32093ve23gfij",
        API_Key="qwerty",
        FirstName="John",
        Surname="Doe",
        Email="john.doe@user.com",
        CreatedAt=datetime.now(),
        LastLogin=None
        )

    for k in range(numberOfScans):
        scan = create_scan()
        db_handle.session.add(scan)
        user.scanresults.append(scan)
        for i in range(numberOfTargets):
            target = create_target(ipAddress)
            scan.targetresults.append(target)

            mac64 = "0123.45ff.fe67." + "{0:0=4d}".format(mac_index)
            mac_index += 1
            device = create_device(mac64)
            target.Device = device

            openports = OpenPort(CreatedAt=datetime.utcnow(), ModifiedAt=datetime.utcnow())
            target.openports.append(openports)
            device.openports.append(openports)

            for j in range(numberOfPorts):
                port = create_port(j)
                openports.ports.append(port)

            ipAddress += 1
    
    db_handle.session.commit()
    
    device = Device.query.first()
    db_handle.session.delete(device)
    
    ## After deletion Devices count decrease by one. Other device related items will be deleted
    assert User.query.count() == 2
    assert ScanResult.query.count() == 2 * numberOfScans
    assert TargetResult.query.count() == 2 * numberOfScans * numberOfTargets - 1
    assert Device.query.count() == 2 * numberOfScans * numberOfTargets - 1
    assert OpenPort.query.count() == 2 * numberOfScans * numberOfTargets - 1
    assert SinglePort.query.count() == 2 * numberOfScans * numberOfTargets * numberOfPorts - numberOfPorts
    
def test_single_openport_deletion_from_multiuser_db_with_multiple_scans(db_handle):
    """
    Tests that after deletion of single OpenPort item other related items are also deleted.
    How ever Device count should remain the same
    """

    baseAddress="192.168.0.0"
    numberOfScans = 3
    numberOfTargets=10
    basePort=1
    numberOfPorts=10
    ipAddress = ipaddress.ip_address(baseAddress)
    mac_index = 0

    ### CREATE USER WITH MULTIPLE SCANS
    user = User(
        Username="VeryFirstUser",
        HashedPassword="A3235454ef39U2v32093ve23gfij",
        API_Key="12312AB3info3i",
        FirstName="First",
        Surname="Second",
        Email="very.first@user.com",
        CreatedAt=datetime.now(),
        LastLogin=None
        )


    for k in range(numberOfScans):
        scan = create_scan()
        db_handle.session.add(scan)
        user.scanresults.append(scan)
        for i in range(numberOfTargets):
            target = create_target(ipAddress)
            scan.targetresults.append(target)

            mac64 = "0123.45ff.fe67." + "{0:0=4d}".format(mac_index)
            mac_index += 1
            device = create_device(mac64)
            target.Device = device

            openports = OpenPort(CreatedAt=datetime.utcnow(), ModifiedAt=datetime.utcnow())
            target.openports.append(openports)
            device.openports.append(openports)

            for j in range(numberOfPorts):
                port = create_port(j)
                openports.ports.append(port)

            ipAddress += 1
    
    db_handle.session.commit()

    ### CREATE ANOTHER USER WITH MULTIPLE SCANS
    user = User(
        Username="SecondUser",
        HashedPassword="A3235454ef39U2v32093ve23gfij",
        API_Key="qwerty",
        FirstName="John",
        Surname="Doe",
        Email="john.doe@user.com",
        CreatedAt=datetime.now(),
        LastLogin=None
        )

    for k in range(numberOfScans):
        scan = create_scan()
        db_handle.session.add(scan)
        user.scanresults.append(scan)
        for i in range(numberOfTargets):
            target = create_target(ipAddress)
            scan.targetresults.append(target)

            mac64 = "0123.45ff.fe67." + "{0:0=4d}".format(mac_index)
            mac_index += 1
            device = create_device(mac64)
            target.Device = device

            openports = OpenPort(CreatedAt=datetime.utcnow(), ModifiedAt=datetime.utcnow())
            target.openports.append(openports)
            device.openports.append(openports)

            for j in range(numberOfPorts):
                port = create_port(j)
                openports.ports.append(port)

            ipAddress += 1
    
    db_handle.session.commit()
    
    openport = OpenPort.query.first()
    db_handle.session.delete(openport)
    
    ## After deletion Openport count should decrease by one. Other openport related items will be deleted. Device count should remain the same.
    assert User.query.count() == 2
    assert ScanResult.query.count() == 2 * numberOfScans
    assert TargetResult.query.count() == 2 * numberOfScans * numberOfTargets - 1
    assert Device.query.count() == 2 * numberOfScans * numberOfTargets
    assert OpenPort.query.count() == 2 * numberOfScans * numberOfTargets - 1
    assert SinglePort.query.count() == 2 * numberOfScans * numberOfTargets * numberOfPorts - numberOfPorts
    
def test_single_singleport_deletion_from_multiuser_db_with_multiple_scans(db_handle):
    """
    Tests that after deletion of single SinglePort item other related items are also deleted.
    How ever Device count should remain the same
    """

    baseAddress="192.168.0.0"
    numberOfScans = 3
    numberOfTargets=10
    basePort=1
    numberOfPorts=10
    ipAddress = ipaddress.ip_address(baseAddress)
    mac_index = 0

    ### CREATE USER WITH MULTIPLE SCANS
    user = User(
        Username="VeryFirstUser",
        HashedPassword="A3235454ef39U2v32093ve23gfij",
        API_Key="12312AB3info3i",
        FirstName="First",
        Surname="Second",
        Email="very.first@user.com",
        CreatedAt=datetime.now(),
        LastLogin=None
        )


    for k in range(numberOfScans):
        scan = create_scan()
        db_handle.session.add(scan)
        user.scanresults.append(scan)
        for i in range(numberOfTargets):
            target = create_target(ipAddress)
            scan.targetresults.append(target)

            mac64 = "0123.45ff.fe67." + "{0:0=4d}".format(mac_index)
            mac_index += 1
            device = create_device(mac64)
            target.Device = device

            openports = OpenPort(CreatedAt=datetime.utcnow(), ModifiedAt=datetime.utcnow())
            target.openports.append(openports)
            device.openports.append(openports)

            for j in range(numberOfPorts):
                port = create_port(j)
                openports.ports.append(port)

            ipAddress += 1
    
    db_handle.session.commit()

    ### CREATE ANOTHER USER WITH MULTIPLE SCANS
    user = User(
        Username="SecondUser",
        HashedPassword="A3235454ef39U2v32093ve23gfij",
        API_Key="qwerty",
        FirstName="John",
        Surname="Doe",
        Email="john.doe@user.com",
        CreatedAt=datetime.now(),
        LastLogin=None
        )

    for k in range(numberOfScans):
        scan = create_scan()
        db_handle.session.add(scan)
        user.scanresults.append(scan)
        for i in range(numberOfTargets):
            target = create_target(ipAddress)
            scan.targetresults.append(target)

            mac64 = "0123.45ff.fe67." + "{0:0=4d}".format(mac_index)
            mac_index += 1
            device = create_device(mac64)
            target.Device = device

            openports = OpenPort(CreatedAt=datetime.utcnow(), ModifiedAt=datetime.utcnow())
            target.openports.append(openports)
            device.openports.append(openports)

            for j in range(numberOfPorts):
                port = create_port(j)
                openports.ports.append(port)

            ipAddress += 1
    
    db_handle.session.commit()
    
    singleport = SinglePort.query.first()
    db_handle.session.delete(singleport)
    
    ## After deletion SinglePort count should decrease by one. Other items will remain the same. Device count should remain the same
    assert User.query.count() == 2
    assert ScanResult.query.count() == 2 * numberOfScans
    assert TargetResult.query.count() == 2 * numberOfScans * numberOfTargets
    assert Device.query.count() == 2 * numberOfScans * numberOfTargets
    assert OpenPort.query.count() == 2 * numberOfScans * numberOfTargets
    assert SinglePort.query.count() == 2 * numberOfScans * numberOfTargets * numberOfPorts - 1
    
