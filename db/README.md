# Database 
*Database related code and documentation can be found from here*
## SQLAlchemy Models
[initModels.py](initModels.py) contains object relation models for database. It also creates instance of Flask application.



To run tests, it is recommended to install virtual environment for required libraries:

```shell
python3 -m venv venv
source venv/bin/activate # NOTE: command for Linux/Mac
pip3 install -r requirements.txt # Install requirements
```


And, simply run in this folder by: `pytest dbtests.py`

It will leave databases generated during tests in `temp_db` folder.
We are using SQLite3.


If you wish to run with coverage (Which might not have purpose, when just testing model relations), it can be done with command:

```shell
pytest dbtests.py --cov=$(pwd) dbtests.py initModels.py
``` 
Note: Linux/Mac only.
$(pwd) must be replaced with current directory.



Database schema should look something like this:

![](relations.png)