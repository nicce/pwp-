import json
# import os
import re
import requests
import sys
from requests.exceptions import ConnectionError
# import time
import logging

LOGGING_LEVEL = logging.INFO

if len(sys.argv) > 1:
    if sys.argv[1] == '--debug':
        print("\nDebugging enabled.\n")
        LOGGING_LEVEL = logging.DEBUG
    elif sys.argv[1] == '--help':
        print("To start program, run without arguments."
              "Give '--debug' as an argument to enable debug mode. "
              "No other arguments accepted.")
    else:
        print("Give '--debug' as an argument to enable debug mode. "
              "No other arguments accepted.")
        exit()


logger = logging.getLogger(__name__)
logger.setLevel(LOGGING_LEVEL)
# Formatter of log
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler = logging.StreamHandler()
handler.setLevel(LOGGING_LEVEL)
handler.setFormatter(formatter)
logger.addHandler(handler)

API_URL = "http://localhost:8080"
API_ROOT = "/api/"


class APIError(Exception):
    """
    Exception class used when the API responds with an error code. Gives
    information about the error in the console.    

    COPIED FROM PWP COURSE EXERCISE 4 SAMPLE SCRIPTS

    """

    def __init__(self, code, error):
        """
        Initializes the exception with *code* as the status code from the
        response and *error* as the response body.
        """

        self.error = json.loads(error)
        self.code = code

    def __str__(self):
        """
        Returns all details from the error response sent by the API formatted into
        a string.
        """

        return "Error {code} while accessing {uri}: {msg}\nDetails:\n{msgs}"\
            .format(
                code=self.code,
                uri=self.error["resource_url"],
                msg=self.error["@error"]["@message"],
                msgs="\n".join(self.error["@error"]["@messages"])
            )


def submit_data(s, ctrl, data):
    """
    submit_data(s, ctrl, data) -> requests.Response

    Sends *data* provided as a JSON compatible Python data structure to the API
    using URI and HTTP method defined in the *ctrl* dictionary (a Mason @control).
    The data is serialized by this function and sent to the API. Returns the 
    response object provided by requests.

    COPIED FROM PWP COURSE EXERCISE 4 SAMPLE SCRIPTS
    """

    resp = s.request(
        ctrl["method"],
        API_URL + ctrl["href"],
        data=json.dumps(data),
        headers={"Content-type": "application/json"},
        allow_redirects=False
    )
    logger.debug("Response code after submitting data in submit_data: {}\n"
                 "And content:{}\n".format(resp.status_code, resp.content))
    return resp


def convert_value(value, schema_props):
    """
    convert_value(value, schema_props) -> value

    Converts a *value* to the type and format defined in the corresponding set
    of JSON schema properties. Returns the converted value, or the value as-is
    if no conversion was necessary.

    COPIED FROM PWP COURSE EXERCISE 4 SAMPLE SCRIPTS
    """

    if schema_props["type"] == "integer":
        value = int(value)
    elif schema_props["type"] == "number":
        value = float(value)
    elif schema_props["type"] == "boolean":
        if value == 'True' or value == 'true':
            value = True
        elif value == 'False' or value == 'false':
            value = False
        else:
            raise TypeError("Input value is not boolean. "
                            "(Should be written as: 'true' or 'false')")
    else:
        value = str(value)
    return value


def prompt_from_schema(s, ctrl):
    """
    Method for generating POST/PUT requests based on JSON schema.
    Asking only required attributes from the user.

     COPIED FROM PWP COURSE EXERCISE 4 SAMPLE SCRIPTS
   """
    body = {}
    # checking if schema is found
    if 'schema' in ctrl:
        #print("Schema body found from the response.")
        schema = ctrl["schema"]
    else:  # Schema not found
        print("Schema not found, using reference uri.")
        schemaUrl = ctrl.get("schemaUrl")
        if schemaUrl is None:
            logger.debug("No schema found from the body, nor proper schemaURL")
            return None
        resp = s.get(schemaUrl)
        schema = resp.json()
        print("SCHEMA = {}".format(schema))

    for field in schema.get("required"):
        properties = schema["properties"][field]
        # print("Properties of the following required attribute = {}"
        #       .format(properties))
        value = input("Provide value for '{}' : \nDescription: {}\nType: {}\nEnter your response: "
                      .format(field, schema.get(
                          "properties").get(field).get("description"),
                          schema.get("properties").get(field).get("type")))
        body[field] = convert_value(value, properties)

    resp = submit_data(s, ctrl, body)
    return resp


def createNewUser(s, ctrl):
    """
    Method for creating new user.
    On successful creation, it redirects on newly created user

    :param s: request session
    :param ctrl: Add new user control
    """
    print("New user creation was selected.\nPlease, provide"
          " required input fields.\n")
    resp = prompt_from_schema(s, ctrl)
    if resp.status_code == 201:
        print("User creation was succesful!")
        print("Here is your OAuth2 credential information:\n")
        for key in resp.json():
            print("{}: {}".format(key, resp.json().get(key)))

        print()
        s.headers.update({"Authorization": "Bearer {}"
                          .format(resp.json()
                                  .get("access_token"))})
        logger.debug("Location of newly created user: {}".format(
            resp.headers.get("location")))
        resp = s.get(resp.headers.get("location"))
        if resp.status_code == 200:
            print("Redirecting into the newly created user."
                  " OAuth token taken on use.")
        else:
            raise APIError(resp.status_code, resp.content)
    return resp


def editUser(s, ctrl):
    """
    Method for editing existing user.

    :param s: request session
    :param ctrl: edit user control
    """
    print("User editing was selected.\nPlease, provide"
          " required input fields.\n")
    resp = prompt_from_schema(s, ctrl)
    if resp.status_code == 204:
        print("User edited successfully!")

    return resp


def updateBearerToken(s, token=None):
    """
    Updates bearer token based on iput value

    :param s: Request instance as parameter,
    where into header will be updated.
    :param token: Token, which contains type as well:
    e.g. Bearer 1234556
    """
    if not token:  # ask token from user
        value = input("Enter token value: ")
        token = "Bearer {}".format(value)
        s.headers.update({"Authorization": token})
    else:  # update token from function call parameter
        token = "{}".format(token)
        s.headers.update({"Authorization": token})


def createMenuFromControls(s, ctrl, previousHref, currentHref, items):
    """
    Creates and prints menu for user.
    Returns user selected URI and link relation name from @controls,
    dictionary {link-relation: uri}
    if user selects to go back to previous URI or update token 
    "link-relation" will be "other"
    """
    print()
    print("Your current location is '{}'".format(currentHref))
    # if the items has "PROPERTY" key then it is not an
    # link but a set of properties to be printed
    if items and ('PROPERTY' in items):
        print("Following properties was found from this resource:")
        print()
        print("{}".format(items["PROPERTY"]))
        print()
    print("Here are the links you can choose.")
    print("Select what you want to do by entering correct number")
    apiUris = []
    # First we add the fixed links to the Menu
    apiUris.append({"other": API_ROOT})
    apiUris.append({"other": previousHref})
    apiUris.append({"other": currentHref})
    numberOfFixedLinks = len(apiUris)
    print("0: Fixed link to API root")
    print("1: Go to previous URI")
    print("2: Update authentication token (hint 'qwerty' or '12312AB3info3i')")

    allowedControls = [  # we only show these links in the menu because others are not implemented
        "netscan:auth",
        "netscan:login-user",
        "netscan:logout-user",
        "netscan:users-all",
        "netscan:add-user",
        "netscan:edit-user",
        "netscan:delete-user",
        "up"]

    skipped = 0
    lastIndex = 0
    # iterating the @controls and building the menu
    for index, (key, value) in enumerate(ctrl.items()):
        if key in allowedControls:
            print("{}: {}".format(index - skipped +
                                  numberOfFixedLinks, ctrl[key]["title"]))
            item = {key: ctrl[key]["href"]}
            apiUris.append(item)
            lastIndex = index - skipped + numberOfFixedLinks
        else:
            skipped = skipped + 1

    # Iterating the "items" and building the items menu
    if items and 'PROPERTY' not in items:  # if the items has "PROPERTY" key then it is not an link
        for key in items:
            print("{}: {}".format(lastIndex+1, key))
            lastIndex = lastIndex + 1
            apiUris.append({"item": items[key]})

    while True:
        try:
            value = int(input("Enter number: "))
        except ValueError:
            print("That was not integer. Please try again.")
        else:
            break
    logger.debug("Registered user input is {}".format(value))
    print()
    if value == numberOfFixedLinks - 1:
        updateBearerToken(s)
    return apiUris[value]


def getResource(s, userSelection):
    """
    Method for general GET request. 

    :param s: request object
    :param userSelection: dictionary item with format {link-relation: uri} 
    :return: response object
    """
    resourceUrl = list(userSelection.values())
    resp = s.get(API_URL + resourceUrl[0])
    return resp


def loginUser(s, linkRel, ctrls):
    """
    Method for logging user in.

    :param s: request object
    :param linkRel: Link-relation, in this case login-user
    :param ctrls: All controls in current location
    :return: response object
    """
    print(
        "User Login. Please, provide correct credentials.")
    logger.debug("Controls before trying to log in: {}\n".format(
        ctrls))

    loginCtrl = ctrls[linkRel]
    resp = prompt_from_schema(s, loginCtrl)

    logger.debug(
        "Response of user login: {}".format(resp.status_code))
    if resp.status_code == 303:  # correct user credentials provided
        token = resp.headers.get('Authorization')

        print(
            "Your authorization token is '{}' (without quotation marks)".format(token))
        print()
        updateBearerToken(s, token)
        redirectionAddress = resp.headers.get('Location')
        resp = s.get(redirectionAddress)
        if resp.status_code == 200:
            print("Redirecting into the authenticated user."
                  " OAuth token taken on use.")

    return resp


def logoutUser(s, linkRel, ctrls):
    """
    Method for logging user out.

    :param s: request object
    :param linkRel: Link-relation, in this case logout-user
    :param ctrls: All controls in current location
    :return: response object
    """
    logoutUserCtrl = ctrls[linkRel]
    if 'method' not in logoutUserCtrl:
        logoutUserCtrl["method"] = "POST"
    data = {}
    resp = submit_data(s, logoutUserCtrl, data)
    logger.debug("Response after logging out: {}".format(resp.status_code))
    return resp


def buildAllItems(body):
    """
    Method for building all items found in the body for get request in collection. 

    :param body: body of response
    :return: dictionary object with all items { "title1": href1, "title2": href2, ... }
    """
    itemDict = {}
    if 'items' in body:
        for index, (item) in enumerate(body['items']):
            title = "".join(['Item information:\n',
                             buildSingleItem(item).get("PROPERTY")])
            href = item['@controls']['self']['href']
            itemDict[title] = href
    else:
        print("NO ITEMS FROM CURRENT RESOURCE")
    return itemDict


def buildSingleItem(body):
    """
    Creating item dictionary regardless of contents

    :param body: body of response

    :return: dictionary object with single item { "PROPERTY": "Item properties to be printed"}. 
    When key is set as "PROPERTY" menuBuilder knows that this item is not a link to some other resource
    """
    value = ''
    itemDict = {}
    for key in body:
        if key == '@controls' or key == '@namespaces':
            continue
        value = "".join([value, '{}: {}\n'.format(key, body.get(key))])
        logger.debug("Key {} found with value {}".format(key, body.get(key)))
    logger.debug('Final item is: {}'.format(value))
    itemDict['PROPERTY'] = value
    return itemDict


def buildItems(resp):
    """
    Method for building possible items from body.
    Checks if there is items list or not, based
    on that, shows all information excluding namaspaces
    and controls.
    """
    if resp.text:
        body = resp.json()
    else:
        body = {}
    itemDict = {}
    if '@controls' in body:
        if 'items' in body:
            itemDict = buildAllItems(body)
        else:
            itemDict = buildSingleItem(body)
    else:
        pass
        # raise APIError(resp.status_code, resp.content)
    return itemDict


def deleteResource(s, linkRel, ctrls):
    deleteCtrl = ctrls[linkRel]
    if 'method' not in deleteCtrl:
        return None
    data = {}
    resp = submit_data(s, deleteCtrl, data)
    if (resp.status_code < 300) and (resp.status_code >= 200):
        print("Response {}: Delete operation successfull".format(resp.status_code))
    else:
        print("Response {}: Delete operation not successfull".format(resp.status_code))

    return resp


if __name__ == "__main__":
    try:
        with requests.Session() as s:
            s.headers.update({"Accept": "application/vnd.mason+json, */*"})
        try:
            # Let's try connect into the server and
            # see if it is online.
            resp = s.get(API_URL + API_ROOT)
        except ConnectionError as e:
            logger.debug(e)
            # if not (resp.status_code == 200):
            print("Error!\nUnable to connect to the server."
                  " Is server running on {}?".format(
                      API_URL))
            print("Client will shut down..")
            exit()

        # Let's get the json from the API root
        body = resp.json()
        print("This is the test client for Network Scanner API")
        print("You can exit the client by pressing CTRL-C at any point")

        # Entering the API maze...
        exit = False
        previousHref = API_ROOT
        currentHref = API_ROOT
        items = None
        while not exit:
            if '@controls' not in body:
                print("Unable to find any controls from response")
                print("Response = {}".format(body))
                print("Client will shut down")
                sys.exit()

            # Create controls based on controls in
            # current location
            userSelection = createMenuFromControls(
                s, body["@controls"], previousHref, currentHref, items)
            # lets empty the items list so that it will not appear until new items are received from response
            items = None
            logger.debug("Currently registered link relation: {}"
                         .format(userSelection))
            selectedLinkRelation = list(userSelection)[0]
            if not (currentHref == userSelection[selectedLinkRelation]):
                previousHref = currentHref
            currentHref = userSelection[selectedLinkRelation]

            print("Link '{}' chosen".format(selectedLinkRelation))
            print()
            if selectedLinkRelation == "netscan:auth":  # COMPLETED
                resp = getResource(s, userSelection)
            if selectedLinkRelation == "netscan:login-user":

                resp = loginUser(s, selectedLinkRelation, body["@controls"])
                if resp.status_code == 200:
                    currentHref = resp.json().get("@controls").get("self").get("href")

            if selectedLinkRelation == "netscan:logout-user":  # COMPLETED
                resp = logoutUser(s, selectedLinkRelation, body["@controls"])
                items = None
                if resp.status_code == 303:  # Expecting redirect to /api/auth
                    print("LOGOUT SUCCESSFULL! CLIENT WILL SHUT DOWN")
                else:
                    raise APIError(resp.status_code, resp.content)
                sys.exit()
            if selectedLinkRelation == "netscan:users-all":  # COMPLETED
                resp = getResource(s, userSelection)
            if selectedLinkRelation == "netscan:add-user":  # COMPLETED
                # Let's create a new user!
                resp = createNewUser(
                    s, body["@controls"].get("netscan:add-user"))
                if resp.status_code == 200:
                    currentHref = resp.json().get("@controls").get("self").get("href")
            if selectedLinkRelation == "netscan:edit-user":  # COMPLETED
                resp = editUser(s, body["@controls"].get("netscan:edit-user"))
            if selectedLinkRelation == "netscan:delete-user":  # COMPLETED
                resp = deleteResource(
                    s, selectedLinkRelation, body["@controls"])
            if selectedLinkRelation == "up":  # COMPLETED
                resp = getResource(s, userSelection)
            if selectedLinkRelation == "other":  # COMPLETED
                # If user selects 0 or 1 from menu this will be done
                resp = s.get(API_URL + currentHref)
            if selectedLinkRelation == "item":  # COMPLETED
                resp = getResource(s, userSelection)

            if resp.text:
                body = resp.json()
            else:
                body = {'@controls': {'None': 'None'}}

            # Check that response does not contain errors
            # If it does, go back to previous location
            # and print the error message
            if '@error' in body:
                print("Error when trying to access resource {} :"
                      .format(currentHref))
                logger.debug(body)
                for message in body["@error"]["@messages"]:
                    print(message)
                currentHref = previousHref
                resp = s.get(API_URL + previousHref)
                body = resp.json()
                continue

            items = buildItems(resp)
            print()

    except KeyboardInterrupt:
        print("CTRL-C detected")
        print("Client will shut down")
        sys.exit()
