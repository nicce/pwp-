## Introduction

This document describes the client script usage. 

## Setting up the client

Client is implemented with Python. It requires version 3.5.3+

It uses some external libraries that must be installed before usage. Run following command to install those libraries:

`pip install requests`

When external libraries are installed the client is ready to be used.
Server must be running, before starting the client.

## Running the client

Start the server with command `python -m network_scanner` in "/pwp-/server" directory

Start the client with command `python client.py` in "/pwp-/client/" directory

Debug mode for client can be enabled by running it as `python client.py --debug`